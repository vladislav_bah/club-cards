﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class MainForm : Form
    {
        private DataBase DB;
        private int id = -1;
        private String abonementID = "";
        private String abonementName = "";
        private COM com;
        private int counter = 0;
        private int zal = 0;
        private bool fitnes_result = false;
        private string portt = "COM3";
        public MainForm()
        {
            DB = new DataBase("db.db");
            if (!DB.Connect())
                MessageBox.Show("Ошибка подключения к базе данных");

            try
            {
                com = new COM(this, DB, portt);
                com.OpenPort();
            }
            catch
            {
                Com_selection window = new Com_selection(this);
                window.ShowDialog();
                com = new COM(this, DB, portt);
                com.OpenPort();
            }
            InitializeComponent();
            gb_abonement.Focus();
            UpdateClientVisiting();
            b_startExercise.Focus();
        }

        public string port
        {
            set { portt = value; }
            get { return portt; }
        }
        public string abName
        {
            get { return abonementName; }
        }
        public int Zal
        {
            set { zal = value; }
        }
        public bool Fitnes_result
        {
            set { fitnes_result = value; }
        }
        public void Fillform(int id) //заполнение формы по ID
        {

            try
            {
                clearAbonement();
                this.id = id;
                checkAbonement();
                DataSet ds = DB.Select("Select * from Clients where ID = " + id);
                DataSet count = DB.Select("select count(*) from meta where SubId in " +
                    " (select SubID from Clients_subs " +
                    " where ClientID = " + ds.Tables[0].DefaultView[0][0] + ")");
                DataSet area1 = DB.Select("Select AreaName from Areas where ID = " + Convert.ToInt32(ds.Tables[0].DefaultView[0][11]));
                DataSet area2 = DB.Select("Select AreaName from Areas where ID = " + Convert.ToInt32(ds.Tables[0].DefaultView[0][12]));
                counter = 0;
                t_clear.Enabled = true;
                if (Convert.ToInt32(count.Tables[0].DefaultView[0][0]) > 0)
                {
                    ChangeActive("Cейчас на занятии", Color.Green);
                    bt_endExercise.Enabled = true;
                }
                else
                {
                    ChangeActive("Не на занятии", Color.Red);
                }

                try
                {
                    pb_photo.ImageLocation = Convert.ToString(ds.Tables[0].DefaultView[0][2]);
                    pb_photo.Load();
                }
                catch
                {
                    pb_photo.ImageLocation = "Photos/Лого совершенство.png";// убрать
                    pb_photo.Load();

                }
                DataSet kids = DB.Select("select Name, DateOfBirth from Children where ParentID = '" + id.ToString() + "'");
                try
                {
                    lv_kids.Clear();
                    lv_kids.Columns.Add("Имя");
                    lv_kids.Columns.Add("Дата рождения");
                    foreach (DataRow child in kids.Tables[0].Rows)
                    {
                        var lvi = new ListViewItem(new[] { child.ItemArray[0].ToString(), child.ItemArray[1].ToString() });
                        lv_kids.Items.Add(lvi);
                    }
                    lv_kids.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                    lv_kids.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                }
                catch { }
                try
                {
                    //tb_card.Text = Convert.ToString(ds.Tables[0].DefaultView[0][1]); номер карты куда?
                    tb_name.Text = Convert.ToString(ds.Tables[0].DefaultView[0][3]);
                    tb_family.Text = Convert.ToString(ds.Tables[0].DefaultView[0][4]);
                    tb_thirdName.Text = Convert.ToString(ds.Tables[0].DefaultView[0][5]);
                    if (Convert.ToInt32(ds.Tables[0].DefaultView[0][6]) == 1)
                        tb_sex.Text = "Мужской";
                    else
                        tb_sex.Text = "Женский";
                    tb_phone.Text = Convert.ToString(ds.Tables[0].DefaultView[0][7]);
                    tb_email.Text = Convert.ToString(ds.Tables[0].DefaultView[0][8]);
                    tb_birthday.Text = Convert.ToString(ds.Tables[0].DefaultView[0][9]);
                    if (Convert.ToInt32(ds.Tables[0].DefaultView[0][15]) == 0)
                        tb_sale.Text = Convert.ToString(ds.Tables[0].DefaultView[0][10]) + " %";
                    else
                        tb_sale.Text = Convert.ToString(ds.Tables[0].DefaultView[0][10]) + " руб";
                    tb_livePlace.Text = Convert.ToString(area1.Tables[0].DefaultView[0][0]);
                    tb_workPlace.Text = Convert.ToString(area2.Tables[0].DefaultView[0][0]);
                    rtb_note.Text = Convert.ToString(ds.Tables[0].DefaultView[0][13]);
                    b_editClient.Enabled = true;
                    b_delete.Enabled = true;
                    b_addAbonement.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка заполнения профиля. Перезапустите приложение. Error:" + ex.Message);
                }
                fillabonementList();
                try
                {
                    fillAbonement(Convert.ToString(lv_abonements.Items[0].SubItems[0].Text));
                    if (lv_abonements.Items.Count > 0)
                    {
                        b_prolongation.Enabled = true;
                        b_freeze.Enabled = true;
                        b_editAbonement.Enabled = true;
                    }
                }
                catch
                {

                }
            }
            catch
            {
                MessageBox.Show("Ошибка заполнения формы");
            }

        }
        public void fillabonementList()
        {
            try
            {
                lv_abonements.Clear();
                lv_abonements.Columns.Add("ID");
                lv_abonements.Columns.Add("Абонемент");
                DataSet tmp = DB.Select("select Subs.ID, Offers.Name, NumberOfVisits, Offers.type, Subs.Status  from Offers, Subs, Clients_Subs " +
                    "where Clients_Subs.ClientID = " + id + " and SubID = Subs.ID and Subs.OfferID = Offers.ID");
                foreach (DataRow abonement in tmp.Tables[0].Rows)
                {
                    string type;
                    if (abonement.ItemArray[2].ToString() != "-1")
                    {
                        if (abonement.ItemArray[3].ToString() == "0")
                        {
                            type = "дневной";
                        }
                        else
                        {
                            type = "вечерний";
                        }
                        var lvi = new ListViewItem(new[] { abonement.ItemArray[0].ToString(), abonement.ItemArray[1].ToString() + " (" + abonement.ItemArray[2].ToString() + " посещений, " + type + ")" });
                        lv_abonements.Items.Add(lvi);
                        if (abonement.ItemArray[4].ToString() == "0")
                        {
                            lv_abonements.Items[lv_abonements.Items.Count - 1].BackColor = Color.LightGray;
                        }
                        if (abonement.ItemArray[4].ToString() == "1")
                        {
                            lv_abonements.Items[lv_abonements.Items.Count - 1].BackColor = Color.LightGreen;
                        }
                        if (abonement.ItemArray[4].ToString() == "2")
                        {
                            lv_abonements.Items[lv_abonements.Items.Count - 1].BackColor = Color.RosyBrown;
                        }
                    }
                    else
                    {
                        if (abonement.ItemArray[3].ToString() == "0")
                        {
                            type = "дневной";
                        }
                        else
                        {
                            type = "вечерний";
                        }
                        var lvi = new ListViewItem(new[] { abonement.ItemArray[0].ToString(), abonement.ItemArray[1].ToString() + " (месяц, " + type + ")" });
                        lv_abonements.Items.Add(lvi);
                        if (abonement.ItemArray[4].ToString() == "0")
                        {
                            lv_abonements.Items[lv_abonements.Items.Count - 1].BackColor = Color.LightGray;
                        }
                        if (abonement.ItemArray[4].ToString() == "1")
                        {
                            lv_abonements.Items[lv_abonements.Items.Count - 1].BackColor = Color.LightGreen;
                        }
                        if (abonement.ItemArray[4].ToString() == "2")
                        {
                            lv_abonements.Items[lv_abonements.Items.Count - 1].BackColor = Color.RosyBrown;
                        }
                    }
                }
                lv_abonements.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                lv_abonements.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                if (lv_abonements.Items.Count > 0)
                {
                    lv_abonements.Items[0].Selected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения списка абонементов. Error: " + ex.Message);
            }
        }
        public void fillAbonement(string ab_ID)
        {
            try
            {

                abonementID = ab_ID;
                tb_freezeDaysRest.Text = "";
                DataSet tmp = DB.Select("select Offers.Name, offers.Type, Subs.Status, CountOfVisits, NumberOfVisits, FirstVisitDate, ActivationDate, " +
                    "subs.EndDate, FreezingStartDate, FreezingEndDate, stockID " +
                    "from Subs, Offers " +
                    "where Subs.ID = " + ab_ID + " and Subs.OfferID = Offers.ID");
                if (Convert.ToString(tmp.Tables[0].DefaultView[0][10]) == "-1")
                {
                    tb_stock.Text = "Нет";
                }
                else
                {
                    DataSet tmp2 = DB.Select("Select name from stock where id = " + Convert.ToString(tmp.Tables[0].DefaultView[0][10]));
                    tb_stock.Text = Convert.ToString(tmp2.Tables[0].DefaultView[0][0]);
                }
                tb_serviceName.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][0]);
                switch (Convert.ToInt32(tmp.Tables[0].DefaultView[0][1]))
                {
                    case 0: tb_type.Text = "Дневной"; break;
                    case 1: tb_type.Text = "Вечерний"; break;
                }

                switch (Convert.ToInt32(tmp.Tables[0].DefaultView[0][2]))
                {
                    case 0: tb_abonementStatus.Text = "Заморожен"; tb_abonementStatus.BackColor = Color.LightGray;
                        b_startExercise.Enabled = false; break;
                    case 1: tb_abonementStatus.Text = "Активен"; tb_abonementStatus.BackColor = Color.LightGreen;
                        b_startExercise.Enabled = true; break;
                    case 2: tb_abonementStatus.Text = "Неактивен"; tb_abonementStatus.BackColor = Color.RosyBrown;
                        b_startExercise.Enabled = false; break;
                }
                if (Convert.ToInt32(tmp.Tables[0].DefaultView[0][4]) != -1)
                {
                    tb_countOfExercice.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][4]);
                    tb_finishedExercise.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][3]);
                    tb_restOfExercise.Text = Convert.ToString(Convert.ToInt32(tmp.Tables[0].DefaultView[0][4]) - Convert.ToInt32(tmp.Tables[0].DefaultView[0][3]));
                }
                else
                {
                    tb_countOfExercice.Text = "Не ограничено";
                    tb_finishedExercise.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][3]);
                    tb_restOfExercise.Text = "Не ограничено";
                }
                tb_activationDate.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][6]);
                tb_startDate.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][5]);
                tb_freezeStartDate.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][8]);
                tb_freezeEndDate.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][9]);
                tb_endDate.Text = Convert.ToString(tmp.Tables[0].DefaultView[0][7]);
                if (tb_freezeStartDate.Text != "" && tb_freezeEndDate.Text != "")
                {
                    DateTime fr1 = DateTime.Parse(Convert.ToString(tmp.Tables[0].DefaultView[0][8]));
                    DateTime fr2 = DateTime.Parse(Convert.ToString(tmp.Tables[0].DefaultView[0][9]));
                    int days = Convert.ToInt32((fr2 - fr1).TotalDays);
                    tb_freezeDaysRest.Text = Convert.ToString(days);
                }
            }
            catch
            {
                MessageBox.Show("Ошибка заполнения абонементов");
            }
        }
        public void clearAbonement()
        {
            foreach (Control c in Controls)
            {
                if (c.GetType() == typeof(GroupBox))
                    if (c.Name == "gb_abonement")
                    {
                        foreach (Control d in c.Controls)
                            if (d.GetType() == typeof(TextBox))
                                d.Text = string.Empty;
                    }
                lb_active.Text = "";
                abonementID = "";
            }
        }
        public int ID
        {
            get { return id; }

        }
        private void b_editClient_Click(object sender, EventArgs e)
        {
            com.ClosePort();
            EditClient editForm = new EditClient(DB, id, this);
            editForm.ShowDialog();
            com.OpenPort();
        }

        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            com.ClosePort();
            EditClient editForm = new EditClient(DB, this);
            editForm.ShowDialog();
            com.OpenPort();
        }

        private void открытьПрофильКлиентаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchClient searchForm = new SearchClient(DB, this);
            searchForm.ShowDialog();
        }

        private void b_addAbonement_Click(object sender, EventArgs e)
        {
            editAbonement editForm = new editAbonement(id, DB, this);
            editForm.ShowDialog();
            if (lv_abonements.Items.Count > 0)
            {
                b_prolongation.Enabled = true;
                b_freeze.Enabled = true;
                b_editAbonement.Enabled = true;
                //b_startExercise.Enabled = true;
            }
            try
            {

                lv_abonements.Items[0].Selected = false;
                lv_abonements.Items[lv_abonements.Items.Count - 1].Selected = true;
            }
            catch { }
        }

        private void b_editAbonement_Click(object sender, EventArgs e)
        {
            int n = lv_abonements.SelectedIndices[0];
            editAbonement editForm = new editAbonement(id, Convert.ToInt32(abonementID), lv_abonements.SelectedItems[0].SubItems[1].Text, tb_stock.Text, DB, this, "Редактирование абонемента");
            editForm.ShowDialog();
            lv_abonements.Items[0].Selected = false;
            lv_abonements.Items[n].Selected = true;
        }

        private void b_freeze_Click(object sender, EventArgs e)
        {
            int n = lv_abonements.SelectedIndices[0];
            freezeForm freeze = new freezeForm(DB, Convert.ToInt32(abonementID), id, this);
            freeze.ShowDialog();
            fillabonementList();
            lv_abonements.Items[0].Selected = false;
            lv_abonements.Items[n].Selected = true;
        }

        private void lv_abonements_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_abonements.SelectedItems.Count > 0)
            {
                abonementID = Convert.ToString(lv_abonements.SelectedItems[0].SubItems[0].Text);
                fillAbonement(abonementID);
                abonementName = lv_abonements.SelectedItems[0].SubItems[1].Text;
            }
        }
        public void ChangeActive(string S, Color col)
        {
            if (S == "Сейчас на занятии")
                bt_endExercise.Enabled = true;
            else
                bt_endExercise.Enabled = false;
            lb_active.Text = S;
            lb_active.ForeColor = col;
        }


        private void b_startExercise_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = false;

                DateTime time1 = DateTime.Now;
                DataSet tmp = DB.Select("select CountOfVisits, NumberOfVisits, FirstVisitDate, ActivationDate, " +
                                        "EndDate, Offers.Name " +
                                        "from Subs, Offers " +
                                        "where Subs.ID = " + abonementID + " and Subs.OfferID = Offers.ID");

                DataSet inf = DB.Select("select offers.name, meta.zal from offers, meta, subs " +
                                        "where meta.SubID = Subs.ID and " + 
                                        "Subs.ID =  "+ abonementID + " and " +
                                        "Subs.OfferID = offers.ID");
       
                if (Convert.ToInt32(tmp.Tables[0].DefaultView[0][1]) == -1)
                {
                    DataSet count = DB.Select("select count(*) from meta where SubId = " + abonementID);
                    if (Convert.ToInt32(count.Tables[0].DefaultView[0][0]) > 0)
                    {
                        int zalNumber;
                        DataSet zal = DB.Select("select zal from meta where subID = " + abonementID);
                        if (zal.Tables[0].DefaultView[0][0].ToString() == "" || Convert.ToInt32(zal.Tables[0].DefaultView[0][0]) == 1)
                        {
                            zalNumber = 1;
                        }
                        else zalNumber = Convert.ToInt32(zal.Tables[0].DefaultView[0][0]);
                        MessageBox.Show("Этот клиент еще занимается. " + Environment.NewLine
                                      + "Услуга: " + Convert.ToString(inf.Tables[0].DefaultView[0][0]) + Environment.NewLine
                                      + "Зал № " + zalNumber);
                    }
                    else
                    {
                        if (Convert.ToString(tmp.Tables[0].DefaultView[0][5]) == "Фитнес")
                        {
                            fitnes_choose fit = new fitnes_choose(DB, this);
                            fit.ShowDialog();
                            if (fitnes_result)
                            {
                                flag = true;
                                DB.Query("Update Subs set CountOfVisits = " + Convert.ToString(Convert.ToInt32(tb_finishedExercise.Text) + 1) +
                                    " where Subs.ID = " + abonementID);
                                DB.Query("Insert into meta values (" + abonementID + ", '" + time1.ToShortDateString() + "', '" + time1.ToShortTimeString() + "'," + zal.ToString() + ")");
                            }
                        }
                        else
                        {
                            flag = true;
                            DB.Query("Update Subs set CountOfVisits = " + Convert.ToString(Convert.ToInt32(tb_finishedExercise.Text) + 1) +
                                " where Subs.ID = " + abonementID);
                            DB.Query("Insert into meta(SubId, StartDate, StartTime) values (" + abonementID + ", '" + time1.ToShortDateString() + "', '" + time1.ToShortTimeString() + "')");
                        }

                    }
                }
                else
                {
                    if (Convert.ToInt32(tmp.Tables[0].DefaultView[0][1]) - Convert.ToInt32(tmp.Tables[0].DefaultView[0][0]) > 0)
                    {
                        DataSet count = DB.Select("select count(SubID) from meta where SubId = " + abonementID);
                        if (Convert.ToInt32(count.Tables[0].DefaultView[0][0]) > 0)
                        {
                            MessageBox.Show( "Этот клиент еще занимается. " + Environment.NewLine 
                                            + "Услуга: " + Convert.ToString(inf.Tables[0].DefaultView[0][0]) + Environment.NewLine
                                            + "Зал № " + Convert.ToString(inf.Tables[0].DefaultView[0][1]));
                        }
                        else
                        {
                            if (Convert.ToString(tmp.Tables[0].DefaultView[0][5]) == "Фитнес")
                            {
                                fitnes_choose fit = new fitnes_choose(DB, this);
                                fit.ShowDialog();
                                if (fitnes_result)
                                {
                                    flag = true;
                                    DB.Query("Update Subs set CountOfVisits = " + Convert.ToString(Convert.ToInt32(tb_finishedExercise.Text) + 1) +
                                    " where Subs.ID = " + abonementID);
                                    DB.Query("Insert into meta values (" + abonementID + ", '" + time1.ToShortDateString() + "', '" + time1.ToShortTimeString() + "'," + zal.ToString() + ")");
                                }
                            }
                            else
                            {
                                flag = true;
                                DB.Query("Update Subs set CountOfVisits = " + Convert.ToString(Convert.ToInt32(tb_finishedExercise.Text) + 1) +
                                    " where Subs.ID = " + abonementID);
                                DB.Query("Insert into meta(SubId, StartDate, StartTime) values (" + abonementID + ", '" + time1.ToShortDateString() + "', '" + time1.ToShortTimeString() + "')");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Посещения закончились");
                    }
                }
                if (Convert.ToString(tmp.Tables[0].DefaultView[0][2]) == "")
                {
                    DB.Query("Update Subs set FirstVisitDate = '" + DateTime.Today.Date.ToLongDateString() + "'" +
                        " where Subs.ID = " + abonementID);
                }

                if (flag)
                    ChangeActive("Сейчас на занятии", Color.Green);
                fillAbonement(abonementID);
                UpdateClientVisiting();
            }
            catch
            {
                MessageBox.Show("Ошибка Базы Данных");
            }
        }

        private void редактироватьСписокУслугToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditOffers edit = new EditOffers(DB);
            edit.ShowDialog();

        }

        private void добавитьАкциюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditStocks stocks = new EditStocks(DB);
            stocks.ShowDialog();
        }

        private void b_prolongation_Click(object sender, EventArgs e)
        {
            int n = lv_abonements.SelectedIndices[0];
            Prolongation prol = new Prolongation(DB, this, lv_abonements.SelectedItems[0].SubItems[1].Text, abonementID, tb_endDate.Text);
            prol.ShowDialog();
            fillabonementList();
            fillAbonement(abonementID);
            lv_abonements.Items[0].Selected = false;
            lv_abonements.Items[n].Selected = true;

        }

        private void checkAbonement()
        {
            try
            {
                DataSet tmp = DB.Select("select Subs.ID from Subs, Clients_Subs " +
                    "where Clients_Subs.ClientID = " + id + " and SubID = Subs.ID");
                foreach (DataRow abonement in tmp.Tables[0].Rows)
                {
                    DataSet ds = DB.Select("Select EndDate, FreezingStartDate, FreezingEndDate from subs where id =" + abonement.ItemArray[0]);
                    //MessageBox.Show(DateTime.Today.Date.ToString() + Convert.ToString(Convert.ToDateTime(ds.Tables[0].DefaultView[0][0])));

                    if (ds.Tables[0].DefaultView[0][1].ToString() != "" && DateTime.Today.Date >= Convert.ToDateTime(ds.Tables[0].DefaultView[0][1]))
                    {
                        DB.Query("update subs set status = 0 where id =" + abonement.ItemArray[0]);
                    }
                    if (ds.Tables[0].DefaultView[0][2].ToString() != "" && DateTime.Today.Date >= Convert.ToDateTime(ds.Tables[0].DefaultView[0][2]))
                    {
                        DB.Query("update subs set status = 1, FreezingStartDate = '', FreezingEndDate = '' where id =" + abonement.ItemArray[0]);

                    }
                    if (DateTime.Today.Date >= Convert.ToDateTime(ds.Tables[0].DefaultView[0][0]))
                    {
                        DB.Query("update subs set status = 2 where id =" + abonement.ItemArray[0]);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка проверки даты абонемента");
            }
        }
        private void bt_endExercise_Click(object sender, EventArgs e)
        {


            DataSet count;
            DateTime time2 = DateTime.Now;
            try
            {
                count = DB.Select("select count(*), SubID from meta where SubId in " +
                     " (select SubID from Clients_subs " +
                     " where ClientID = " + id + ")");
                DataSet dtime = DB.Select("select startDate, startTime from meta where SubId = " + abonementID);
                if (Convert.ToInt32(count.Tables[0].DefaultView[0][0]) > 0)
                {
                    EndOfExercise endform = new EndOfExercise(id, DB, Convert.ToString(count.Tables[0].DefaultView[0][1]));
                    endform.ShowDialog();
                    DataSet IsEx = DB.Select("select * from meta where SubID = " + abonementID);
                    if (IsEx.Tables[0].Rows.Count == 0)
                    {
                        ChangeActive("Не на занятии", System.Drawing.Color.Red);


                        DB.Query("Insert into History(FIO, Offer, startDate, endDate) values('" + tb_family.Text + " " + tb_name.Text + " " + tb_thirdName.Text + "', " +
                            "'" + lv_abonements.SelectedItems[0].SubItems[1].Text + "', '" + dtime.Tables[0].DefaultView[0][0] + " " +
                            dtime.Tables[0].DefaultView[0][1] + "', '" + time2.ToShortDateString() + " " + time2.ToShortTimeString() + "')");
                    }
                }
                UpdateClientVisiting();
            }
            catch
            {
                MessageBox.Show("Ошибка Базы Данных");
            }
        }

        private void clearall()
        {
            foreach (Control c in Controls)
            {
                if (c.GetType() == typeof(GroupBox))
                    foreach (Control d in c.Controls)
                    {
                        if (d.GetType() == typeof(TextBox))
                            d.Text = string.Empty;
                        if (d.GetType() == typeof(ListView))
                            ((ListView)d).Clear();

                    }


            }
            lb_active.Text = "";
            abonementID = "";
            pb_photo.ImageLocation = "Photos/Лого совершенство.png";
            rtb_note.Text = string.Empty;
            pb_photo.Load();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter++;
            if (counter == 120)
            {
                clearall();
                counter = 0;
                t_clear.Enabled = false;
            }
        }


        private void MainForm_Deactivate(object sender, EventArgs e)
        {
            t_clear.Enabled = false;
            counter = 0;
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
            t_clear.Enabled = true;

        }


        private void MainForm_MouseEnter(object sender, EventArgs e)
        {
            counter = 0;
        }

        private void b_delete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите удалить профиль " + tb_name.Text + " " + tb_family.Text, "Подтверждение", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    string query = "delete from Clients_Subs where ClientID =" + id;
                    DB.Query(query);
                    query = "delete from Clients where ID =" + id;
                    DB.Query(query);
                    clearall();

                }
                catch
                {
                    MessageBox.Show("Ошибка удаления пользователя");
                }
            }

        }


        private void тренераToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Treners tren = new Treners(DB);
            tren.ShowDialog();
        }

        private void расписаниеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Shedule shedule = new Shedule(DB);
            shedule.ShowDialog();
        }

        private void выборПортаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            com.ClosePort();
            Com_selection window = new Com_selection(this);
            window.ShowDialog();
            com = new COM(this, DB, portt);
            com.OpenPort();

        }

        private void статистикаПосещенийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StatForm stat = new StatForm(DB, this);
            stat.ShowDialog();
        }

        private void статистикаОплатToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PaymentStory stat = new PaymentStory(DB);
            stat.ShowDialog();
        }

        private void статистикаЗаДеньToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            StatForDay stat = new StatForDay(DB);
            stat.ShowDialog();
        }

        private void выгрузитьСтатистикуЗаМесяцToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            StatMonth mon = new StatMonth(DB);
            mon.ShowDialog();
        }

        private void UpdateClientVisiting()
        {
            var date = Convert.ToString(DateTime.Now.ToShortDateString());
            DataSet count = DB.Select("select count(*) from history where endDate like'" + date + "%'");
            if (Convert.ToInt32(count.Tables[0].DefaultView[0][0]) >= 0)
            {
                lbSumPerDay.Text = "За день: " + count.Tables[0].DefaultView[0][0].ToString();
            }
            count = DB.Select("select count(*) from meta where zal = '" + 1 + "' or zal is NULL");
            var bz = Convert.ToInt32(count.Tables[0].DefaultView[0][0]);
            if (bz >= 0)
            {
                lbBZ.Text = "Большой зал: " + count.Tables[0].DefaultView[0][0].ToString();
            }

            count = DB.Select("select count(*) from meta where zal = '" + 2 + "'");
            var mz = Convert.ToInt32(count.Tables[0].DefaultView[0][0]);
            if (mz >= 0)
            {
                lbMZ.Text = "Малый зал: " + count.Tables[0].DefaultView[0][0].ToString();
            }

            lbSum.Text = "Всего: " + (bz + mz);
        }

        private void InfoAboutLesson_Click(object sender, EventArgs e)
        {

           
        }
    }
}   
