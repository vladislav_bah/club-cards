﻿namespace ClubCardForms
{
    partial class AddTrener
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_secondName = new System.Windows.Forms.TextBox();
            this.tb_firstName = new System.Windows.Forms.TextBox();
            this.tb_thirdName = new System.Windows.Forms.TextBox();
            this.tb_phone = new System.Windows.Forms.TextBox();
            this.bt_add = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Фамилия:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Отчество:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Личный телефон:";
            // 
            // tb_secondName
            // 
            this.tb_secondName.Location = new System.Drawing.Point(66, 23);
            this.tb_secondName.Name = "tb_secondName";
            this.tb_secondName.Size = new System.Drawing.Size(171, 20);
            this.tb_secondName.TabIndex = 4;
            this.tb_secondName.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_firstName
            // 
            this.tb_firstName.Location = new System.Drawing.Point(66, 71);
            this.tb_firstName.Name = "tb_firstName";
            this.tb_firstName.Size = new System.Drawing.Size(171, 20);
            this.tb_firstName.TabIndex = 5;
            this.tb_firstName.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_thirdName
            // 
            this.tb_thirdName.Location = new System.Drawing.Point(66, 122);
            this.tb_thirdName.Name = "tb_thirdName";
            this.tb_thirdName.Size = new System.Drawing.Size(171, 20);
            this.tb_thirdName.TabIndex = 6;
            this.tb_thirdName.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_phone
            // 
            this.tb_phone.Location = new System.Drawing.Point(66, 176);
            this.tb_phone.Name = "tb_phone";
            this.tb_phone.Size = new System.Drawing.Size(171, 20);
            this.tb_phone.TabIndex = 7;
            // 
            // bt_add
            // 
            this.bt_add.Enabled = false;
            this.bt_add.Location = new System.Drawing.Point(104, 214);
            this.bt_add.Name = "bt_add";
            this.bt_add.Size = new System.Drawing.Size(75, 23);
            this.bt_add.TabIndex = 8;
            this.bt_add.Text = "Добавить";
            this.bt_add.UseVisualStyleBackColor = true;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // AddTrener
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 261);
            this.Controls.Add(this.bt_add);
            this.Controls.Add(this.tb_phone);
            this.Controls.Add(this.tb_thirdName);
            this.Controls.Add(this.tb_firstName);
            this.Controls.Add(this.tb_secondName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddTrener";
            this.Text = "Добавление тренера";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_secondName;
        private System.Windows.Forms.TextBox tb_firstName;
        private System.Windows.Forms.TextBox tb_thirdName;
        private System.Windows.Forms.TextBox tb_phone;
        private System.Windows.Forms.Button bt_add;
    }
}