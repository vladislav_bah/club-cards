﻿namespace ClubCardForms
{
    partial class AddStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddStock));
            this.bt_addOrDel = new System.Windows.Forms.Button();
            this.lv_notLinked = new System.Windows.Forms.ListView();
            this.lv_linked = new System.Windows.Forms.ListView();
            this.cb_typeOfDiscount = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtp_end = new System.Windows.Forms.DateTimePicker();
            this.dtp_start = new System.Windows.Forms.DateTimePicker();
            this.tb_discount = new System.Windows.Forms.MaskedTextBox();
            this.tb_stockName = new System.Windows.Forms.MaskedTextBox();
            this.bt_add = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bt_addOrDel
            // 
            this.bt_addOrDel.Location = new System.Drawing.Point(225, 239);
            this.bt_addOrDel.Name = "bt_addOrDel";
            this.bt_addOrDel.Size = new System.Drawing.Size(71, 23);
            this.bt_addOrDel.TabIndex = 71;
            this.bt_addOrDel.UseVisualStyleBackColor = true;
            this.bt_addOrDel.Click += new System.EventHandler(this.bt_addOrDel_Click);
            // 
            // lv_notLinked
            // 
            this.lv_notLinked.FullRowSelect = true;
            this.lv_notLinked.GridLines = true;
            this.lv_notLinked.HideSelection = false;
            this.lv_notLinked.Location = new System.Drawing.Point(302, 131);
            this.lv_notLinked.MultiSelect = false;
            this.lv_notLinked.Name = "lv_notLinked";
            this.lv_notLinked.Size = new System.Drawing.Size(198, 247);
            this.lv_notLinked.TabIndex = 70;
            this.lv_notLinked.UseCompatibleStateImageBehavior = false;
            this.lv_notLinked.View = System.Windows.Forms.View.Details;
            this.lv_notLinked.Click += new System.EventHandler(this.lv_notLinked_Click);
            // 
            // lv_linked
            // 
            this.lv_linked.FullRowSelect = true;
            this.lv_linked.GridLines = true;
            this.lv_linked.HideSelection = false;
            this.lv_linked.Location = new System.Drawing.Point(28, 131);
            this.lv_linked.MultiSelect = false;
            this.lv_linked.Name = "lv_linked";
            this.lv_linked.Size = new System.Drawing.Size(191, 247);
            this.lv_linked.TabIndex = 69;
            this.lv_linked.UseCompatibleStateImageBehavior = false;
            this.lv_linked.View = System.Windows.Forms.View.Details;
            this.lv_linked.Click += new System.EventHandler(this.lv_linked_Click);
            // 
            // cb_typeOfDiscount
            // 
            this.cb_typeOfDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_typeOfDiscount.FormattingEnabled = true;
            this.cb_typeOfDiscount.Items.AddRange(new object[] {
            "Рублей",
            "%"});
            this.cb_typeOfDiscount.Location = new System.Drawing.Point(134, 88);
            this.cb_typeOfDiscount.Name = "cb_typeOfDiscount";
            this.cb_typeOfDiscount.Size = new System.Drawing.Size(85, 21);
            this.cb_typeOfDiscount.TabIndex = 67;
            this.cb_typeOfDiscount.TextChanged += new System.EventHandler(this.Changed);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(297, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 66;
            this.label5.Text = "Дата окончания";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(297, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 65;
            this.label4.Text = "Дата начала:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 64;
            this.label3.Text = "Размер скидки:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 63;
            this.label2.Text = "Название акции";
            // 
            // dtp_end
            // 
            this.dtp_end.Location = new System.Drawing.Point(300, 88);
            this.dtp_end.Name = "dtp_end";
            this.dtp_end.Size = new System.Drawing.Size(200, 20);
            this.dtp_end.TabIndex = 62;
            this.dtp_end.ValueChanged += new System.EventHandler(this.Changed);
            // 
            // dtp_start
            // 
            this.dtp_start.Location = new System.Drawing.Point(300, 45);
            this.dtp_start.Name = "dtp_start";
            this.dtp_start.Size = new System.Drawing.Size(200, 20);
            this.dtp_start.TabIndex = 61;
            // 
            // tb_discount
            // 
            this.tb_discount.Location = new System.Drawing.Point(28, 89);
            this.tb_discount.Name = "tb_discount";
            this.tb_discount.Size = new System.Drawing.Size(100, 20);
            this.tb_discount.TabIndex = 60;
            this.tb_discount.TextChanged += new System.EventHandler(this.Changed);
            // 
            // tb_stockName
            // 
            this.tb_stockName.Location = new System.Drawing.Point(28, 46);
            this.tb_stockName.Name = "tb_stockName";
            this.tb_stockName.Size = new System.Drawing.Size(191, 20);
            this.tb_stockName.TabIndex = 59;
            this.tb_stockName.TextChanged += new System.EventHandler(this.Changed);
            // 
            // bt_add
            // 
            this.bt_add.Enabled = false;
            this.bt_add.Location = new System.Drawing.Point(189, 425);
            this.bt_add.Name = "bt_add";
            this.bt_add.Size = new System.Drawing.Size(151, 33);
            this.bt_add.TabIndex = 72;
            this.bt_add.Text = "Добавить";
            this.bt_add.UseVisualStyleBackColor = true;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // AddStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 483);
            this.Controls.Add(this.bt_add);
            this.Controls.Add(this.bt_addOrDel);
            this.Controls.Add(this.lv_notLinked);
            this.Controls.Add(this.lv_linked);
            this.Controls.Add(this.cb_typeOfDiscount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtp_end);
            this.Controls.Add(this.dtp_start);
            this.Controls.Add(this.tb_discount);
            this.Controls.Add(this.tb_stockName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddStock";
            this.Text = "Добавление акции";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_addOrDel;
        private System.Windows.Forms.ListView lv_notLinked;
        private System.Windows.Forms.ListView lv_linked;
        private System.Windows.Forms.ComboBox cb_typeOfDiscount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtp_end;
        private System.Windows.Forms.DateTimePicker dtp_start;
        private System.Windows.Forms.MaskedTextBox tb_discount;
        private System.Windows.Forms.MaskedTextBox tb_stockName;
        private System.Windows.Forms.Button bt_add;

    }
}