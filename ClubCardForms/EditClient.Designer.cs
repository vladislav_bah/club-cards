﻿namespace ClubCardForms
{
    partial class EditClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditClient));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.b_photoFromCamera = new System.Windows.Forms.Button();
            this.cb_typeOfDiscount = new System.Windows.Forms.ComboBox();
            this.lv_kids = new System.Windows.Forms.ListView();
            this.tb_phone = new System.Windows.Forms.MaskedTextBox();
            this.b_changeCard = new System.Windows.Forms.Button();
            this.rb_note = new System.Windows.Forms.RichTextBox();
            this.dtp_birthday = new System.Windows.Forms.DateTimePicker();
            this.b_addKid = new System.Windows.Forms.Button();
            this.cb_livePlace = new System.Windows.Forms.ComboBox();
            this.cb_workPlace = new System.Windows.Forms.ComboBox();
            this.b_photo = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.tb_card = new System.Windows.Forms.TextBox();
            this.b_ok = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.rb_woman = new System.Windows.Forms.RadioButton();
            this.rb_man = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_status = new System.Windows.Forms.ComboBox();
            this.tb_sale = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.tb_thirdName = new System.Windows.Forms.TextBox();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_family = new System.Windows.Forms.TextBox();
            this.pb_photo = new System.Windows.Forms.PictureBox();
            this.ofd_photo = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_photo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.b_photoFromCamera);
            this.groupBox1.Controls.Add(this.cb_typeOfDiscount);
            this.groupBox1.Controls.Add(this.lv_kids);
            this.groupBox1.Controls.Add(this.tb_phone);
            this.groupBox1.Controls.Add(this.b_changeCard);
            this.groupBox1.Controls.Add(this.rb_note);
            this.groupBox1.Controls.Add(this.dtp_birthday);
            this.groupBox1.Controls.Add(this.b_addKid);
            this.groupBox1.Controls.Add(this.cb_livePlace);
            this.groupBox1.Controls.Add(this.cb_workPlace);
            this.groupBox1.Controls.Add(this.b_photo);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tb_card);
            this.groupBox1.Controls.Add(this.b_ok);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rb_woman);
            this.groupBox1.Controls.Add(this.rb_man);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cb_status);
            this.groupBox1.Controls.Add(this.tb_sale);
            this.groupBox1.Controls.Add(this.tb_email);
            this.groupBox1.Controls.Add(this.tb_thirdName);
            this.groupBox1.Controls.Add(this.tb_name);
            this.groupBox1.Controls.Add(this.tb_family);
            this.groupBox1.Controls.Add(this.pb_photo);
            this.groupBox1.Location = new System.Drawing.Point(12, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(710, 473);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Клиент";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(23, 457);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(152, 13);
            this.label25.TabIndex = 65;
            this.label25.Text = "- обязательно к заполнению";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(15, 457);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 13);
            this.label24.TabIndex = 64;
            this.label24.Text = "*";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(364, 171);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 63;
            this.label23.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(564, 83);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 13);
            this.label21.TabIndex = 61;
            this.label21.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(281, 83);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 13);
            this.label19.TabIndex = 59;
            this.label19.Text = "*";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(655, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 13);
            this.label18.TabIndex = 58;
            this.label18.Text = "*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(531, 35);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 13);
            this.label17.TabIndex = 57;
            this.label17.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(380, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 13);
            this.label12.TabIndex = 56;
            this.label12.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(285, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 13);
            this.label11.TabIndex = 55;
            this.label11.Text = "*";
            // 
            // b_photoFromCamera
            // 
            this.b_photoFromCamera.Location = new System.Drawing.Point(112, 292);
            this.b_photoFromCamera.Name = "b_photoFromCamera";
            this.b_photoFromCamera.Size = new System.Drawing.Size(102, 39);
            this.b_photoFromCamera.TabIndex = 54;
            this.b_photoFromCamera.Text = "Фото с веб-камеры";
            this.b_photoFromCamera.UseVisualStyleBackColor = true;
            this.b_photoFromCamera.Click += new System.EventHandler(this.b_photoFromCamera_Click);
            // 
            // cb_typeOfDiscount
            // 
            this.cb_typeOfDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_typeOfDiscount.FormattingEnabled = true;
            this.cb_typeOfDiscount.Items.AddRange(new object[] {
            "%",
            "Рублей"});
            this.cb_typeOfDiscount.Location = new System.Drawing.Point(461, 277);
            this.cb_typeOfDiscount.Name = "cb_typeOfDiscount";
            this.cb_typeOfDiscount.Size = new System.Drawing.Size(90, 21);
            this.cb_typeOfDiscount.TabIndex = 53;
            // 
            // lv_kids
            // 
            this.lv_kids.FullRowSelect = true;
            this.lv_kids.GridLines = true;
            this.lv_kids.Location = new System.Drawing.Point(296, 191);
            this.lv_kids.Name = "lv_kids";
            this.lv_kids.Size = new System.Drawing.Size(181, 68);
            this.lv_kids.TabIndex = 52;
            this.lv_kids.UseCompatibleStateImageBehavior = false;
            this.lv_kids.View = System.Windows.Forms.View.Details;
            // 
            // tb_phone
            // 
            this.tb_phone.Location = new System.Drawing.Point(231, 99);
            this.tb_phone.Mask = "+79990000000";
            this.tb_phone.Name = "tb_phone";
            this.tb_phone.Size = new System.Drawing.Size(120, 20);
            this.tb_phone.TabIndex = 51;
            // 
            // b_changeCard
            // 
            this.b_changeCard.Location = new System.Drawing.Point(360, 311);
            this.b_changeCard.Name = "b_changeCard";
            this.b_changeCard.Size = new System.Drawing.Size(104, 39);
            this.b_changeCard.TabIndex = 50;
            this.b_changeCard.Text = "Изменить номер карты";
            this.b_changeCard.UseVisualStyleBackColor = true;
            this.b_changeCard.Click += new System.EventHandler(this.b_changeCard_Click);
            // 
            // rb_note
            // 
            this.rb_note.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rb_note.Location = new System.Drawing.Point(8, 357);
            this.rb_note.Name = "rb_note";
            this.rb_note.Size = new System.Drawing.Size(679, 67);
            this.rb_note.TabIndex = 49;
            this.rb_note.Text = "";
            // 
            // dtp_birthday
            // 
            this.dtp_birthday.Location = new System.Drawing.Point(484, 99);
            this.dtp_birthday.Name = "dtp_birthday";
            this.dtp_birthday.Size = new System.Drawing.Size(119, 20);
            this.dtp_birthday.TabIndex = 48;
            // 
            // b_addKid
            // 
            this.b_addKid.Location = new System.Drawing.Point(492, 213);
            this.b_addKid.Name = "b_addKid";
            this.b_addKid.Size = new System.Drawing.Size(106, 23);
            this.b_addKid.TabIndex = 45;
            this.b_addKid.Text = "Добавить";
            this.b_addKid.UseVisualStyleBackColor = true;
            this.b_addKid.Click += new System.EventHandler(this.b_addKid_Click);
            // 
            // cb_livePlace
            // 
            this.cb_livePlace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_livePlace.FormattingEnabled = true;
            this.cb_livePlace.Location = new System.Drawing.Point(385, 164);
            this.cb_livePlace.Name = "cb_livePlace";
            this.cb_livePlace.Size = new System.Drawing.Size(218, 21);
            this.cb_livePlace.TabIndex = 44;
            this.cb_livePlace.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // cb_workPlace
            // 
            this.cb_workPlace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_workPlace.FormattingEnabled = true;
            this.cb_workPlace.Location = new System.Drawing.Point(385, 137);
            this.cb_workPlace.Name = "cb_workPlace";
            this.cb_workPlace.Size = new System.Drawing.Size(218, 21);
            this.cb_workPlace.TabIndex = 43;
            this.cb_workPlace.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // b_photo
            // 
            this.b_photo.Location = new System.Drawing.Point(6, 292);
            this.b_photo.Name = "b_photo";
            this.b_photo.Size = new System.Drawing.Size(100, 39);
            this.b_photo.TabIndex = 42;
            this.b_photo.Text = "Фото из файла";
            this.b_photo.UseVisualStyleBackColor = true;
            this.b_photo.Click += new System.EventHandler(this.b_photo_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(231, 305);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "Номер карты";
            // 
            // tb_card
            // 
            this.tb_card.Location = new System.Drawing.Point(231, 321);
            this.tb_card.Name = "tb_card";
            this.tb_card.ReadOnly = true;
            this.tb_card.Size = new System.Drawing.Size(120, 20);
            this.tb_card.TabIndex = 40;
            this.tb_card.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // b_ok
            // 
            this.b_ok.Enabled = false;
            this.b_ok.Location = new System.Drawing.Point(302, 430);
            this.b_ok.Name = "b_ok";
            this.b_ok.Size = new System.Drawing.Size(89, 31);
            this.b_ok.TabIndex = 39;
            this.b_ok.Text = "Ok";
            this.b_ok.UseVisualStyleBackColor = true;
            this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(631, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "Пол";
            // 
            // rb_woman
            // 
            this.rb_woman.AutoSize = true;
            this.rb_woman.Location = new System.Drawing.Point(631, 69);
            this.rb_woman.Name = "rb_woman";
            this.rb_woman.Size = new System.Drawing.Size(72, 17);
            this.rb_woman.TabIndex = 37;
            this.rb_woman.TabStop = true;
            this.rb_woman.Text = "Женский";
            this.rb_woman.UseVisualStyleBackColor = true;
            this.rb_woman.CheckedChanged += new System.EventHandler(this.EnabledOk);
            // 
            // rb_man
            // 
            this.rb_man.AutoSize = true;
            this.rb_man.Location = new System.Drawing.Point(631, 46);
            this.rb_man.Name = "rb_man";
            this.rb_man.Size = new System.Drawing.Size(71, 17);
            this.rb_man.TabIndex = 36;
            this.rb_man.TabStop = true;
            this.rb_man.Text = "Мужской";
            this.rb_man.UseVisualStyleBackColor = true;
            this.rb_man.CheckedChanged += new System.EventHandler(this.EnabledOk);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 341);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Примечание";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(566, 262);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Статус";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(354, 262);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Личная скидка";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(231, 213);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Дети";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(231, 172);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Район места жительства";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(231, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Район места работы";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(480, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Дата рождения";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(357, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "e-mail";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(231, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Телефон";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(480, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Отчество";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(354, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Имя";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(231, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Фамилия";
            // 
            // cb_status
            // 
            this.cb_status.Cursor = System.Windows.Forms.Cursors.Default;
            this.cb_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_status.FormattingEnabled = true;
            this.cb_status.Items.AddRange(new object[] {
            "Активный",
            "Неактивный",
            "Архивный"});
            this.cb_status.Location = new System.Drawing.Point(569, 278);
            this.cb_status.Name = "cb_status";
            this.cb_status.Size = new System.Drawing.Size(120, 21);
            this.cb_status.TabIndex = 13;
            this.cb_status.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_sale
            // 
            this.tb_sale.Location = new System.Drawing.Point(357, 278);
            this.tb_sale.Name = "tb_sale";
            this.tb_sale.Size = new System.Drawing.Size(98, 20);
            this.tb_sale.TabIndex = 11;
            this.tb_sale.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_email
            // 
            this.tb_email.Location = new System.Drawing.Point(357, 99);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(120, 20);
            this.tb_email.TabIndex = 5;
            // 
            // tb_thirdName
            // 
            this.tb_thirdName.Location = new System.Drawing.Point(483, 52);
            this.tb_thirdName.Name = "tb_thirdName";
            this.tb_thirdName.Size = new System.Drawing.Size(120, 20);
            this.tb_thirdName.TabIndex = 3;
            this.tb_thirdName.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(357, 52);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(120, 20);
            this.tb_name.TabIndex = 2;
            this.tb_name.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_family
            // 
            this.tb_family.Location = new System.Drawing.Point(231, 52);
            this.tb_family.Name = "tb_family";
            this.tb_family.Size = new System.Drawing.Size(120, 20);
            this.tb_family.TabIndex = 1;
            this.tb_family.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // pb_photo
            // 
            this.pb_photo.Location = new System.Drawing.Point(6, 19);
            this.pb_photo.Name = "pb_photo";
            this.pb_photo.Size = new System.Drawing.Size(208, 265);
            this.pb_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_photo.TabIndex = 0;
            this.pb_photo.TabStop = false;
            // 
            // ofd_photo
            // 
            this.ofd_photo.FileName = "openFileDialog1";
            // 
            // EditClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 491);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditClient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_photo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rb_woman;
        private System.Windows.Forms.RadioButton rb_man;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_status;
        private System.Windows.Forms.TextBox tb_sale;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.TextBox tb_thirdName;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.TextBox tb_family;
        private System.Windows.Forms.PictureBox pb_photo;
        private System.Windows.Forms.TextBox tb_card;
        private System.Windows.Forms.Button b_ok;
        private System.Windows.Forms.Button b_photo;
        private System.Windows.Forms.ComboBox cb_livePlace;
        private System.Windows.Forms.ComboBox cb_workPlace;
        private System.Windows.Forms.Button b_addKid;
        private System.Windows.Forms.DateTimePicker dtp_birthday;
        private System.Windows.Forms.OpenFileDialog ofd_photo;
        private System.Windows.Forms.RichTextBox rb_note;
        private System.Windows.Forms.Button b_changeCard;
        private System.Windows.Forms.MaskedTextBox tb_phone;
        private System.Windows.Forms.ListView lv_kids;
        private System.Windows.Forms.ComboBox cb_typeOfDiscount;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button b_photoFromCamera;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
    }
}