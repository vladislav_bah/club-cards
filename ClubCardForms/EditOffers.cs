﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class EditOffers : Form
    {
        private DataBase BD;
        public EditOffers(DataBase BD)
        {
            InitializeComponent();
            this.BD = BD;
            fill();
        }

        private void fill()
        {
            try
            {
                int rowsCount = dgv_Offers.Rows.Count;
                for (int i = 0; i < rowsCount; i++)
                {
                    dgv_Offers.Rows.Remove(dgv_Offers.Rows[0]);
                }
                DataSet ds = BD.Select("Select ID as 'ID', Name as 'Название', NumberOfVisits as 'Количество посещений',duration as 'Длительность',Price as 'Цена', " +
                "Status as 'Статус', toarhivedate as 'Дата занесения в архив', type as 'Тип' from Offers");
                dgv_Offers.DataSource = ds.Tables[0].DefaultView;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (dgv_Offers.Rows[i].Cells[5].Value.ToString() == "1")
                    {
                        dgv_Offers.Rows[i].Cells[5].Value = "Активна";
                    }
                    else
                    {
                        dgv_Offers.Rows[i].Cells[5].Value = "Не активна";
                    }

                    if (dgv_Offers.Rows[i].Cells[7].Value.ToString() == "1")
                    {
                        dgv_Offers.Rows[i].Cells[7].Value = "Вечерняя";
                    }
                    else
                    {
                        dgv_Offers.Rows[i].Cells[7].Value = "Дневная";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения списка клиентов. Error: " + ex.Message);
            }

        }

        private void dgv_Offers_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            string query = "update Offers set Name ='" + dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[1].Value.ToString() + "', NumberOfVisits =" + dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[2].Value.ToString() +
                    ",Duration ='" + dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[3].Value.ToString() + "',Price =" + dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[4].Value.ToString() + ",Status=" + dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[5].Value.ToString() + ",ToArhiveDate = '"
                    + dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[6].Value.ToString() + "', type = "+ dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[7].Value.ToString() + " where ID=" + dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[0].Value.ToString();
            try
            {
                BD.Query(query);
                MessageBox.Show("Услуга изменена успешно!");
                fill();
            }
            catch
            {
                MessageBox.Show("Ошибка изменения услуги");
            }
        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            AddOffer add = new AddOffer(BD);
            add.ShowDialog();
            fill();
        }

        private void bt_delete_Click(object sender, EventArgs e)
        {
            string query = "update Offers set Status = 0, ToArhiveDate ='"+ DateTime.Today.ToLongDateString() +"' where ID=" + dgv_Offers.Rows[dgv_Offers.CurrentRow.Index].Cells[0].Value.ToString();
            try
            {
                BD.Query(query);
                MessageBox.Show("Услуга добавлена в архив!");
                fill();
            }
            catch
            {
                MessageBox.Show("Ошибка добавления услуги в архив");
            }
        }
    }
}
