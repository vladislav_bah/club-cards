﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class freezeForm : Form
    {
        private DataBase DB;
        private int SubID;
        private int id;
        private MainForm mainform;
        public freezeForm(DataBase DB, int SubID, int id, MainForm mainform)
        {
            this.DB = DB;
            this.id = id;
            this.SubID = SubID;
            this.mainform = mainform;
            InitializeComponent();
            dtp_freezeEndDate.Value = dtp_freezeEndDate.Value.AddMonths(1);
        }

        private void b_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void b_ok_Click(object sender, EventArgs e)
        {
            DataSet endDate = DB.Select("Select EndDate, status, FreezingEndDate from Subs where ID = " + SubID);
            DateTime end = DateTime.Parse(Convert.ToString(endDate.Tables[0].DefaultView[0][0]));
            if (Convert.ToInt32(endDate.Tables[0].DefaultView[0][1].ToString()) != 0)
            {
                DB.Query("Update Subs set freezingStartDate = '" + dtp_freezeStartDate.Value.ToLongDateString() + "', freezingEndDate = '" +
                    dtp_freezeEndDate.Value.ToLongDateString() + "', Status = 0, EndDate = '" +
                    (dtp_freezeEndDate.Value + (end - dtp_freezeStartDate.Value)).AddDays(1).ToLongDateString() + "' " +
                    "where ID = " + SubID);
            }
            else
            {
                DateTime FrEnd = DateTime.Parse(Convert.ToString(endDate.Tables[0].DefaultView[0][2]));
                DB.Query("Update Subs set freezingStartDate = '" + dtp_freezeStartDate.Value.ToLongDateString() + "', freezingEndDate = '" +
                    dtp_freezeEndDate.Value.ToLongDateString() + "', Status = 0, EndDate = '" +
                    (end - (FrEnd - dtp_freezeEndDate.Value)).ToLongDateString() + "' " +
                    "where ID = " + SubID);
            }
            mainform.Fillform(id);
            mainform.fillAbonement(Convert.ToString(SubID));
            this.Close();
        }
    }
}
