﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class Prolongation : Form
    {
        private DataBase BD;
        private String PaymentDate = DateTime.Now.ToShortDateString();
        private string offer_name;
        private string abonement_ID;
        private string dateOfEnd;
        private string numberOfVisits;
        private int price;
        string duration;
        DataSet stocks;
        private MainForm mainform;

        public Prolongation(DataBase BD, MainForm mainform, string offer_name, string abonement_ID, string dateOfEnd)
        {
            InitializeComponent();
            this.BD = BD;
            this.mainform = mainform;
            this.offer_name = offer_name;
            this.abonement_ID = abonement_ID;
            this.dateOfEnd = dateOfEnd;
            fill_abonement();
        }

        void fill_abonement()
        {
            string time = dateOfEnd;
            string fullname = offer_name;
            l_offer.Text = fullname;
            if (DateTime.Today > Convert.ToDateTime(dateOfEnd))
                time = DateTime.Today.ToLongDateString();

            try
            {
                DataSet offer_ID = BD.Select("Select OfferID, offers.duration from Subs, offers where subs.id = " + abonement_ID);
                duration = offer_ID.Tables[0].DefaultView[0][1].ToString();
                time = Convert.ToDateTime(time).AddMonths(Convert.ToInt32(duration)).ToLongDateString();
                tb_dateOfEnd.Text = time;
                price = Convert.ToInt32(BD.Select("Select price from offers where id = " + offer_ID.Tables[0].DefaultView[0][0]).Tables[0].DefaultView[0][0]);

                stocks = BD.Select("Select stock.name, Discount, stock.type from stock, offers, Offers_Stock where Offers.ID = " + offer_ID.Tables[0].DefaultView[0][0]
                    + " and offers.Id = Offers_Stock.Offer_ID and  Offers_Stock.Stock_ID = Stock.ID");

                cb_stock.Items.Add("Нет");
                foreach (DataRow stock in stocks.Tables[0].Rows)
                {
                    cb_stock.Items.Add(stock.ItemArray[0]);
                }

                cb_stock.SelectedIndex = 0;
            }
            catch
            {
                MessageBox.Show("Ошибка заполнения формы");
            }
            miner();
        }

        private void miner()
        {
            if (cb_stock.SelectedItem.ToString() == "Нет")
            {
                l_price.Text = Convert.ToString(price);
                l_stock.Text = "Скидка: нет";
            }
            else
            {
                if ((int)stocks.Tables[0].DefaultView[cb_stock.SelectedIndex - 1][2] == 0)
                {
                    l_price.Text = Convert.ToString(price - Convert.ToInt32(stocks.Tables[0].DefaultView[cb_stock.SelectedIndex - 1][1]));
                    l_stock.Text = "Скидка: " + stocks.Tables[0].DefaultView[cb_stock.SelectedIndex - 1][1]+" Р.";
                }
                else
                {
                    l_price.Text = Convert.ToString(price * (100 - Convert.ToInt32(stocks.Tables[0].DefaultView[cb_stock.SelectedIndex - 1][1])) / 100);
                    l_stock.Text = "Скидка: " + stocks.Tables[0].DefaultView[cb_stock.SelectedIndex - 1][1] + " %.";
                }
            }
        }
        private void b_ok_Click(object sender, EventArgs e)
        {
            string StartTime = dateOfEnd;
            DataSet NumberOfVis = BD.Select("Select numberOfVisits from Subs, offers where subs.id = " + abonement_ID + " and subs.offerID = Offers.ID");
            numberOfVisits = NumberOfVis.Tables[0].DefaultView[0][0].ToString();
            if (DateTime.Today > Convert.ToDateTime(dateOfEnd))
                StartTime = DateTime.Today.ToLongDateString();
            try
            {
                BD.Query("update subs set EndDate = '" + Convert.ToDateTime(StartTime).AddMonths(Convert.ToInt32(duration)).ToLongDateString() + "', " +
                    "Status = 1, countOfVisits = 0 where id =" + abonement_ID );
                int PaymentType;
                if (rb_nal.Checked)
                {
                    PaymentType = 0;
                }
                else
                {
                    PaymentType = 1;
                }
                BD.Query("Insert into PaymentHistory(OfferName, Cost, PaymentDate, SubID, PaymentType) values('" + offer_name +
                    "', " + l_price.Text + ", '" + PaymentDate + "', " + abonement_ID + ", " + PaymentType + " )");
            }
            catch
            {
                MessageBox.Show("Ошибка редактирования услуги");
            }
            Close();
        }

        private void cb_stock_SelectedIndexChanged(object sender, EventArgs e)
        {
            miner();
        }
    }
}
