﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class editAbonement : Form
    {
        private DataBase BD;
        private String PaymentDate = DateTime.Now.ToShortDateString();
        private int id = -1;
        private int AbonemID = -1;
        private string service_name;
        private string stock_name;
        private MainForm mainform;
        int per_discount = 0;
        int per_type = 0;
        private Dictionary<string, List<string>> dic = new Dictionary<string, List<string>>();
        public editAbonement(int id, DataBase BD, MainForm mainform)
        {
            InitializeComponent();
            this.BD = BD;
            this.id = id;
            this.mainform = mainform;
            Text = "Добавление абонемента";
            fill_abonement();
        }

        public editAbonement(int id, int AbonemID, string service_name, string stock_name, DataBase BD, MainForm mainform,string name)
        {
            InitializeComponent();
            this.BD = BD;
            this.id = id;
            this.AbonemID = AbonemID;
            this.mainform = mainform;
            this.service_name = service_name;
            this.stock_name = stock_name;
            Text = name;
            if (name == "Продление абонемента")
            {
                cb_status.Enabled = false;
                cb_serviceName.Enabled = false;
                dtp_dateOfActivation.Enabled = false;
            }
            fill_abonement();
            l_price.Visible = false;
            l_stock.Visible = false;
            label2.Visible = false;
        }


        private void fill_abonement()
        {
            cb_status.SelectedIndex = 1;
            tb_finishedExercise.Text = "0";
            try
            {
                DataSet per_sale = BD.Select("Select PersonalDiscount, typeOfDiscount from Clients where id = " + Convert.ToString(id));

                per_discount = Convert.ToInt32(per_sale.Tables[0].DefaultView[0][0].ToString());
                per_type = Convert.ToInt32(per_sale.Tables[0].DefaultView[0][1].ToString());
                if (per_sale.Tables[0].DefaultView[0][1].ToString() == "0")
                {
                    cb_person.Text = "Использовать личную скидку " + per_sale.Tables[0].DefaultView[0][0].ToString() + "%";                   
                }
                else
                {
                    cb_person.Text = "Использовать личную скидку " + per_sale.Tables[0].DefaultView[0][0].ToString() + " рублей";
                }

                DataSet ds = BD.Select("select name, numberofvisits,duration, type, price, ID from offers where Status =1");
                foreach (DataRow offer in ds.Tables[0].Rows) 
                {
                    if (offer.ItemArray[1].ToString() != "-1")
                    {
                        string type;
                        if (offer.ItemArray[3].ToString() == "0")
                        {
                            type = "дневной";
                        }
                        else
                        {
                            type = "вечерний";
                        }
                        cb_serviceName.Items.Add(offer.ItemArray[0].ToString() + " (" + offer.ItemArray[1].ToString() + " посещений, " + type + ")");
                        List<string> tmp = new List<string>();
                        tmp.Add(offer.ItemArray[1].ToString());
                        tmp.Add(offer.ItemArray[2].ToString());
                        tmp.Add(offer.ItemArray[0].ToString());
                        tmp.Add(offer.ItemArray[4].ToString());
                        tmp.Add(offer.ItemArray[5].ToString());
                        tmp.Add(offer.ItemArray[3].ToString());
                        dic.Add(offer.ItemArray[0].ToString() + " (" + offer.ItemArray[1].ToString() + " посещений, " + type + ")", tmp);
                    }
                    else
                    {
                        string type;
                        if (offer.ItemArray[3].ToString() == "0")
                        {
                            type = "дневной";
                        }
                        else
                        {
                            type = "вечерний";
                        }
                        cb_serviceName.Items.Add(offer.ItemArray[0].ToString() + " (месяц, " + type + ")");
                        List<string> tmp = new List<string>();
                        tmp.Add(offer.ItemArray[1].ToString());
                        tmp.Add(offer.ItemArray[2].ToString());
                        tmp.Add(offer.ItemArray[0].ToString());
                        tmp.Add(offer.ItemArray[4].ToString());
                        tmp.Add(offer.ItemArray[5].ToString());
                        tmp.Add(offer.ItemArray[3].ToString());
                        dic.Add(offer.ItemArray[0].ToString() + " (месяц, " + type + ")", tmp);
                    }
                }
                if (this.Text == "Редактирование абонемента" || this.Text == "Продление абонемента")
                {
                    l_stock.Text = "";
                    l_price.Text = "";
                    label2.Text = "";
                    ds = BD.Select("select * from subs where ID =" + AbonemID);
                    for (int i = 0; i < cb_serviceName.Items.Count; i++)
                    {
                        if (cb_serviceName.Items[i].ToString() == service_name)
                        {
                            cb_serviceName.SelectedIndex = i;
                            break;
                        }
                    }

                    for (int i = 0; i < cb_stock.Items.Count; i++)
                    {
                        if (cb_stock.Items[i].ToString() == stock_name)
                        {
                            cb_stock.SelectedIndex = i;
                            break;
                        }
                    }

                    cb_status.Enabled = true;
                    tb_finishedExercise.ReadOnly = false;
                    //cb_serviceName.Enabled = false;
                    tb_finishedExercise.Text = ds.Tables[0].DefaultView[0][3].ToString();
                    dtp_dateOfActivation.Text = ds.Tables[0].DefaultView[0][5].ToString();
                    dtp_firstVisit.Text = ds.Tables[0].DefaultView[0][4].ToString();
                    dtp_dateOfEnd.Text = ds.Tables[0].DefaultView[0][6].ToString();
                    cb_status.SelectedIndex = Convert.ToInt32(ds.Tables[0].DefaultView[0][2].ToString());
                    if(tb_countOfExercise.Text != "Не ограничено")
                    {
                        tb_resrOfExercise.Text = Convert.ToString(Convert.ToInt32(tb_countOfExercise.Text) - Convert.ToInt32(ds.Tables[0].DefaultView[0][3]));
                    }
                }
                if (this.Text == "Продление абонемента")
                {
                    cb_status.Enabled = false;
                    tb_finishedExercise.ReadOnly = true;
                    dtp_dateOfEnd.Enabled = false;
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения абонемента. Перезапустите приложение. Error:" + ex.Message);
            }
        }

        private void cb_serviceName_TextChanged(object sender, EventArgs e)
        {
            if (dic[cb_serviceName.Text][0] != "-1")
            {
                tb_countOfExercise.Text = dic[cb_serviceName.Text][0];
                tb_resrOfExercise.Text = dic[cb_serviceName.Text][0];
            }
            else
            {
                tb_countOfExercise.Text = "Не ограничено";
                tb_resrOfExercise.Text = "Не ограничено";
            }
            l_price.Text = dic[cb_serviceName.Text][3];

            cb_stock.Items.Clear();
            //MessageBox.Show(dic[cb_serviceName.Text][2]);
            try
            {
                DataSet stocks = BD.Select("Select stock.name from stock, offers, Offers_Stock where Offers.ID = " + dic[cb_serviceName.Text][4] + " and offers.Id = Offers_Stock.Offer_ID and  Offers_Stock.Stock_ID = Stock.ID");
                cb_stock.Enabled = true;
                cb_person.Enabled = true;
                if (stocks != null)
                {
                    cb_stock.Items.Add("Нет");
                    foreach (DataRow stock in stocks.Tables[0].Rows)
                    {
                        cb_stock.Items.Add(stock.ItemArray[0]);
                    }
                    cb_stock.SelectedIndex = 0;
                }
            }
            catch
            {
                MessageBox.Show("Ошибка обновления формы");
            }


        }

        private void b_ok_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(cb_stock.SelectedItem.ToString());
            if (Text == "Добавление абонемента")
            {
                try
                {
                    
                    DateTime time;
                    DataSet ds = BD.Select("Select id, duration from offers where name ='"+dic[cb_serviceName.Text][2]+ "'" +
                        " and NumberOfVisits = " + dic[cb_serviceName.Text][0] + " and type = " + dic[cb_serviceName.Text][5]);
                    DataSet stock = BD.Select("Select id from stock where name = '" + cb_stock.SelectedItem.ToString() + "'");    

                    time = DateTime.Today.AddMonths(Convert.ToInt32(ds.Tables[0].DefaultView[0][1]));

                    int st = -1;
                    if (!(cb_stock.SelectedIndex == 0))
                    {
                        st = Convert.ToInt32(stock.Tables[0].DefaultView[0][0]);
                    }
                    BD.Query("insert into Subs(OfferID,Status,CountOfVisits,ActivationDate,EndDate, stockID) values("+ds.Tables[0].DefaultView[0][0]+
                        ",1,0,'"+dtp_dateOfActivation.Text+"','"+ time.Date.ToLongDateString() + "', " + st + ")");
                    ds = BD.Select("Select max(id) from Subs");
                    BD.Query("insert into Clients_Subs values("+id+","+ds.Tables[0].DefaultView[0][0]+")");
                    mainform.fillabonementList();

                    int PaymentType;
                    if (rb_nal.Checked)
                    {
                        PaymentType = 0;
                    }
                    else
                    {
                        PaymentType = 1;
                    }
                    BD.Query("Insert into PaymentHistory(OfferName, Cost, PaymentDate, SubID, PaymentType) values('" + cb_serviceName.SelectedItem.ToString() + 
                        "', " + l_price.Text + ", '" + PaymentDate + "', " + ds.Tables[0].DefaultView[0][0] + ", " + PaymentType + " )");
                }
                catch(Exception Ex)
                {
                    MessageBox.Show("Ошибка добавления абонемента. Error:"+Ex.Message);
                }
                Close();
            }

            if (Text == "Редактирование абонемента")
            {
                try
                {
                    DataSet ds = BD.Select("Select id, duration from offers where ID ='" + dic[cb_serviceName.Text][4] + "'");
                    DataSet stock = BD.Select("Select id from stock where name = '" + cb_stock.SelectedItem.ToString() + "'");
                    int st = -1;
                    if (!(cb_stock.SelectedIndex == 0))
                    {
                        st = Convert.ToInt32(stock.Tables[0].DefaultView[0][0]);
                    }

                    BD.Query("Update Subs set " +
                        " OfferID = " + ds.Tables[0].DefaultView[0][0] +
                        ", Status = " + cb_status.SelectedIndex +
                        ", CountOfVisits = " + tb_finishedExercise.Text +
                        ", ActivationDate = '" + dtp_dateOfActivation.Text +
                        "', EndDate = '" + dtp_dateOfEnd.Text +
                        "', stockID = " + st +
                        " where ID = " + AbonemID);
                    mainform.fillabonementList();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show("Ошибка добавления абонемента. Error:" + Ex.Message);
                }
                Close();
            }

            if(Text=="Продление абонемента")
            {
                DateTime time;
                DataSet ds = BD.Select("Select id, duration from offers where name ='" + dic[cb_serviceName.Text][2] + "'");
                DataSet endday = BD.Select("Select EndDate,FirstVisitDate from subs where id ='" + AbonemID +"'");

                time = Convert.ToDateTime(endday.Tables[0].DefaultView[0][0]).AddMonths(Convert.ToInt32(ds.Tables[0].DefaultView[0][1]));

                BD.Query("Update Subs set EndDate = '" + time.Date.ToLongDateString() +
                  "', Status = " + cb_status.SelectedIndex + " where ID = " + AbonemID);
                mainform.fillabonementList();
                Close();
            }
        }

        private void cb_stock_TextChanged(object sender, EventArgs e)
        {
            if (cb_stock.SelectedItem.ToString() != "Нет" && Text == "Добавление абонемента")
            {
                cb_person.Checked = false;
                DataSet stocks = BD.Select("select discount,type from stock where Name = '" + cb_stock.SelectedItem.ToString() + "'");
                if (Convert.ToInt32(stocks.Tables[0].DefaultView[0][1]) == 1)
                {
                    l_stock.Text = "Cкидка:" + Convert.ToInt32(stocks.Tables[0].DefaultView[0][0]) + "%";
                    l_price.Text = Convert.ToString(Convert.ToInt32(dic[cb_serviceName.Text][3]) * (100 - Convert.ToInt32(stocks.Tables[0].DefaultView[0][0])) / 100);
                }
                else
                {
                    l_stock.Text = "Cкидка:" + Convert.ToInt32(stocks.Tables[0].DefaultView[0][0]) + " руб.";
                    l_price.Text = Convert.ToString(Convert.ToInt32(dic[cb_serviceName.Text][3]) - Convert.ToInt32(stocks.Tables[0].DefaultView[0][0].ToString()));
                }
                if (Convert.ToInt32(l_price.Text) < 0)
                {
                    l_price.Text = "0";
                }
            }
            else if(Text == "Добавление абонемента")
            {
                l_stock.Text = "Cкидка: 0";
                l_price.Text = dic[cb_serviceName.Text][3];
            }
            
        }

        private void tb_finishedExercise_TextChanged(object sender, EventArgs e)
        {
            if (tb_resrOfExercise.Text != "Не ограничено"&& tb_finishedExercise.Text != "" && tb_countOfExercise.Text != "" && Convert.ToInt32(tb_finishedExercise.Text) > (Convert.ToInt32(tb_countOfExercise.Text)))
            {
                tb_finishedExercise.Text = tb_countOfExercise.Text;
            }

            if (tb_finishedExercise.Text != "" && tb_countOfExercise.Text != "" && tb_resrOfExercise.Text != "Не ограничено")
            {
                tb_resrOfExercise.Text = Convert.ToString((Convert.ToInt32(tb_countOfExercise.Text)) - Convert.ToInt32(tb_finishedExercise.Text));
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_person.Checked)
            {
                cb_stock.SelectedIndex =0;
                if (per_type == 0)
                {
                    l_price.Text = Convert.ToString(Convert.ToInt32(dic[cb_serviceName.Text][3]) * ((100 - per_discount) / 100));
                }
                else
                {
                    l_price.Text = Convert.ToString(Convert.ToInt32(dic[cb_serviceName.Text][3]) - per_discount);
                }
                if (Convert.ToInt32(l_price.Text) < 0)
                {
                    l_price.Text = "0";
                }
            }
            else
            {
                if (cb_stock.SelectedItem.ToString() != "Нет" && Text == "Добавление абонемента")
                {
                    DataSet stocks = BD.Select("select discount,type from stock where Name = '" + cb_stock.SelectedItem.ToString() + "'");
                    if (Convert.ToInt32(stocks.Tables[0].DefaultView[0][1]) == 1)
                    {
                        l_stock.Text = "Cкидка:" + Convert.ToInt32(stocks.Tables[0].DefaultView[0][0]) + "%";
                        l_price.Text = Convert.ToString(Convert.ToInt32(dic[cb_serviceName.Text][3]) * (100 - Convert.ToInt32(stocks.Tables[0].DefaultView[0][0])) / 100);
                    }
                    else
                    {
                        l_stock.Text = "Cкидка:" + Convert.ToInt32(stocks.Tables[0].DefaultView[0][0]) + " руб.";
                        l_price.Text = Convert.ToString(Convert.ToInt32(dic[cb_serviceName.Text][3]) - Convert.ToInt32(stocks.Tables[0].DefaultView[0][0].ToString()));
                    }
                    if (Convert.ToInt32(l_price.Text) < 0)
                    {
                        l_price.Text = "0";
                    }
                }
                else if (Text == "Добавление абонемента")
                {
                    l_stock.Text = "Cкидка: 0";
                    l_price.Text = dic[cb_serviceName.Text][3];
                }
            }
        }


    }
}
