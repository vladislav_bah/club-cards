﻿namespace ClubCardForms
{
    partial class EditOffers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditOffers));
            this.dgv_Offers = new System.Windows.Forms.DataGridView();
            this.bt_add = new System.Windows.Forms.Button();
            this.bt_delete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Offers)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_Offers
            // 
            this.dgv_Offers.AllowUserToAddRows = false;
            this.dgv_Offers.AllowUserToDeleteRows = false;
            this.dgv_Offers.AllowUserToResizeRows = false;
            this.dgv_Offers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Offers.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgv_Offers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Offers.Location = new System.Drawing.Point(12, 21);
            this.dgv_Offers.MultiSelect = false;
            this.dgv_Offers.Name = "dgv_Offers";
            this.dgv_Offers.Size = new System.Drawing.Size(687, 409);
            this.dgv_Offers.TabIndex = 1;
            this.dgv_Offers.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Offers_CellEndEdit);
            // 
            // bt_add
            // 
            this.bt_add.Location = new System.Drawing.Point(25, 437);
            this.bt_add.Name = "bt_add";
            this.bt_add.Size = new System.Drawing.Size(102, 31);
            this.bt_add.TabIndex = 2;
            this.bt_add.Text = "Добавить услугу";
            this.bt_add.UseVisualStyleBackColor = true;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // bt_delete
            // 
            this.bt_delete.Location = new System.Drawing.Point(133, 437);
            this.bt_delete.Name = "bt_delete";
            this.bt_delete.Size = new System.Drawing.Size(159, 31);
            this.bt_delete.TabIndex = 3;
            this.bt_delete.Text = "Перевести в Архив";
            this.bt_delete.UseVisualStyleBackColor = true;
            this.bt_delete.Click += new System.EventHandler(this.bt_delete_Click);
            // 
            // EditOffers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 491);
            this.Controls.Add(this.bt_delete);
            this.Controls.Add(this.bt_add);
            this.Controls.Add(this.dgv_Offers);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditOffers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование услуг";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Offers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_Offers;
        private System.Windows.Forms.Button bt_add;
        private System.Windows.Forms.Button bt_delete;
    }
}