﻿namespace ClubCardForms
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lv_kids = new System.Windows.Forms.ListView();
            this.lb_active = new System.Windows.Forms.Label();
            this.tb_sex = new System.Windows.Forms.TextBox();
            this.rtb_note = new System.Windows.Forms.RichTextBox();
            this.tb_birthday = new System.Windows.Forms.TextBox();
            this.tb_status = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.b_addAbonement = new System.Windows.Forms.Button();
            this.lv_abonements = new System.Windows.Forms.ListView();
            this.b_delete = new System.Windows.Forms.Button();
            this.b_editClient = new System.Windows.Forms.Button();
            this.tb_sale = new System.Windows.Forms.TextBox();
            this.tb_livePlace = new System.Windows.Forms.TextBox();
            this.tb_workPlace = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.tb_phone = new System.Windows.Forms.TextBox();
            this.tb_thirdName = new System.Windows.Forms.TextBox();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_family = new System.Windows.Forms.TextBox();
            this.pb_photo = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_addClient = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьПрофильКлиентаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьСписокУслугToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьАкциюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.статистикаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.статистикаПосещенийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.статистикаОплатToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.статистикаЗаДеньToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.выгрузитьСтатистикуЗаМесяцToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.тренераToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.расписаниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выборПортаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gb_abonement = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.bt_endExercise = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_stock = new System.Windows.Forms.TextBox();
            this.tb_endDate = new System.Windows.Forms.TextBox();
            this.tb_freezeStartDate = new System.Windows.Forms.TextBox();
            this.tb_freezeEndDate = new System.Windows.Forms.TextBox();
            this.tb_startDate = new System.Windows.Forms.TextBox();
            this.tb_activationDate = new System.Windows.Forms.TextBox();
            this.tb_restOfExercise = new System.Windows.Forms.TextBox();
            this.tb_finishedExercise = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.b_editAbonement = new System.Windows.Forms.Button();
            this.b_freeze = new System.Windows.Forms.Button();
            this.b_prolongation = new System.Windows.Forms.Button();
            this.b_startExercise = new System.Windows.Forms.Button();
            this.tb_freezeDaysRest = new System.Windows.Forms.TextBox();
            this.tb_abonementStatus = new System.Windows.Forms.TextBox();
            this.tb_countOfExercice = new System.Windows.Forms.TextBox();
            this.tb_type = new System.Windows.Forms.TextBox();
            this.tb_serviceName = new System.Windows.Forms.TextBox();
            this.t_clear = new System.Windows.Forms.Timer(this.components);
            this.lbBZ = new System.Windows.Forms.Label();
            this.lbMZ = new System.Windows.Forms.Label();
            this.lbSum = new System.Windows.Forms.Label();
            this.lbSumPerDay = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_photo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.gb_abonement.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lv_kids);
            this.groupBox1.Controls.Add(this.lb_active);
            this.groupBox1.Controls.Add(this.tb_sex);
            this.groupBox1.Controls.Add(this.rtb_note);
            this.groupBox1.Controls.Add(this.tb_birthday);
            this.groupBox1.Controls.Add(this.tb_status);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.b_addAbonement);
            this.groupBox1.Controls.Add(this.lv_abonements);
            this.groupBox1.Controls.Add(this.b_delete);
            this.groupBox1.Controls.Add(this.b_editClient);
            this.groupBox1.Controls.Add(this.tb_sale);
            this.groupBox1.Controls.Add(this.tb_livePlace);
            this.groupBox1.Controls.Add(this.tb_workPlace);
            this.groupBox1.Controls.Add(this.tb_email);
            this.groupBox1.Controls.Add(this.tb_phone);
            this.groupBox1.Controls.Add(this.tb_thirdName);
            this.groupBox1.Controls.Add(this.tb_name);
            this.groupBox1.Controls.Add(this.tb_family);
            this.groupBox1.Controls.Add(this.pb_photo);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(710, 513);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Клиент";
            // 
            // lv_kids
            // 
            this.lv_kids.FullRowSelect = true;
            this.lv_kids.GridLines = true;
            this.lv_kids.Location = new System.Drawing.Point(382, 188);
            this.lv_kids.Name = "lv_kids";
            this.lv_kids.Size = new System.Drawing.Size(222, 59);
            this.lv_kids.TabIndex = 68;
            this.lv_kids.UseCompatibleStateImageBehavior = false;
            this.lv_kids.View = System.Windows.Forms.View.Details;
            // 
            // lb_active
            // 
            this.lb_active.AutoSize = true;
            this.lb_active.Location = new System.Drawing.Point(460, 16);
            this.lb_active.Name = "lb_active";
            this.lb_active.Size = new System.Drawing.Size(0, 13);
            this.lb_active.TabIndex = 66;
            // 
            // tb_sex
            // 
            this.tb_sex.Location = new System.Drawing.Point(610, 49);
            this.tb_sex.Name = "tb_sex";
            this.tb_sex.ReadOnly = true;
            this.tb_sex.Size = new System.Drawing.Size(94, 20);
            this.tb_sex.TabIndex = 50;
            // 
            // rtb_note
            // 
            this.rtb_note.Location = new System.Drawing.Point(6, 446);
            this.rtb_note.Name = "rtb_note";
            this.rtb_note.ReadOnly = true;
            this.rtb_note.Size = new System.Drawing.Size(697, 55);
            this.rtb_note.TabIndex = 49;
            this.rtb_note.Text = "";
            // 
            // tb_birthday
            // 
            this.tb_birthday.Location = new System.Drawing.Point(484, 96);
            this.tb_birthday.Name = "tb_birthday";
            this.tb_birthday.ReadOnly = true;
            this.tb_birthday.Size = new System.Drawing.Size(120, 20);
            this.tb_birthday.TabIndex = 48;
            // 
            // tb_status
            // 
            this.tb_status.Location = new System.Drawing.Point(484, 267);
            this.tb_status.Name = "tb_status";
            this.tb_status.ReadOnly = true;
            this.tb_status.Size = new System.Drawing.Size(120, 20);
            this.tb_status.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(607, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "Пол";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 430);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Примечание";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(229, 307);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 34;
            this.label15.Text = "Абонементы";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(481, 250);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Статус";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(355, 250);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Личная скидка";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(232, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Дети";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(232, 169);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Район места жительства";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(232, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Район места работы";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(481, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Дата рождения";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(358, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "e-mail";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(232, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Телефон";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(481, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Отчество";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(355, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Имя";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Фамилия";
            // 
            // b_addAbonement
            // 
            this.b_addAbonement.Enabled = false;
            this.b_addAbonement.Location = new System.Drawing.Point(517, 362);
            this.b_addAbonement.Name = "b_addAbonement";
            this.b_addAbonement.Size = new System.Drawing.Size(144, 31);
            this.b_addAbonement.TabIndex = 18;
            this.b_addAbonement.Text = "Добавить абонемент";
            this.b_addAbonement.UseVisualStyleBackColor = true;
            this.b_addAbonement.Click += new System.EventHandler(this.b_addAbonement_Click);
            // 
            // lv_abonements
            // 
            this.lv_abonements.FullRowSelect = true;
            this.lv_abonements.GridLines = true;
            this.lv_abonements.Location = new System.Drawing.Point(232, 323);
            this.lv_abonements.Name = "lv_abonements";
            this.lv_abonements.Size = new System.Drawing.Size(279, 117);
            this.lv_abonements.TabIndex = 17;
            this.lv_abonements.UseCompatibleStateImageBehavior = false;
            this.lv_abonements.View = System.Windows.Forms.View.Details;
            this.lv_abonements.SelectedIndexChanged += new System.EventHandler(this.lv_abonements_SelectedIndexChanged);
            // 
            // b_delete
            // 
            this.b_delete.Enabled = false;
            this.b_delete.Location = new System.Drawing.Point(6, 380);
            this.b_delete.Name = "b_delete";
            this.b_delete.Size = new System.Drawing.Size(144, 31);
            this.b_delete.TabIndex = 16;
            this.b_delete.Text = "Удалить";
            this.b_delete.UseVisualStyleBackColor = true;
            this.b_delete.Click += new System.EventHandler(this.b_delete_Click);
            // 
            // b_editClient
            // 
            this.b_editClient.Enabled = false;
            this.b_editClient.Location = new System.Drawing.Point(6, 339);
            this.b_editClient.Name = "b_editClient";
            this.b_editClient.Size = new System.Drawing.Size(144, 31);
            this.b_editClient.TabIndex = 15;
            this.b_editClient.Text = "Редактировать";
            this.b_editClient.UseVisualStyleBackColor = true;
            this.b_editClient.Click += new System.EventHandler(this.b_editClient_Click);
            // 
            // tb_sale
            // 
            this.tb_sale.Location = new System.Drawing.Point(358, 267);
            this.tb_sale.Name = "tb_sale";
            this.tb_sale.ReadOnly = true;
            this.tb_sale.Size = new System.Drawing.Size(120, 20);
            this.tb_sale.TabIndex = 11;
            // 
            // tb_livePlace
            // 
            this.tb_livePlace.Location = new System.Drawing.Point(382, 162);
            this.tb_livePlace.Name = "tb_livePlace";
            this.tb_livePlace.ReadOnly = true;
            this.tb_livePlace.Size = new System.Drawing.Size(222, 20);
            this.tb_livePlace.TabIndex = 8;
            // 
            // tb_workPlace
            // 
            this.tb_workPlace.Location = new System.Drawing.Point(382, 136);
            this.tb_workPlace.Name = "tb_workPlace";
            this.tb_workPlace.ReadOnly = true;
            this.tb_workPlace.Size = new System.Drawing.Size(222, 20);
            this.tb_workPlace.TabIndex = 7;
            // 
            // tb_email
            // 
            this.tb_email.Location = new System.Drawing.Point(358, 96);
            this.tb_email.Name = "tb_email";
            this.tb_email.ReadOnly = true;
            this.tb_email.Size = new System.Drawing.Size(120, 20);
            this.tb_email.TabIndex = 5;
            // 
            // tb_phone
            // 
            this.tb_phone.Location = new System.Drawing.Point(232, 96);
            this.tb_phone.Name = "tb_phone";
            this.tb_phone.ReadOnly = true;
            this.tb_phone.Size = new System.Drawing.Size(120, 20);
            this.tb_phone.TabIndex = 4;
            // 
            // tb_thirdName
            // 
            this.tb_thirdName.Location = new System.Drawing.Point(484, 49);
            this.tb_thirdName.Name = "tb_thirdName";
            this.tb_thirdName.ReadOnly = true;
            this.tb_thirdName.Size = new System.Drawing.Size(120, 20);
            this.tb_thirdName.TabIndex = 3;
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(358, 49);
            this.tb_name.Name = "tb_name";
            this.tb_name.ReadOnly = true;
            this.tb_name.Size = new System.Drawing.Size(120, 20);
            this.tb_name.TabIndex = 2;
            // 
            // tb_family
            // 
            this.tb_family.Location = new System.Drawing.Point(232, 49);
            this.tb_family.Name = "tb_family";
            this.tb_family.ReadOnly = true;
            this.tb_family.Size = new System.Drawing.Size(120, 20);
            this.tb_family.TabIndex = 1;
            // 
            // pb_photo
            // 
            this.pb_photo.Image = ((System.Drawing.Image)(resources.GetObject("pb_photo.Image")));
            this.pb_photo.Location = new System.Drawing.Point(6, 19);
            this.pb_photo.Name = "pb_photo";
            this.pb_photo.Size = new System.Drawing.Size(208, 313);
            this.pb_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_photo.TabIndex = 0;
            this.pb_photo.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.настройкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1277, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_addClient,
            this.открытьПрофильКлиентаToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // m_addClient
            // 
            this.m_addClient.Name = "m_addClient";
            this.m_addClient.Size = new System.Drawing.Size(215, 22);
            this.m_addClient.Text = "Добавить нового клиента";
            this.m_addClient.Click += new System.EventHandler(this.поискToolStripMenuItem_Click);
            // 
            // открытьПрофильКлиентаToolStripMenuItem
            // 
            this.открытьПрофильКлиентаToolStripMenuItem.Name = "открытьПрофильКлиентаToolStripMenuItem";
            this.открытьПрофильКлиентаToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.открытьПрофильКлиентаToolStripMenuItem.Text = "Поиск клиента";
            this.открытьПрофильКлиентаToolStripMenuItem.Click += new System.EventHandler(this.открытьПрофильКлиентаToolStripMenuItem_Click);
            // 
            // настройкаToolStripMenuItem
            // 
            this.настройкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.редактироватьСписокУслугToolStripMenuItem,
            this.добавитьАкциюToolStripMenuItem,
            this.статистикаToolStripMenuItem1,
            this.тренераToolStripMenuItem,
            this.расписаниеToolStripMenuItem,
            this.выборПортаToolStripMenuItem});
            this.настройкаToolStripMenuItem.Name = "настройкаToolStripMenuItem";
            this.настройкаToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.настройкаToolStripMenuItem.Text = "Настройка";
            // 
            // редактироватьСписокУслугToolStripMenuItem
            // 
            this.редактироватьСписокУслугToolStripMenuItem.Name = "редактироватьСписокУслугToolStripMenuItem";
            this.редактироватьСписокУслугToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.редактироватьСписокУслугToolStripMenuItem.Text = "Редактировать список услуг";
            this.редактироватьСписокУслугToolStripMenuItem.Click += new System.EventHandler(this.редактироватьСписокУслугToolStripMenuItem_Click);
            // 
            // добавитьАкциюToolStripMenuItem
            // 
            this.добавитьАкциюToolStripMenuItem.Name = "добавитьАкциюToolStripMenuItem";
            this.добавитьАкциюToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.добавитьАкциюToolStripMenuItem.Text = "Редактировать акции";
            this.добавитьАкциюToolStripMenuItem.Click += new System.EventHandler(this.добавитьАкциюToolStripMenuItem_Click);
            // 
            // статистикаToolStripMenuItem1
            // 
            this.статистикаToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.статистикаПосещенийToolStripMenuItem,
            this.статистикаОплатToolStripMenuItem,
            this.статистикаЗаДеньToolStripMenuItem1,
            this.toolStripSeparator1,
            this.выгрузитьСтатистикуЗаМесяцToolStripMenuItem1});
            this.статистикаToolStripMenuItem1.Name = "статистикаToolStripMenuItem1";
            this.статистикаToolStripMenuItem1.Size = new System.Drawing.Size(229, 22);
            this.статистикаToolStripMenuItem1.Text = "Статистика";
            // 
            // статистикаПосещенийToolStripMenuItem
            // 
            this.статистикаПосещенийToolStripMenuItem.Name = "статистикаПосещенийToolStripMenuItem";
            this.статистикаПосещенийToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.статистикаПосещенийToolStripMenuItem.Text = "Статистика посещений";
            this.статистикаПосещенийToolStripMenuItem.Click += new System.EventHandler(this.статистикаПосещенийToolStripMenuItem_Click);
            // 
            // статистикаОплатToolStripMenuItem
            // 
            this.статистикаОплатToolStripMenuItem.Name = "статистикаОплатToolStripMenuItem";
            this.статистикаОплатToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.статистикаОплатToolStripMenuItem.Text = "Статистика оплат";
            this.статистикаОплатToolStripMenuItem.Click += new System.EventHandler(this.статистикаОплатToolStripMenuItem_Click);
            // 
            // статистикаЗаДеньToolStripMenuItem1
            // 
            this.статистикаЗаДеньToolStripMenuItem1.Name = "статистикаЗаДеньToolStripMenuItem1";
            this.статистикаЗаДеньToolStripMenuItem1.Size = new System.Drawing.Size(244, 22);
            this.статистикаЗаДеньToolStripMenuItem1.Text = "Статистика за день";
            this.статистикаЗаДеньToolStripMenuItem1.Click += new System.EventHandler(this.статистикаЗаДеньToolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(241, 6);
            // 
            // выгрузитьСтатистикуЗаМесяцToolStripMenuItem1
            // 
            this.выгрузитьСтатистикуЗаМесяцToolStripMenuItem1.Name = "выгрузитьСтатистикуЗаМесяцToolStripMenuItem1";
            this.выгрузитьСтатистикуЗаМесяцToolStripMenuItem1.Size = new System.Drawing.Size(244, 22);
            this.выгрузитьСтатистикуЗаМесяцToolStripMenuItem1.Text = "Выгрузить статистику за месяц";
            this.выгрузитьСтатистикуЗаМесяцToolStripMenuItem1.Click += new System.EventHandler(this.выгрузитьСтатистикуЗаМесяцToolStripMenuItem1_Click);
            // 
            // тренераToolStripMenuItem
            // 
            this.тренераToolStripMenuItem.Name = "тренераToolStripMenuItem";
            this.тренераToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.тренераToolStripMenuItem.Text = "Тренера";
            this.тренераToolStripMenuItem.Click += new System.EventHandler(this.тренераToolStripMenuItem_Click);
            // 
            // расписаниеToolStripMenuItem
            // 
            this.расписаниеToolStripMenuItem.Name = "расписаниеToolStripMenuItem";
            this.расписаниеToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.расписаниеToolStripMenuItem.Text = "Расписание";
            this.расписаниеToolStripMenuItem.Click += new System.EventHandler(this.расписаниеToolStripMenuItem_Click);
            // 
            // выборПортаToolStripMenuItem
            // 
            this.выборПортаToolStripMenuItem.Name = "выборПортаToolStripMenuItem";
            this.выборПортаToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.выборПортаToolStripMenuItem.Text = "Выбор порта";
            this.выборПортаToolStripMenuItem.Click += new System.EventHandler(this.выборПортаToolStripMenuItem_Click);
            // 
            // gb_abonement
            // 
            this.gb_abonement.Controls.Add(this.label30);
            this.gb_abonement.Controls.Add(this.bt_endExercise);
            this.gb_abonement.Controls.Add(this.label11);
            this.gb_abonement.Controls.Add(this.tb_stock);
            this.gb_abonement.Controls.Add(this.tb_endDate);
            this.gb_abonement.Controls.Add(this.tb_freezeStartDate);
            this.gb_abonement.Controls.Add(this.tb_freezeEndDate);
            this.gb_abonement.Controls.Add(this.tb_startDate);
            this.gb_abonement.Controls.Add(this.tb_activationDate);
            this.gb_abonement.Controls.Add(this.tb_restOfExercise);
            this.gb_abonement.Controls.Add(this.tb_finishedExercise);
            this.gb_abonement.Controls.Add(this.label29);
            this.gb_abonement.Controls.Add(this.label28);
            this.gb_abonement.Controls.Add(this.label27);
            this.gb_abonement.Controls.Add(this.label26);
            this.gb_abonement.Controls.Add(this.label25);
            this.gb_abonement.Controls.Add(this.label24);
            this.gb_abonement.Controls.Add(this.label23);
            this.gb_abonement.Controls.Add(this.label22);
            this.gb_abonement.Controls.Add(this.label21);
            this.gb_abonement.Controls.Add(this.label20);
            this.gb_abonement.Controls.Add(this.label19);
            this.gb_abonement.Controls.Add(this.label18);
            this.gb_abonement.Controls.Add(this.label17);
            this.gb_abonement.Controls.Add(this.b_editAbonement);
            this.gb_abonement.Controls.Add(this.b_freeze);
            this.gb_abonement.Controls.Add(this.b_prolongation);
            this.gb_abonement.Controls.Add(this.b_startExercise);
            this.gb_abonement.Controls.Add(this.tb_freezeDaysRest);
            this.gb_abonement.Controls.Add(this.tb_abonementStatus);
            this.gb_abonement.Controls.Add(this.tb_countOfExercice);
            this.gb_abonement.Controls.Add(this.tb_type);
            this.gb_abonement.Controls.Add(this.tb_serviceName);
            this.gb_abonement.Location = new System.Drawing.Point(728, 27);
            this.gb_abonement.Name = "gb_abonement";
            this.gb_abonement.Size = new System.Drawing.Size(545, 513);
            this.gb_abonement.TabIndex = 2;
            this.gb_abonement.TabStop = false;
            this.gb_abonement.Text = "Абонемент";
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(100, 23);
            this.label30.TabIndex = 0;
            // 
            // bt_endExercise
            // 
            this.bt_endExercise.Enabled = false;
            this.bt_endExercise.Location = new System.Drawing.Point(393, 143);
            this.bt_endExercise.Name = "bt_endExercise";
            this.bt_endExercise.Size = new System.Drawing.Size(124, 76);
            this.bt_endExercise.TabIndex = 68;
            this.bt_endExercise.Text = "Закончить занятие";
            this.bt_endExercise.UseVisualStyleBackColor = true;
            this.bt_endExercise.Click += new System.EventHandler(this.bt_endExercise_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(174, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 67;
            this.label11.Text = "Куплен по акции";
            // 
            // tb_stock
            // 
            this.tb_stock.Location = new System.Drawing.Point(173, 49);
            this.tb_stock.Name = "tb_stock";
            this.tb_stock.ReadOnly = true;
            this.tb_stock.Size = new System.Drawing.Size(161, 20);
            this.tb_stock.TabIndex = 66;
            // 
            // tb_endDate
            // 
            this.tb_endDate.Location = new System.Drawing.Point(6, 405);
            this.tb_endDate.Name = "tb_endDate";
            this.tb_endDate.ReadOnly = true;
            this.tb_endDate.Size = new System.Drawing.Size(161, 20);
            this.tb_endDate.TabIndex = 65;
            // 
            // tb_freezeStartDate
            // 
            this.tb_freezeStartDate.Location = new System.Drawing.Point(6, 339);
            this.tb_freezeStartDate.Name = "tb_freezeStartDate";
            this.tb_freezeStartDate.ReadOnly = true;
            this.tb_freezeStartDate.Size = new System.Drawing.Size(161, 20);
            this.tb_freezeStartDate.TabIndex = 64;
            // 
            // tb_freezeEndDate
            // 
            this.tb_freezeEndDate.Location = new System.Drawing.Point(177, 339);
            this.tb_freezeEndDate.Name = "tb_freezeEndDate";
            this.tb_freezeEndDate.ReadOnly = true;
            this.tb_freezeEndDate.Size = new System.Drawing.Size(157, 20);
            this.tb_freezeEndDate.TabIndex = 63;
            // 
            // tb_startDate
            // 
            this.tb_startDate.Location = new System.Drawing.Point(176, 188);
            this.tb_startDate.Name = "tb_startDate";
            this.tb_startDate.ReadOnly = true;
            this.tb_startDate.Size = new System.Drawing.Size(161, 20);
            this.tb_startDate.TabIndex = 62;
            // 
            // tb_activationDate
            // 
            this.tb_activationDate.Location = new System.Drawing.Point(6, 188);
            this.tb_activationDate.Name = "tb_activationDate";
            this.tb_activationDate.ReadOnly = true;
            this.tb_activationDate.Size = new System.Drawing.Size(161, 20);
            this.tb_activationDate.TabIndex = 61;
            // 
            // tb_restOfExercise
            // 
            this.tb_restOfExercise.Location = new System.Drawing.Point(176, 233);
            this.tb_restOfExercise.Name = "tb_restOfExercise";
            this.tb_restOfExercise.ReadOnly = true;
            this.tb_restOfExercise.Size = new System.Drawing.Size(161, 20);
            this.tb_restOfExercise.TabIndex = 60;
            // 
            // tb_finishedExercise
            // 
            this.tb_finishedExercise.Location = new System.Drawing.Point(6, 233);
            this.tb_finishedExercise.Name = "tb_finishedExercise";
            this.tb_finishedExercise.ReadOnly = true;
            this.tb_finishedExercise.Size = new System.Drawing.Size(161, 20);
            this.tb_finishedExercise.TabIndex = 59;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 295);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 13);
            this.label29.TabIndex = 51;
            this.label29.Text = "Заморозка";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 389);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(139, 13);
            this.label28.TabIndex = 50;
            this.label28.Text = "Дата окончания действия";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(337, 323);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(142, 13);
            this.label27.TabIndex = 49;
            this.label27.Text = "Осталось дней заморозки";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(173, 323);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(148, 13);
            this.label26.TabIndex = 48;
            this.label26.Text = "Дата окончания заморозки";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 323);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(130, 13);
            this.label25.TabIndex = 47;
            this.label25.Text = "Дата начала заморозки";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(173, 217);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(93, 13);
            this.label24.TabIndex = 46;
            this.label24.Text = "Остаток занятий";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 217);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(124, 13);
            this.label23.TabIndex = 45;
            this.label23.Text = "Совершено посещений";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(173, 171);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(104, 13);
            this.label22.TabIndex = 44;
            this.label22.Text = "Начало посещений";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 171);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(89, 13);
            this.label21.TabIndex = 43;
            this.label21.Text = "Дата активации";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 127);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 13);
            this.label20.TabIndex = 42;
            this.label20.Text = "Количество занятий";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(173, 82);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 41;
            this.label19.Text = "Статус";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 82);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(26, 13);
            this.label18.TabIndex = 40;
            this.label18.Text = "Тип";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "Название услуги";
            // 
            // b_editAbonement
            // 
            this.b_editAbonement.Enabled = false;
            this.b_editAbonement.Location = new System.Drawing.Point(340, 455);
            this.b_editAbonement.Name = "b_editAbonement";
            this.b_editAbonement.Size = new System.Drawing.Size(124, 47);
            this.b_editAbonement.TabIndex = 15;
            this.b_editAbonement.Text = "Редактиовать";
            this.b_editAbonement.UseVisualStyleBackColor = true;
            this.b_editAbonement.Click += new System.EventHandler(this.b_editAbonement_Click);
            // 
            // b_freeze
            // 
            this.b_freeze.Enabled = false;
            this.b_freeze.Location = new System.Drawing.Point(173, 455);
            this.b_freeze.Name = "b_freeze";
            this.b_freeze.Size = new System.Drawing.Size(124, 47);
            this.b_freeze.TabIndex = 14;
            this.b_freeze.Text = "Заморозка";
            this.b_freeze.UseVisualStyleBackColor = true;
            this.b_freeze.Click += new System.EventHandler(this.b_freeze_Click);
            // 
            // b_prolongation
            // 
            this.b_prolongation.Enabled = false;
            this.b_prolongation.Location = new System.Drawing.Point(12, 455);
            this.b_prolongation.Name = "b_prolongation";
            this.b_prolongation.Size = new System.Drawing.Size(124, 47);
            this.b_prolongation.TabIndex = 13;
            this.b_prolongation.Text = "Продлить";
            this.b_prolongation.UseVisualStyleBackColor = true;
            this.b_prolongation.Click += new System.EventHandler(this.b_prolongation_Click);
            // 
            // b_startExercise
            // 
            this.b_startExercise.Enabled = false;
            this.b_startExercise.Location = new System.Drawing.Point(393, 53);
            this.b_startExercise.Name = "b_startExercise";
            this.b_startExercise.Size = new System.Drawing.Size(124, 76);
            this.b_startExercise.TabIndex = 12;
            this.b_startExercise.Text = "Начать занятие";
            this.b_startExercise.UseVisualStyleBackColor = true;
            this.b_startExercise.Click += new System.EventHandler(this.b_startExercise_Click);
            // 
            // tb_freezeDaysRest
            // 
            this.tb_freezeDaysRest.Location = new System.Drawing.Point(340, 339);
            this.tb_freezeDaysRest.Name = "tb_freezeDaysRest";
            this.tb_freezeDaysRest.ReadOnly = true;
            this.tb_freezeDaysRest.Size = new System.Drawing.Size(161, 20);
            this.tb_freezeDaysRest.TabIndex = 10;
            // 
            // tb_abonementStatus
            // 
            this.tb_abonementStatus.Location = new System.Drawing.Point(173, 96);
            this.tb_abonementStatus.Name = "tb_abonementStatus";
            this.tb_abonementStatus.ReadOnly = true;
            this.tb_abonementStatus.Size = new System.Drawing.Size(161, 20);
            this.tb_abonementStatus.TabIndex = 3;
            // 
            // tb_countOfExercice
            // 
            this.tb_countOfExercice.Location = new System.Drawing.Point(6, 143);
            this.tb_countOfExercice.Name = "tb_countOfExercice";
            this.tb_countOfExercice.ReadOnly = true;
            this.tb_countOfExercice.Size = new System.Drawing.Size(161, 20);
            this.tb_countOfExercice.TabIndex = 2;
            // 
            // tb_type
            // 
            this.tb_type.Location = new System.Drawing.Point(6, 96);
            this.tb_type.Name = "tb_type";
            this.tb_type.ReadOnly = true;
            this.tb_type.Size = new System.Drawing.Size(161, 20);
            this.tb_type.TabIndex = 1;
            // 
            // tb_serviceName
            // 
            this.tb_serviceName.Location = new System.Drawing.Point(6, 49);
            this.tb_serviceName.Name = "tb_serviceName";
            this.tb_serviceName.ReadOnly = true;
            this.tb_serviceName.Size = new System.Drawing.Size(161, 20);
            this.tb_serviceName.TabIndex = 0;
            // 
            // t_clear
            // 
            this.t_clear.Interval = 1000;
            this.t_clear.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lbBZ
            // 
            this.lbBZ.AutoSize = true;
            this.lbBZ.Location = new System.Drawing.Point(12, 547);
            this.lbBZ.Name = "lbBZ";
            this.lbBZ.Size = new System.Drawing.Size(85, 13);
            this.lbBZ.TabIndex = 3;
            this.lbBZ.Text = "Большой зал: 0";
            // 
            // lbMZ
            // 
            this.lbMZ.AutoSize = true;
            this.lbMZ.Location = new System.Drawing.Point(12, 573);
            this.lbMZ.Name = "lbMZ";
            this.lbMZ.Size = new System.Drawing.Size(75, 13);
            this.lbMZ.TabIndex = 4;
            this.lbMZ.Text = "Малый зал: 0";
            // 
            // lbSum
            // 
            this.lbSum.AutoSize = true;
            this.lbSum.Location = new System.Drawing.Point(12, 601);
            this.lbSum.Name = "lbSum";
            this.lbSum.Size = new System.Drawing.Size(49, 13);
            this.lbSum.TabIndex = 5;
            this.lbSum.Text = "Всего: 0";
            // 
            // lbSumPerDay
            // 
            this.lbSumPerDay.AutoSize = true;
            this.lbSumPerDay.Location = new System.Drawing.Point(12, 627);
            this.lbSumPerDay.Name = "lbSumPerDay";
            this.lbSumPerDay.Size = new System.Drawing.Size(59, 13);
            this.lbSumPerDay.TabIndex = 6;
            this.lbSumPerDay.Text = "За день: 0";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 750);
            this.Controls.Add(this.lbSumPerDay);
            this.Controls.Add(this.lbSum);
            this.Controls.Add(this.lbMZ);
            this.Controls.Add(this.lbBZ);
            this.Controls.Add(this.gb_abonement);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Главное окно";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Deactivate += new System.EventHandler(this.MainForm_Deactivate);
            this.MouseEnter += new System.EventHandler(this.MainForm_MouseEnter);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_photo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gb_abonement.ResumeLayout(false);
            this.gb_abonement.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_addClient;
        private System.Windows.Forms.ToolStripMenuItem настройкаToolStripMenuItem;
        private System.Windows.Forms.TextBox tb_sale;
        private System.Windows.Forms.TextBox tb_livePlace;
        private System.Windows.Forms.TextBox tb_workPlace;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.TextBox tb_phone;
        private System.Windows.Forms.TextBox tb_thirdName;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.TextBox tb_family;
        private System.Windows.Forms.PictureBox pb_photo;
        private System.Windows.Forms.Button b_addAbonement;
        private System.Windows.Forms.Button b_delete;
        private System.Windows.Forms.Button b_editClient;
        private System.Windows.Forms.ToolStripMenuItem открытьПрофильКлиентаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьСписокУслугToolStripMenuItem;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gb_abonement;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button b_editAbonement;
        private System.Windows.Forms.Button b_freeze;
        private System.Windows.Forms.Button b_prolongation;
        private System.Windows.Forms.Button b_startExercise;
        private System.Windows.Forms.TextBox tb_freezeDaysRest;
        private System.Windows.Forms.TextBox tb_abonementStatus;
        private System.Windows.Forms.TextBox tb_countOfExercice;
        private System.Windows.Forms.TextBox tb_type;
        private System.Windows.Forms.TextBox tb_serviceName;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tb_status;
        private System.Windows.Forms.TextBox tb_restOfExercise;
        private System.Windows.Forms.TextBox tb_finishedExercise;
        private System.Windows.Forms.TextBox tb_birthday;
        private System.Windows.Forms.TextBox tb_endDate;
        private System.Windows.Forms.TextBox tb_freezeStartDate;
        private System.Windows.Forms.TextBox tb_freezeEndDate;
        private System.Windows.Forms.TextBox tb_startDate;
        private System.Windows.Forms.TextBox tb_activationDate;
        private System.Windows.Forms.RichTextBox rtb_note;
        private System.Windows.Forms.TextBox tb_sex;
        private System.Windows.Forms.ListView lv_abonements;
        private System.Windows.Forms.Label lb_active;
        private System.Windows.Forms.ListView lv_kids;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_stock;
        private System.Windows.Forms.ToolStripMenuItem добавитьАкциюToolStripMenuItem;
        private System.Windows.Forms.Button bt_endExercise;
        private System.Windows.Forms.Timer t_clear;
        private System.Windows.Forms.ToolStripMenuItem тренераToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem расписаниеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выборПортаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem статистикаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem статистикаПосещенийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem статистикаОплатToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem статистикаЗаДеньToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выгрузитьСтатистикуЗаМесяцToolStripMenuItem1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbBZ;
        private System.Windows.Forms.Label lbMZ;
        private System.Windows.Forms.Label lbSum;
        private System.Windows.Forms.Label lbSumPerDay;
    }
}

