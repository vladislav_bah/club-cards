﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class ChangeCard : Form
    {
        COM com;
        public EditClient client;
        public ChangeCard(DataBase DB, EditClient cli,string port)
        {
            InitializeComponent();
            client = cli;
            com = new COM(this, DB,port);
            com.OpenPort();
        }

        public void CloseCard()
        {
            com.ClosePort();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            com.ClosePort();
            this.Close();
        }
    }
}
