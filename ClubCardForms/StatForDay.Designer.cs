﻿namespace ClubCardForms
{
    partial class StatForDay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_stat1 = new System.Windows.Forms.DataGridView();
            this.dgv_statPayment = new System.Windows.Forms.DataGridView();
            this.dgv_statTreners = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.l_ter = new System.Windows.Forms.Label();
            this.l_nal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_stat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_statPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_statTreners)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_stat1
            // 
            this.dgv_stat1.AllowUserToAddRows = false;
            this.dgv_stat1.AllowUserToDeleteRows = false;
            this.dgv_stat1.AllowUserToResizeRows = false;
            this.dgv_stat1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_stat1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgv_stat1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_stat1.Location = new System.Drawing.Point(12, 38);
            this.dgv_stat1.MultiSelect = false;
            this.dgv_stat1.Name = "dgv_stat1";
            this.dgv_stat1.ReadOnly = true;
            this.dgv_stat1.Size = new System.Drawing.Size(415, 394);
            this.dgv_stat1.TabIndex = 1;
            // 
            // dgv_statPayment
            // 
            this.dgv_statPayment.AllowUserToAddRows = false;
            this.dgv_statPayment.AllowUserToDeleteRows = false;
            this.dgv_statPayment.AllowUserToResizeRows = false;
            this.dgv_statPayment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_statPayment.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgv_statPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_statPayment.Location = new System.Drawing.Point(433, 38);
            this.dgv_statPayment.MultiSelect = false;
            this.dgv_statPayment.Name = "dgv_statPayment";
            this.dgv_statPayment.ReadOnly = true;
            this.dgv_statPayment.Size = new System.Drawing.Size(469, 394);
            this.dgv_statPayment.TabIndex = 2;
            // 
            // dgv_statTreners
            // 
            this.dgv_statTreners.AllowUserToAddRows = false;
            this.dgv_statTreners.AllowUserToDeleteRows = false;
            this.dgv_statTreners.AllowUserToResizeRows = false;
            this.dgv_statTreners.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_statTreners.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgv_statTreners.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_statTreners.Location = new System.Drawing.Point(908, 38);
            this.dgv_statTreners.MultiSelect = false;
            this.dgv_statTreners.Name = "dgv_statTreners";
            this.dgv_statTreners.ReadOnly = true;
            this.dgv_statTreners.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgv_statTreners.Size = new System.Drawing.Size(476, 394);
            this.dgv_statTreners.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Статистика посещений:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(430, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Статистика оплат:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(905, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Статистика тренеров:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(516, 506);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(334, 43);
            this.button1.TabIndex = 7;
            this.button1.Text = "Сохранить в Excel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(536, 447);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Всего по терминалу (руб):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(536, 469);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Всего наличным расчетом (руб): ";
            // 
            // l_ter
            // 
            this.l_ter.AutoSize = true;
            this.l_ter.Location = new System.Drawing.Point(708, 446);
            this.l_ter.Name = "l_ter";
            this.l_ter.Size = new System.Drawing.Size(0, 13);
            this.l_ter.TabIndex = 10;
            // 
            // l_nal
            // 
            this.l_nal.AutoSize = true;
            this.l_nal.Location = new System.Drawing.Point(708, 469);
            this.l_nal.Name = "l_nal";
            this.l_nal.Size = new System.Drawing.Size(0, 13);
            this.l_nal.TabIndex = 11;
            // 
            // StatForDay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1396, 577);
            this.Controls.Add(this.l_nal);
            this.Controls.Add(this.l_ter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_statTreners);
            this.Controls.Add(this.dgv_statPayment);
            this.Controls.Add(this.dgv_stat1);
            this.Name = "StatForDay";
            this.Text = "Статистика за день";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_stat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_statPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_statTreners)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_stat1;
        private System.Windows.Forms.DataGridView dgv_statPayment;
        private System.Windows.Forms.DataGridView dgv_statTreners;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label l_ter;
        private System.Windows.Forms.Label l_nal;

    }
}