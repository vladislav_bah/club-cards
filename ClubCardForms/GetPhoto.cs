﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;

namespace ClubCardForms
{
    public partial class GetPhoto : Form
    {
        WebCam cam = new WebCam();
        bool flag = true;
        EditClient Form;
        public GetPhoto(EditClient Form)
        {
            InitializeComponent();
            this.Form = Form;
            cam.Start(1);
            streamVideo();
        }

        async private void streamVideo()
        {
            while (flag)
            {
                pictureBox1.Image = cam.GetFrame();
                await Task.Delay(2);
            }
        }
        private void bt_getPhoto_Click(object sender, EventArgs e)
        {
            Image temp = pictureBox1.Image;
            flag = false;
            Form.Image = temp;
            cam.Stop();
            Close();
            
        }
    }

    class WebCam
    {
        private int m_Width = 640;
        private int m_Height = 480;
        private int m_CapHwnd;

        public const int WM_USER = 1024;

        public const int WM_CAP_CONNECT = 1034;
        public const int WM_CAP_DISCONNECT = 1035;
        public const int WM_CAP_GT_FRAME = 1084;
        public const int WM_CAP_COPY = 1054;

        public const int WM_CAP_START = WM_USER;

        public const int WM_CAP_DLG_VIDEOFORMAT = WM_CAP_START + 41;
        public const int WM_CAP_DLG_VIDEOSOURCE = WM_CAP_START + 42;
        public const int WM_CAP_DLG_VIDEODISPLAY = WM_CAP_START + 43;
        public const int WM_CAP_GET_VIDEOFORMAT = WM_CAP_START + 44;
        public const int WM_CAP_SET_VIDEOFORMAT = WM_CAP_START + 45;
        public const int WM_CAP_DLG_VIDEOCOMPRESSION = WM_CAP_START + 46;
        public const int WM_CAP_SET_PREVIEW = WM_CAP_START + 50;

        [DllImport("user32", EntryPoint = "SendMessage")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);

        [DllImport("avicap32.dll", EntryPoint = "capCreateCaptureWindowA")]
        public static extern int capCreateCaptureWindowA(string lpszWindowName, int dwStyle, int x, int y, int nWidth, int nHeight, int hwndParent, int nID);

        [DllImport("user32", EntryPoint = "OpenClipboard")]
        public static extern int OpenClipboard(int hWnd);

        [DllImport("user32", EntryPoint = "EmptyClipboard")]
        public static extern int EmptyClipboard();

        [DllImport("user32", EntryPoint = "CloseClipboard")]
        public static extern int CloseClipboard();

        [DllImport("user32.dll")]
        extern static IntPtr GetClipboardData(uint uFormat);

        public void Stop()
        {
            SendMessage(m_CapHwnd, WM_CAP_DISCONNECT, 0, 0);
            CloseClipboard();
        }

        public void Start(int ptr)
        {
            this.Stop();
            m_CapHwnd = capCreateCaptureWindowA("WebCap", 0, 0, 0, m_Width, m_Height, 0, 0);
            SendMessage(m_CapHwnd, WM_CAP_CONNECT, 0, 0);
        }

        public Image GetFrame()
        {
            Clipboard.Clear();
            SendMessage(m_CapHwnd, WM_CAP_GT_FRAME, 0, 0);
            SendMessage(m_CapHwnd, WM_CAP_COPY, 0, 0);
            return Clipboard.GetImage();
        }

        private byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);

            return ms.ToArray();
        }
    }
}
