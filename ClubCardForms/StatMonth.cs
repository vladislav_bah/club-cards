﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class StatMonth : Form
    {
        private DataBase DB;
        int year;
        int month;
        string strmonth = "";
        DateTime startTime;
       // DateTime startTime;
        public StatMonth(DataBase DB)
        {
            InitializeComponent();
            this.DB = DB;
            tb_year.Text = DateTime.Now.Year.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            year = Convert.ToInt16(tb_year.Text);
            month = cb_month.SelectedIndex + 1;
            startTime = new DateTime(year, month, 1);
            if (month < 10)
            {
                strmonth = "0" + Convert.ToString(month);
            }
            else
            {
                strmonth = Convert.ToString(month);
            }
            ExportToExcel();
        }

        private void ExportToExcel()
        {
            DataSet payment = null;
            DataSet nal = null;
            DataSet ter = null;
            DataSet treners = null;
            DataSet actions = null;
            try
            {
                payment = DB.Select("select FirstName as 'Имя',Secondname as 'Фамилия',Thirdname as 'Отчество',OfferName as 'Услуга',Cost as 'Цена',PaymentDate as 'Дата покупки',PaymentType as 'Тип платежа' from PaymentHistory,Clients_Subs,Clients where PaymentHistory.SubID=Clients_Subs.SubID " +
                "and Clients.ID=Clients_Subs.ClientID and substr(PaymentDate,4,2) ='" + strmonth + "'");

                nal = DB.Select("Select sum(cost) from PaymentHistory where PaymentType = 0 and substr(PaymentDate,4,2) ='" + strmonth + "'");
                ter = DB.Select("Select sum(cost) from PaymentHistory where PaymentType = 1 and substr(PaymentDate,4,2) ='" + strmonth + "'");
                
                treners = DB.Select("select distinct SecondName, FirstName, ThirdName, treners.id from treners, fitnes_history where treners.id = trener_id and substr(date,4,2)  = '" + strmonth + "'");
                actions = DB.Select("select distinct action from  fitnes_history where substr(date,4,2)  = '" + strmonth + "'");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения статистики. Error: " + ex.Message);
            }




            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            Microsoft.Office.Interop.Excel._Worksheet worksheet2 = null;

            try
            {

                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Cтатистика оплат ";
                (worksheet.Cells[1, payment.Tables[0].Columns.Count + 1] as Microsoft.Office.Interop.Excel.Range).Font.Bold = true;
                int cellRowIndex = 1;
                int cellColumnIndex = 1;

                //Loop through each row and read value from each column. 
                for (int i = 0; i < payment.Tables[0].Rows.Count + 1; i++)
                {

                    for (int j = 0; j < payment.Tables[0].Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = payment.Tables[0].Columns[j].Caption;
                        }
                        else
                        {
                            if (j == 6)
                            {
                                if (payment.Tables[0].DefaultView[i - 1][j].ToString() == "0")
                                {
                                    worksheet.Cells[cellRowIndex, cellColumnIndex] = "Наличный расчет";
                                }
                                else
                                {
                                    worksheet.Cells[cellRowIndex, cellColumnIndex] = "Терминал";
                                }
                            }
                            else
                            {
                                worksheet.Cells[cellRowIndex, cellColumnIndex] = payment.Tables[0].DefaultView[i - 1][j].ToString();
                            }
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                worksheet.Cells[cellRowIndex, cellColumnIndex] = "Всего наличный расчет";
                worksheet.Cells[cellRowIndex, cellColumnIndex + 1] = nal.Tables[0].DefaultView[0][0].ToString();
                cellRowIndex++;
                worksheet.Cells[cellRowIndex, cellColumnIndex] = "Терминал";
                worksheet.Cells[cellRowIndex, cellColumnIndex + 1] = ter.Tables[0].DefaultView[0][0].ToString();
                worksheet.Columns.AutoFit();
                

                workbook.Sheets.Add();
                worksheet2 = workbook.Sheets[1];
                worksheet2.Name = "Статистика занятий ";


                for (int i = 0; i < treners.Tables[0].Rows.Count; i++)
                {
                    String tren;
                    tren = treners.Tables[0].DefaultView[i][0].ToString() + " " + treners.Tables[0].DefaultView[i][1].ToString()[0] + "." + treners.Tables[0].DefaultView[i][2].ToString()[0] + ".";
                    
                    worksheet2.Cells[1, i+2] = tren;
                }

                try
                {
                    for (int i = 0; i < actions.Tables[0].Rows.Count; i++)
                    {
                        String action;
                        action = actions.Tables[0].DefaultView[i][0].ToString();
                        worksheet2.Cells[i + 2, 1] = action;
                    }
                }
                catch { }

                DataSet tmp;
                try
                {
                    cellRowIndex = 1;
                    cellColumnIndex = 1;
                    for (int i = 0; i < treners.Tables[0].Rows.Count; i++)
                        for (int j = 0; j < actions.Tables[0].Rows.Count; j++)
                        {
                            cellRowIndex = j;
                            cellColumnIndex = i;
                            tmp = DB.Select("select count(*) from fitnes_history where action = '" + actions.Tables[0].DefaultView[cellRowIndex][0] + "' and trener_id = " +
                                treners.Tables[0].DefaultView[i][3].ToString());
                            worksheet2.Cells[cellRowIndex + 2, cellColumnIndex + 2].Value = tmp.Tables[0].DefaultView[0][0].ToString();
                        }

                    worksheet2.Cells[cellRowIndex + 3, 1].Value = "Итого";
                    worksheet2.Cells[1, cellRowIndex  + 2].Value = "Итого";

                    for (int i = 0; i < actions.Tables[0].Rows.Count; i++)
                    {
                        int count = 0;
                        for (int j = 0; j < treners.Tables[0].Rows.Count; j++)
                        {
                            count += Convert.ToInt32(worksheet2.Cells[i+2,j+2].Value);
                        }
                        worksheet2.Cells[i+2, cellRowIndex + 2].Value = count;
                    }

                    for (int i = 0; i < treners.Tables[0].Rows.Count; i++)
                    {
                        int count = 0;
                        for (int j = 0; j < actions.Tables[0].Rows.Count; j++)
                        {
                            count += Convert.ToInt32(worksheet2.Cells[j + 2, i + 2].Value);
                        }
                        worksheet2.Cells[cellRowIndex + 3, i+2].Value = count;
                    }
                }
                catch { }
                cellRowIndex = 1;
                cellColumnIndex = 1;                
                worksheet2.Columns.AutoFit();

                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 1;
                saveDialog.FileName = cb_month.SelectedItem.ToString() + " " + Convert.ToString(year);
                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Выписка успешно сохранена");
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                workbook = null;
                excel = null;
            }

        }     
    }
}
