﻿namespace ClubCardForms
{
    partial class AddOffer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddOffer));
            this.tb_name = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_numbOfVisits = new System.Windows.Forms.MaskedTextBox();
            this.tb_cost = new System.Windows.Forms.MaskedTextBox();
            this.cb_type = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bt_add = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_duration = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_unlimited = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // tb_name
            // 
            resources.ApplyResources(this.tb_name, "tb_name");
            this.tb_name.Name = "tb_name";
            this.tb_name.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // tb_numbOfVisits
            // 
            resources.ApplyResources(this.tb_numbOfVisits, "tb_numbOfVisits");
            this.tb_numbOfVisits.Name = "tb_numbOfVisits";
            this.tb_numbOfVisits.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_cost
            // 
            resources.ApplyResources(this.tb_cost, "tb_cost");
            this.tb_cost.Name = "tb_cost";
            this.tb_cost.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // cb_type
            // 
            this.cb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_type.FormattingEnabled = true;
            this.cb_type.Items.AddRange(new object[] {
            resources.GetString("cb_type.Items"),
            resources.GetString("cb_type.Items1")});
            resources.ApplyResources(this.cb_type, "cb_type");
            this.cb_type.Name = "cb_type";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // bt_add
            // 
            resources.ApplyResources(this.bt_add, "bt_add");
            this.bt_add.Name = "bt_add";
            this.bt_add.UseVisualStyleBackColor = true;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // tb_duration
            // 
            resources.ApplyResources(this.tb_duration, "tb_duration");
            this.tb_duration.Name = "tb_duration";
            this.tb_duration.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // cb_unlimited
            // 
            resources.ApplyResources(this.cb_unlimited, "cb_unlimited");
            this.cb_unlimited.Name = "cb_unlimited";
            this.cb_unlimited.UseVisualStyleBackColor = true;
            this.cb_unlimited.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // AddOffer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cb_unlimited);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_duration);
            this.Controls.Add(this.bt_add);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cb_type);
            this.Controls.Add(this.tb_cost);
            this.Controls.Add(this.tb_numbOfVisits);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_name);
            this.Name = "AddOffer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox tb_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox tb_numbOfVisits;
        private System.Windows.Forms.MaskedTextBox tb_cost;
        private System.Windows.Forms.ComboBox cb_type;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bt_add;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox tb_duration;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cb_unlimited;
    }
}