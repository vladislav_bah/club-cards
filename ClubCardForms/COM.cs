﻿using System;
using System.IO.Ports;
using System.Data;
using System.Media;
using System.Threading;
using System.Windows.Forms;

namespace ClubCardForms
{
    class COM
    {
        SerialPort comPort = new SerialPort();
        private delegate void SetTextDeleg(string text);
        private MainForm form1 = null;
        private ChangeCard form2 = null;
        private DataBase DB;
        string port;
        string oldcard = "";
        static int timer2 = 0;
        System.Threading.Timer timer;    

        static private void Callback(Object state)
        {
            timer2++;
        }

        public COM(MainForm formName,DataBase DB,string port)
        {
            form1 = formName;
            this.DB = DB;
            this.port = port;
            timer = new System.Threading.Timer(Callback, null, 0, 1000);
        }

        public COM(ChangeCard formName, DataBase DB, string port)
        {
            form2 = formName;
            this.DB = DB;
            this.port = port;
            timer = new System.Threading.Timer(Callback, null, 0, 1000);
        }

        public void OpenPort()
        {

            try
            {
                if (comPort.IsOpen == true) comPort.Close();
                comPort = new SerialPort(Convert.ToString(port),
                                        Convert.ToInt32(9600),
                                        Parity.None,//Допилить
                                        Convert.ToInt32(8),
                                        StopBits.One);//Допилить
                //Открываем порт
                comPort.Open();
                comPort.DataReceived += new SerialDataReceivedEventHandler(sp_DataReceived);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ClosePort()
        {
            try
            {
                comPort.Close();
            }
            catch
            { }
        }

        void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (form1 != null)
            {
                try
                {
                    Thread.Sleep(150);
                    char[] data = new char[14];
                    comPort.Read(data,0,14);
                    string card = "";
                    for (int i = 1; i < 13; i++ )
                    {
                        card += data[i];
                    }
                    if ((card != oldcard || timer2 >= 5) && card != "-1")
                    {
                        form1.BeginInvoke(new SetTextDeleg(action_form1), new object[] { card });
                        oldcard = card;
                        timer2 = 0;
                    }
                    else
                    {
                        card = "-1";
                    }
                    
                }
                catch 
                {

                }
            }
            else if(form2 != null)
            {
                try
                {
                    Thread.Sleep(150);
                    char[] data = new char[14];
                    comPort.Read(data, 0, 14);
                    string card = "";
                    for (int i = 1; i < 13; i++)
                    {
                        card += data[i];
                    }
                    if ((card != oldcard || timer2 >= 5) && card != "-1")
                    {
                        form2.BeginInvoke(new SetTextDeleg(action_form2), new object[] { card });
                        oldcard = card;
                        timer2 = 0;
                    }
                    else
                    {
                        card = "-1";
                    }
                }
                catch 
                {

                }
            }
        }


        private void action_form1(string data)
        {
            DataSet ds = DB.Select("Select * from Clients where CardID = '" + data + "'");
            DataSet count;
            DateTime time2 = DateTime.Now;
            try
            {

                count = DB.Select("select count(*), SubID from meta where SubId in " +
                     " (select SubID from Clients_subs " +
                     " where ClientID = " + ds.Tables[0].DefaultView[0][0] + ")");
                if (Convert.ToInt32(count.Tables[0].DefaultView[0][0]) > 0)
                {
                    DataSet dtime = DB.Select("select startDate, startTime from meta where SubId = " + count.Tables[0].DefaultView[0][1].ToString());
                    EndOfExercise endform = new EndOfExercise(Convert.ToInt32(ds.Tables[0].DefaultView[0][0]), DB, Convert.ToString(count.Tables[0].DefaultView[0][1]));
                    endform.ShowDialog();
                    DataSet IsEx = DB.Select("select * from meta where SubID = " +  count.Tables[0].DefaultView[0][1].ToString());
                    if (IsEx.Tables[0].Rows.Count == 0)
                    {
                        form1.ChangeActive("Не на занятии", System.Drawing.Color.Red);

                        DataSet tmp = DB.Select("select Offers.Name, NumberOfVisits, Offers.type from Offers, Subs " +
                            "where Subs.ID = " + count.Tables[0].DefaultView[0][1].ToString() + " and Offers.ID = Subs.OfferID");
                        string type;
                        string ab;
                        if (tmp.Tables[0].DefaultView[0][1].ToString() != "-1")
                        {
                            if (tmp.Tables[0].DefaultView[0][2].ToString() == "0")
                            {
                                type = "дневной";
                            }
                            else
                            {
                                type = "вечерний";
                            }
                            ab = tmp.Tables[0].DefaultView[0][0].ToString() + " (" + tmp.Tables[0].DefaultView[0][1].ToString() + " посещений, " + type + ")";
                        }
                        else
                        {
                            if (tmp.Tables[0].DefaultView[0][2].ToString() == "0")
                            {
                                type = "дневной";
                            }
                            else
                            {
                                type = "вечерний";
                            }
                            ab = tmp.Tables[0].DefaultView[0][0].ToString() + " (месяц, " + type + ")";
                        }



                        DB.Query("Insert into History(FIO, Offer, startDate, endDate) values('" + ds.Tables[0].DefaultView[0][4].ToString() + " " + ds.Tables[0].DefaultView[0][3].ToString() + " " + ds.Tables[0].DefaultView[0][5].ToString() + "', " +
                            "'" + ab + "', '" + dtime.Tables[0].DefaultView[0][0].ToString() + " " +
                            dtime.Tables[0].DefaultView[0][1].ToString() + "', '" + time2.ToShortDateString() + " " + time2.ToShortTimeString() + "')");
                    }
                }
                else
                {

                        form1.clearAbonement();
                        form1.Fillform(Convert.ToInt32(ds.Tables[0].DefaultView[0][0]));

                }
            }
            catch (Exception ex)
            {
                ClosePort();
                EditClient editForm = new EditClient(DB, form1);
                editForm.SetCard(data);
                editForm.ShowDialog();
                OpenPort();
            }

            

        }

        private void action_form2(string data)
        {
                form2.client.SetCard(data);
                form2.CloseCard();
                form2.Close();
        }
    }
}
