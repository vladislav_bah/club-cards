﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class fitnes_choose : Form
    {
        private DataBase BD;
        private int ID = -1;
        private int old_id = -1;
        MainForm mainform;
        String zalint = "";
        public fitnes_choose(DataBase BD,MainForm Mainform)
        {
            InitializeComponent();
            this.BD = BD;
            fill();
            this.Height = 170;
            bt_ok.Location = new Point(bt_ok.Location.X, 95);
            bt_cancel.Location = new Point(bt_cancel.Location.X, 95);
            lv_shedule.Visible = false;
            mainform = Mainform;
        }
        private void fill()
        {

            lv_shedule.Clear();
            lv_shedule.Columns.Add("ID");
            lv_shedule.Columns.Add("Время");
            lv_shedule.Columns.Add("Занятие");
            lv_shedule.Columns.Add("Зал");
            lv_shedule.Columns.Add("Тренер");
            string today = DateTime.Today.DayOfWeek.ToString();
            int todayInt=0;
            switch(today)
            {
                case "Monday":todayInt=1; break;
                case "Tuesday":todayInt=2; break;
                case "Wednesday":todayInt=3; break;
                case "Thursday":todayInt=4; break;
                case "Friday":todayInt=5; break;
                case "Saturday":todayInt=6; break;
                case "Sunday":todayInt=7; break;

            }
            DataSet tmp = BD.Select("select * from Shedule where day = '" + Convert.ToString(todayInt) + "'  order by time");
            string time = DateTime.Now.ToShortTimeString();
            if (tmp.Tables[0].Rows.Count > 0)
            {
                bool flag = true;
                foreach (DataRow unit in tmp.Tables[0].Rows)
                {
                    String zal = ""; 
                    String tr_name = "";
                    string time2 = unit.ItemArray[2].ToString();
                    try
                    {
                        DataSet trener = BD.Select("select SecondName, FirstName, ThirdName from Treners where id = " + unit.ItemArray[4].ToString());
                        tr_name = trener.Tables[0].DefaultView[0][0].ToString() + " " + trener.Tables[0].DefaultView[0][1].ToString() +
                          " " + trener.Tables[0].DefaultView[0][2].ToString();
                    }
                    catch { }
                    
                    if (unit.ItemArray[3].ToString() == "1")
                    {
                        zal = "Большой зал";
                        zalint = "1";
                    }
                    if (unit.ItemArray[3].ToString() == "2")
                    {
                        zal = "Малый зал";
                        zalint = "2";
                    }
                    var lvi = new ListViewItem(new[] {unit.ItemArray[0].ToString(), unit.ItemArray[2].ToString(), unit.ItemArray[5].ToString(),
                            zal, tr_name});
                    lv_shedule.Items.Add(lvi);
                    if(flag &&(Convert.ToInt32(time.Split(':')[0])>Convert.ToInt32(time2.Split(':')[0]) || (Convert.ToInt32(time.Split(':')[0])==Convert.ToInt32(time2.Split(':')[0]) && Convert.ToInt32(time.Split(':')[1])>Convert.ToInt32(time2.Split(':')[1]) )))
                    {
                        ID = -1;
                    }
                    else
                    {
                        if(flag)
                        {
                            ID = Convert.ToInt32(unit.ItemArray[0].ToString());
                            rb_yes.Text = "Записать клиента на занятие " + unit.ItemArray[5].ToString() + " в " + time2;
                            flag = false;
                            old_id = ID;
                        }                    
                    }
                }
                //if (flag)
                //{
                //    rb_yes.Text = "Ближайших занятий нет";
                //}
            }
            lv_shedule.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lv_shedule.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            lv_shedule.Columns[0].Width = 0;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_yes.Checked)
            {
                ID = old_id;
                this.Height = 170;
                bt_ok.Location = new Point(bt_ok.Location.X, 95);
                bt_cancel.Location = new Point(bt_cancel.Location.X, 95);
                lv_shedule.Visible = false;
            }
            else
            {
                ID = -1;
                this.Height = 428;
                bt_ok.Location = new Point(bt_ok.Location.X, 350);
                bt_cancel.Location = new Point(bt_cancel.Location.X, 350);
                lv_shedule.Visible = true;
                
            }
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            try
            {
                mainform.Zal = Convert.ToInt32(zalint);
            }
            catch { }
            if (ID == -1)
            {
                MessageBox.Show("Выбирете занятие");
            }
            else
            {
                String action = "";
                String trener_id = "";
                String trener_name = "";
                String date = "";
                String time = "";
                for (int i = 0; i< lv_shedule.Items.Count; i++)
                {
                    if (lv_shedule.Items[i].SubItems[0].Text == ID.ToString())
                    {
                        action = lv_shedule.Items[i].SubItems[2].Text;
                        time = lv_shedule.Items[i].SubItems[1].Text;
                        trener_name = lv_shedule.Items[i].SubItems[4].Text;
                    }
                }
                DataSet tr_id = BD.Select("Select id from treners where SecondName = '" + trener_name.Split(' ')[0] + "' and FirstName = '" +
                    trener_name.Split(' ')[1] + "' and ThirdName = '" + trener_name.Split(' ')[2] + "'");
                trener_id = tr_id.Tables[0].DefaultView[0][0].ToString();
                date = DateTime.Now.ToShortDateString() + " " + time;
                BD.Query("insert into fitnes_history(action, trener_id, date) values('" + action + "', " + trener_id 
                    +", '" + date + "')");
                mainform.Fitnes_result = true;
                this.Close();
            }
        }

        private void bt_cancel_Click(object sender, EventArgs e)
        {
            mainform.Fitnes_result = false;
            this.Close();
        }

        private void lv_shedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_shedule.SelectedItems.Count > 0)
            {
                ID = Convert.ToInt32(lv_shedule.SelectedItems[0].SubItems[0].Text);
                if (lv_shedule.SelectedItems[0].SubItems[3].Text == "Большой зал")
                {
                    zalint = "1";
                }
                if (lv_shedule.SelectedItems[0].SubItems[3].Text == "Малый зал")
                {
                    zalint = "2";
                }
            }
        }

        private void rb_no_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
