﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class AddOffer : Form
    {

        private DataBase BD;
        public AddOffer(DataBase BD)
        {
            InitializeComponent();
            this.BD = BD;
        }

        private void EnabledOk(object sender, EventArgs e)
        {
            if (tb_name.Text != "" && tb_duration.Text!= "" && tb_cost.Text != "" && (tb_numbOfVisits.Text != "" || cb_unlimited.Checked == true)
          && cb_type.SelectedIndex >= 0)
                bt_add.Enabled = true;
            else
                bt_add.Enabled = false;
        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            string numb = "";
            if (cb_unlimited.Checked)
            {
                numb = "-1";
            }
            else
            {
                numb = tb_numbOfVisits.Text;
            }
            string query = "insert into Offers(Name,NumberOfVisits,Duration,Price,Status, type) values('" +
                   tb_name.Text + "'," + numb + "," + tb_duration.Text + "," + tb_cost.Text + ",1," + (cb_type.SelectedIndex)+")";
            try
            {
                BD.Query(query);
                MessageBox.Show("Услуга добавлена успешно!");
            }
            catch
            {
                MessageBox.Show("Ошибка добавления услуги");
            }
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            EnabledOk(sender, e);
            if (cb_unlimited.Checked)
            {
                tb_numbOfVisits.Enabled = false;
            }
            else
            {
                tb_numbOfVisits.Enabled = true;
            }
        }

    }
}
