﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class EditStocks : Form
    {
        private DataBase BD;
        public EditStocks(DataBase BD)
        {
            InitializeComponent();
            this.BD = BD;
            fill_stocks();
        }

        private void fill_stocks()
        {
            lv_stocks.Clear();
            lv_stocks.Columns.Add("ID");
            lv_stocks.Columns.Add("Акция");
            try
            {
                DataSet tmp = BD.Select("select ID, Name from Stock");
                if (tmp.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow abonement in tmp.Tables[0].Rows)
                    {
                        var lvi = new ListViewItem(new[] { abonement.ItemArray[0].ToString(), abonement.ItemArray[1].ToString() });
                        lv_stocks.Items.Add(lvi);
                    }


                    lv_stocks.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                    lv_stocks.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

                    lv_stocks.Items[0].Selected = true;
                }
            }
            catch
            {
                MessageBox.Show("Ошибка заполнения списка акций");
            }
            fill_all();
        }

        private void fill_all()
        {
            try
            {

                lv_linked.Clear();
                lv_notLinked.Clear();
                lv_linked.Columns.Add("ID");
                lv_linked.Columns.Add("Распространяется на:");
                lv_notLinked.Columns.Add("ID");
                lv_notLinked.Columns.Add("Не распространяется на:");

                DataSet tmp;
                if (lv_stocks.Items.Count > 0)
                {
                    tmp = BD.Select("select * from Stock where ID = " + lv_stocks.SelectedItems[0].SubItems[0].Text);
                    tb_stockName.Text = tmp.Tables[0].DefaultView[0][2].ToString();
                    tb_discount.Text = tmp.Tables[0].DefaultView[0][1].ToString();
                    cb_typeOfDiscount.SelectedIndex = Convert.ToInt32(tmp.Tables[0].DefaultView[0][5]);
                    dtp_start.Text = tmp.Tables[0].DefaultView[0][3].ToString();
                    dtp_end.Text = tmp.Tables[0].DefaultView[0][4].ToString();


                    DataSet tmp2 = BD.Select("select Id,Name,NumberOfVisits,type from Offers");
                    List<string> Offers_ID = new List<string>();
                    tmp = BD.Select("select Offer_ID from Offers_Stock where Stock_ID = " + lv_stocks.SelectedItems[0].SubItems[0].Text);

                    if (tmp.Tables[0].Rows.Count >= 0)
                    {
                        foreach (DataRow Offer_ID in tmp.Tables[0].Rows)
                        {
                            Offers_ID.Add(Offer_ID.ItemArray[0].ToString());

                        }
                        string type = "";
                        if (tmp2.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow stock in tmp2.Tables[0].Rows)
                            {
                                switch (stock.ItemArray[3].ToString())
                                {
                                    case "0": type = "Дневной";
                                        break;

                                    case "1": type = "Вечерний";
                                        break;


                                }
                                string limit = "";
                                if (stock.ItemArray[2].ToString() == "-1")
                                {
                                    limit = "Неограниченно";
                                }
                                else
                                {
                                    limit = stock.ItemArray[2].ToString();
                                }
                                var lvi = new ListViewItem(new[] { stock.ItemArray[0].ToString(), stock.ItemArray[1].ToString() + " (" + type + ") " + limit + " посещений" });
                                if (Offers_ID.Contains(stock.ItemArray[0].ToString()))
                                {
                                    lv_linked.Items.Add(lvi);
                                }
                                else
                                {
                                    lv_notLinked.Items.Add(lvi);
                                }

                            }
                        }

                    }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка заполнения данных услуги");
            }
            lv_linked.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lv_linked.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            lv_notLinked.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lv_notLinked.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }
        private void lv_stocks_DoubleClick(object sender, EventArgs e)
        {
            fill_all();
        }

        private void bt_edit_Click(object sender, EventArgs e)
        {

            if (lv_stocks.SelectedItems.Count == 0)
            {
                MessageBox.Show("Выберите акцию!");
                return;
            }
            string query = "update Stock set Name ='" + tb_stockName.Text + "', Discount =" + tb_discount.Text+",startdate ='"+
                dtp_start.Text+"',enddate = '"+dtp_end.Text+"',type="+cb_typeOfDiscount.SelectedIndex+" where id =" + lv_stocks.SelectedItems[0].SubItems[0].Text;
            try
            {
                BD.Query(query);
                MessageBox.Show("Услуга изменена успешно!");
            }
            catch
            {
                MessageBox.Show("Ошибка изменения услуги");
            }
            int n = lv_stocks.SelectedIndices[0];
            fill_stocks();
            lv_stocks.Items[0].Selected = false;
            lv_stocks.Items[n].Selected = true;
        }

        private void lv_linked_Click(object sender, EventArgs e)
        {
            bt_addOrDel.Text = "--->";
        }

        private void lv_notLinked_Click(object sender, EventArgs e)
        {
            bt_addOrDel.Text = "<---";
        }

        private void bt_addOrDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (bt_addOrDel.Text == "--->")
                {
                    string query = "delete from Offers_Stock where Offer_ID = " + lv_linked.SelectedItems[0].SubItems[0].Text + " and Stock_ID =" + lv_stocks.SelectedItems[0].SubItems[0].Text;
                    try
                    {
                        BD.Query(query);
                        //MessageBox.Show("Связь успешно удалена!");
                    }
                    catch
                    {
                        //MessageBox.Show("Ошибка удаления связи");
                    }
                    fill_all();
                }

                if (bt_addOrDel.Text == "<---")
                {
                    string query = "Insert into Offers_Stock(Offer_ID,Stock_ID) values(" + lv_notLinked.SelectedItems[0].SubItems[0].Text + "," + lv_stocks.SelectedItems[0].SubItems[0].Text + ")";
                    try
                    {
                        BD.Query(query);
                        //MessageBox.Show("Связь успешно добавлена!");
                    }
                    catch
                    {
                        //MessageBox.Show("Ошибка добавления связи");
                    }
                    fill_all();
                }
            }
            catch { }
        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            AddStock addstock = new AddStock(BD);
            addstock.ShowDialog();
        }

        private void EditStocks_Activated(object sender, EventArgs e)
        {
            try
            {
                lv_stocks.Items[0].Selected = true;
            }
            catch { }
            fill_stocks();
        }






    }
}
