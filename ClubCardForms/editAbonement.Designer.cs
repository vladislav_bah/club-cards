﻿namespace ClubCardForms
{
    partial class editAbonement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(editAbonement));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_nal = new System.Windows.Forms.RadioButton();
            this.rb_term = new System.Windows.Forms.RadioButton();
            this.cb_person = new System.Windows.Forms.CheckBox();
            this.dtp_dateOfEnd = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.l_price = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.l_stock = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_stock = new System.Windows.Forms.ComboBox();
            this.dtp_firstVisit = new System.Windows.Forms.DateTimePicker();
            this.dtp_dateOfActivation = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cb_status = new System.Windows.Forms.ComboBox();
            this.cb_serviceName = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_resrOfExercise = new System.Windows.Forms.TextBox();
            this.tb_finishedExercise = new System.Windows.Forms.TextBox();
            this.tb_countOfExercise = new System.Windows.Forms.TextBox();
            this.b_ok = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.cb_person);
            this.groupBox2.Controls.Add(this.dtp_dateOfEnd);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.l_price);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.l_stock);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cb_stock);
            this.groupBox2.Controls.Add(this.dtp_firstVisit);
            this.groupBox2.Controls.Add(this.dtp_dateOfActivation);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.cb_status);
            this.groupBox2.Controls.Add(this.cb_serviceName);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.tb_resrOfExercise);
            this.groupBox2.Controls.Add(this.tb_finishedExercise);
            this.groupBox2.Controls.Add(this.tb_countOfExercise);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(543, 313);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Абонемент";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_nal);
            this.groupBox1.Controls.Add(this.rb_term);
            this.groupBox1.Location = new System.Drawing.Point(354, 189);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(168, 64);
            this.groupBox1.TabIndex = 68;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тип оплаты";
            // 
            // rb_nal
            // 
            this.rb_nal.AutoSize = true;
            this.rb_nal.Checked = true;
            this.rb_nal.Location = new System.Drawing.Point(25, 19);
            this.rb_nal.Name = "rb_nal";
            this.rb_nal.Size = new System.Drawing.Size(113, 17);
            this.rb_nal.TabIndex = 66;
            this.rb_nal.TabStop = true;
            this.rb_nal.Text = "Наличный расчет";
            this.rb_nal.UseVisualStyleBackColor = true;
            // 
            // rb_term
            // 
            this.rb_term.AutoSize = true;
            this.rb_term.Location = new System.Drawing.Point(25, 41);
            this.rb_term.Name = "rb_term";
            this.rb_term.Size = new System.Drawing.Size(76, 17);
            this.rb_term.TabIndex = 67;
            this.rb_term.Text = "Терминал";
            this.rb_term.UseVisualStyleBackColor = true;
            // 
            // cb_person
            // 
            this.cb_person.AutoSize = true;
            this.cb_person.Enabled = false;
            this.cb_person.Location = new System.Drawing.Point(278, 94);
            this.cb_person.Name = "cb_person";
            this.cb_person.Size = new System.Drawing.Size(176, 17);
            this.cb_person.TabIndex = 65;
            this.cb_person.Text = "Использовать личную скидку";
            this.cb_person.UseVisualStyleBackColor = true;
            this.cb_person.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // dtp_dateOfEnd
            // 
            this.dtp_dateOfEnd.Location = new System.Drawing.Point(6, 287);
            this.dtp_dateOfEnd.Name = "dtp_dateOfEnd";
            this.dtp_dateOfEnd.Size = new System.Drawing.Size(161, 20);
            this.dtp_dateOfEnd.TabIndex = 64;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 271);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(139, 13);
            this.label28.TabIndex = 61;
            this.label28.Text = "Дата окончания действия";
            // 
            // l_price
            // 
            this.l_price.AutoSize = true;
            this.l_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_price.Location = new System.Drawing.Point(417, 287);
            this.l_price.Name = "l_price";
            this.l_price.Size = new System.Drawing.Size(0, 18);
            this.l_price.TabIndex = 63;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(351, 287);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 18);
            this.label2.TabIndex = 62;
            this.label2.Text = "Итого:";
            // 
            // l_stock
            // 
            this.l_stock.AutoSize = true;
            this.l_stock.Location = new System.Drawing.Point(442, 49);
            this.l_stock.Name = "l_stock";
            this.l_stock.Size = new System.Drawing.Size(50, 13);
            this.l_stock.TabIndex = 61;
            this.l_stock.Text = "Скидка: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(275, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 60;
            this.label1.Text = "Куплен по акции";
            // 
            // cb_stock
            // 
            this.cb_stock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_stock.Enabled = false;
            this.cb_stock.FormattingEnabled = true;
            this.cb_stock.Items.AddRange(new object[] {
            "Заморожен",
            "Активен",
            "Неактивен"});
            this.cb_stock.Location = new System.Drawing.Point(275, 49);
            this.cb_stock.Name = "cb_stock";
            this.cb_stock.Size = new System.Drawing.Size(161, 21);
            this.cb_stock.TabIndex = 59;
            this.cb_stock.TextChanged += new System.EventHandler(this.cb_stock_TextChanged);
            // 
            // dtp_firstVisit
            // 
            this.dtp_firstVisit.Enabled = false;
            this.dtp_firstVisit.Location = new System.Drawing.Point(173, 189);
            this.dtp_firstVisit.Name = "dtp_firstVisit";
            this.dtp_firstVisit.Size = new System.Drawing.Size(161, 20);
            this.dtp_firstVisit.TabIndex = 58;
            // 
            // dtp_dateOfActivation
            // 
            this.dtp_dateOfActivation.Location = new System.Drawing.Point(6, 189);
            this.dtp_dateOfActivation.Name = "dtp_dateOfActivation";
            this.dtp_dateOfActivation.Size = new System.Drawing.Size(161, 20);
            this.dtp_dateOfActivation.TabIndex = 57;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(173, 173);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(104, 13);
            this.label22.TabIndex = 56;
            this.label22.Text = "Начало посещений";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 173);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(89, 13);
            this.label21.TabIndex = 55;
            this.label21.Text = "Дата активации";
            // 
            // cb_status
            // 
            this.cb_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_status.Enabled = false;
            this.cb_status.FormattingEnabled = true;
            this.cb_status.Items.AddRange(new object[] {
            "Заморожен",
            "Активен",
            "Неактивен"});
            this.cb_status.Location = new System.Drawing.Point(6, 94);
            this.cb_status.Name = "cb_status";
            this.cb_status.Size = new System.Drawing.Size(161, 21);
            this.cb_status.TabIndex = 54;
            // 
            // cb_serviceName
            // 
            this.cb_serviceName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_serviceName.FormattingEnabled = true;
            this.cb_serviceName.Location = new System.Drawing.Point(6, 49);
            this.cb_serviceName.Name = "cb_serviceName";
            this.cb_serviceName.Size = new System.Drawing.Size(260, 21);
            this.cb_serviceName.TabIndex = 52;
            this.cb_serviceName.TextChanged += new System.EventHandler(this.cb_serviceName_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(173, 217);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(93, 13);
            this.label24.TabIndex = 46;
            this.label24.Text = "Остаток занятий";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 217);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(124, 13);
            this.label23.TabIndex = 45;
            this.label23.Text = "Совершено посещений";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 127);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 13);
            this.label20.TabIndex = 42;
            this.label20.Text = "Количество занятий";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 81);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 41;
            this.label19.Text = "Статус";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "Название услуги";
            // 
            // tb_resrOfExercise
            // 
            this.tb_resrOfExercise.Location = new System.Drawing.Point(173, 233);
            this.tb_resrOfExercise.Name = "tb_resrOfExercise";
            this.tb_resrOfExercise.ReadOnly = true;
            this.tb_resrOfExercise.Size = new System.Drawing.Size(161, 20);
            this.tb_resrOfExercise.TabIndex = 7;
            // 
            // tb_finishedExercise
            // 
            this.tb_finishedExercise.Location = new System.Drawing.Point(6, 233);
            this.tb_finishedExercise.Name = "tb_finishedExercise";
            this.tb_finishedExercise.ReadOnly = true;
            this.tb_finishedExercise.Size = new System.Drawing.Size(161, 20);
            this.tb_finishedExercise.TabIndex = 6;
            this.tb_finishedExercise.TextChanged += new System.EventHandler(this.tb_finishedExercise_TextChanged);
            // 
            // tb_countOfExercise
            // 
            this.tb_countOfExercise.Location = new System.Drawing.Point(6, 143);
            this.tb_countOfExercise.Name = "tb_countOfExercise";
            this.tb_countOfExercise.ReadOnly = true;
            this.tb_countOfExercise.Size = new System.Drawing.Size(161, 20);
            this.tb_countOfExercise.TabIndex = 2;
            // 
            // b_ok
            // 
            this.b_ok.Location = new System.Drawing.Point(210, 331);
            this.b_ok.Name = "b_ok";
            this.b_ok.Size = new System.Drawing.Size(124, 47);
            this.b_ok.TabIndex = 14;
            this.b_ok.Text = "Ok";
            this.b_ok.UseVisualStyleBackColor = true;
            this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
            // 
            // editAbonement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 393);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.b_ok);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "editAbonement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление/редактирование абонемента";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button b_ok;
        private System.Windows.Forms.TextBox tb_resrOfExercise;
        private System.Windows.Forms.TextBox tb_finishedExercise;
        private System.Windows.Forms.TextBox tb_countOfExercise;
        private System.Windows.Forms.ComboBox cb_status;
        private System.Windows.Forms.ComboBox cb_serviceName;
        private System.Windows.Forms.DateTimePicker dtp_firstVisit;
        private System.Windows.Forms.DateTimePicker dtp_dateOfActivation;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_stock;
        private System.Windows.Forms.Label l_stock;
        private System.Windows.Forms.Label l_price;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtp_dateOfEnd;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox cb_person;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb_nal;
        private System.Windows.Forms.RadioButton rb_term;
    }
}