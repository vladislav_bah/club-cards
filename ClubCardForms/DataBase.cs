﻿using System;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Windows.Forms;

namespace ClubCardForms
{
    public class DataBase
    {
        private String dbFileName;
        private SQLiteConnection dbConn;
        private SQLiteCommand sqlCmd;

        public DataBase(string name)
        {
            dbConn = new SQLiteConnection();
            sqlCmd = new SQLiteCommand();

            dbFileName = name;
            if (!File.Exists(dbFileName))
                SQLiteConnection.CreateFile(dbFileName);
        }

        ~DataBase()
        {
            //dbConn.Close();//Закрываем соединение
        }

        public bool Connect()
        {
            try
            {
                dbConn = new SQLiteConnection("Data Source=" + dbFileName + ";Version=3;");
                dbConn.Open();
                sqlCmd.Connection = dbConn;
            }
            catch 
            {
                return false;
            }
            return true;
        }

        public DataSet Select(string query)
        {
            DataSet ds = new DataSet(); //Создаем объект класса DataSet
            string sql = query; //Sql запрос (достать все из таблицы customer)
            try
            {
                SQLiteDataAdapter da = new SQLiteDataAdapter(sql, dbConn);//Создаем объект класса DataAdapter (тут мы передаем наш запрос и получаем ответ)
                da.Fill(ds);//Заполняем DataSet cодержимым DataAdapter'a
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public void Query(string query)
        {
            SQLiteTransaction trans = dbConn.BeginTransaction();
            try
            { 
                sqlCmd.CommandText = query;
                sqlCmd.ExecuteNonQuery();
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
        }
    }
}
