﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class Shedule : Form
    {
        private DataBase BD;
        private List<ListView> vws= new List<ListView>();
        private String id;
        public Shedule(DataBase BD)
        {
            InitializeComponent();
            this.BD = BD;
            cd_zal.Items.Add("Большой зал");
            cd_zal.Items.Add("Малый зал");
            cb_zal2.Items.Add("Большой зал");
            cb_zal2.Items.Add("Малый зал");

            cb_weekDay.Items.Add("Понедельник");
            cb_weekDay.Items.Add("Вторник");
            cb_weekDay.Items.Add("Среда");
            cb_weekDay.Items.Add("Четверг");
            cb_weekDay.Items.Add("Пятница");
            cb_weekDay.Items.Add("Суббота");
            cb_weekDay.Items.Add("Воскресение");
            fill();
        }

        private void fill()
        {
            cb_trener.Items.Clear();
            cb_trener2.Items.Clear();
            foreach (Control lv in this.Controls)
                if (lv is ListView)
                {
                    vws.Add((ListView)lv);
                    ((ListView)lv).Clear();
                    ((ListView)lv).Columns.Add("ID");             
                    ((ListView)lv).Columns.Add("Время");
                    ((ListView)lv).Columns.Add("Занятие");
                    ((ListView)lv).Columns.Add("Зал");
                    ((ListView)lv).Columns.Add("Тренер");
                }                  
            try
            {              
                DataSet trener_list = BD.Select("select SecondName, FirstName, ThirdName from Treners");
                foreach (DataRow unit in trener_list.Tables[0].Rows)
                {
                    cb_trener.Items.Add(unit.ItemArray[0].ToString() + " " + unit.ItemArray[1].ToString() + " " + unit.ItemArray[2].ToString());
                    cb_trener2.Items.Add(unit.ItemArray[0].ToString() + " " + unit.ItemArray[1].ToString() + " " + unit.ItemArray[2].ToString());
                }
                DataSet tmp = BD.Select("select * from Shedule order by time");
                if (tmp.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow unit in tmp.Tables[0].Rows)
                    {
                        String tr_name;
                        String zal = "";
                        try
                        {
                            DataSet trener = BD.Select("select SecondName, FirstName, ThirdName from Treners where id = " + unit.ItemArray[4].ToString());
                            tr_name = trener.Tables[0].DefaultView[0][0].ToString() + " " + trener.Tables[0].DefaultView[0][1].ToString() +
                              " " + trener.Tables[0].DefaultView[0][2].ToString();
                        }
                        catch
                        {
                            tr_name = "";
                        }
                        if (unit.ItemArray[3].ToString() == "1")
                            zal = "Большой зал";
                        if (unit.ItemArray[3].ToString() == "2")
                            zal = "Малый зал";
                        var lvi = new ListViewItem(new[] {unit.ItemArray[0].ToString(), unit.ItemArray[2].ToString(), unit.ItemArray[5].ToString(),
                            zal, tr_name});
                        if (unit.ItemArray[1].ToString() == "1")
                        {                 
                            lv_mon.Items.Add(lvi);
                        }
                        if (unit.ItemArray[1].ToString() == "2")
                        {
                            lv_tue.Items.Add(lvi);
                        }
                        if (unit.ItemArray[1].ToString() == "3")
                        {
                            lv_wed.Items.Add(lvi);
                        }
                        if (unit.ItemArray[1].ToString() == "4")
                        {
                            lv_thu.Items.Add(lvi);
                        }
                        if (unit.ItemArray[1].ToString() == "5")
                        {
                            lv_fri.Items.Add(lvi);
                        }
                        if (unit.ItemArray[1].ToString() == "6")
                        {
                            lv_sat.Items.Add(lvi);
                        }
                        if (unit.ItemArray[1].ToString() == "7")
                        {
                            lv_sun.Items.Add(lvi);
                        }
                    }

                    foreach (Control lv in this.Controls)
                        if (lv is ListView)
                        {
                            ((ListView)lv).AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                            ((ListView)lv).AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                            ((ListView)lv).Columns[0].Width = 0;
                        }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка заполнения расписания");
            }
        }

        private void fillAdd(int i)
        {
            if (vws[i].SelectedItems.Count > 0)
            {
                id = vws[i].SelectedItems[0].SubItems[0].Text;
                string selected_trener = vws[i].SelectedItems[0].SubItems[4].Text;
                cb_trener.SelectedItem = selected_trener;
                tb_time.Text = vws[i].SelectedItems[0].SubItems[1].Text;
                tb_exercise.Text = vws[i].SelectedItems[0].SubItems[2].Text;
                cd_zal.SelectedItem = vws[i].SelectedItems[0].SubItems[3].Text;
            }

        }

        private void lv_mon_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillAdd(6);
            bt_apply.Enabled = true;
        }

        private void lv_tue_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillAdd(5);
            bt_apply.Enabled = true;
        }

        private void lv_wed_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillAdd(4);
            bt_apply.Enabled = true;
        }

        private void lv_thu_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillAdd(3);
            bt_apply.Enabled = true;
        }

        private void lv_fri_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillAdd(2);
            bt_apply.Enabled = true;
        }

        private void lv_sat_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillAdd(1);
            bt_apply.Enabled = true;
        }

        private void lv_sun_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillAdd(0);
            bt_apply.Enabled = true;
        }

        private void bt_apply_Click(object sender, EventArgs e)
        {
            String time;
            if (tb_time.Text.Length == 4)
            {
                time = "0" + tb_time.Text;
            }
            else
            {
                time = tb_time.Text;
            }
            DataSet trener_id = BD.Select("Select id from treners where SecondName = '" + cb_trener.SelectedItem.ToString().Split(' ')[0] + "' and FirstName = '" +
                cb_trener.SelectedItem.ToString().Split(' ')[1] + "' and ThirdName = '" + cb_trener.SelectedItem.ToString().Split(' ')[2] + "'");
            BD.Query("update shedule set time = '" + time + "', action = '" + tb_exercise.Text + "', zal = " + (cd_zal.SelectedIndex+1) + ", trener_id = " +
                trener_id.Tables[0].DefaultView[0][0].ToString() + " where id = " + id);
            fill();
        }

        private void bt_delete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show
                (
                 "Вы уверены, что хотите удалить занятие из расписания? ",
                 "Подтверждение",
                 MessageBoxButtons.YesNo,
                 MessageBoxIcon.None,
                 MessageBoxDefaultButton.Button1
                );
            if (result == DialogResult.Yes)
            {
                try
                {
                    BD.Query("delete from Shedule where id =" + id);
                    fill();
                }

                catch
                {
                    MessageBox.Show("Ошибка удаления занятия");
                }

            }
            else
            {
                return;
            }
        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            String time;
            if (tb_time2.Text.Length == 4)
            {
                time = "0" + tb_time2.Text;
            }
            else
            {
                time = tb_time2.Text;
            }
            DataSet trener_id = BD.Select("Select id from treners where SecondName = '" + cb_trener2.SelectedItem.ToString().Split(' ')[0] + "' and FirstName = '" +
                cb_trener2.SelectedItem.ToString().Split(' ')[1] + "' and ThirdName = '" + cb_trener2.SelectedItem.ToString().Split(' ')[2] + "'");
            BD.Query("Insert into shedule(day, time, zal, trener_id, action) values (" + (cb_weekDay.SelectedIndex + 1) + ", '" + time + "', " 
                + (cb_zal2.SelectedIndex+1) + ", " + trener_id.Tables[0].DefaultView[0][0].ToString() + ", '" + tb_exercise2.Text + "')");
            fill();
        }


        private void EnabledOk(object sender, EventArgs e)
        {
            if (tb_time2.Text!="" && tb_exercise2.Text!="" && cb_zal2.SelectedIndex>=0 && cb_trener2.SelectedIndex>=0 && cb_weekDay.SelectedIndex>=0)
                bt_add.Enabled = true;
            else
                bt_add.Enabled = false;

        }
    }
}
