﻿namespace ClubCardForms
{
    partial class EditStocks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditStocks));
            this.lv_stocks = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_stockName = new System.Windows.Forms.MaskedTextBox();
            this.tb_discount = new System.Windows.Forms.MaskedTextBox();
            this.dtp_start = new System.Windows.Forms.DateTimePicker();
            this.dtp_end = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cb_typeOfDiscount = new System.Windows.Forms.ComboBox();
            this.bt_edit = new System.Windows.Forms.Button();
            this.lv_linked = new System.Windows.Forms.ListView();
            this.lv_notLinked = new System.Windows.Forms.ListView();
            this.bt_addOrDel = new System.Windows.Forms.Button();
            this.bt_add = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lv_stocks
            // 
            this.lv_stocks.FullRowSelect = true;
            this.lv_stocks.GridLines = true;
            this.lv_stocks.HideSelection = false;
            this.lv_stocks.Location = new System.Drawing.Point(36, 40);
            this.lv_stocks.MultiSelect = false;
            this.lv_stocks.Name = "lv_stocks";
            this.lv_stocks.Size = new System.Drawing.Size(198, 372);
            this.lv_stocks.TabIndex = 18;
            this.lv_stocks.UseCompatibleStateImageBehavior = false;
            this.lv_stocks.View = System.Windows.Forms.View.Details;
            this.lv_stocks.Click += new System.EventHandler(this.lv_stocks_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Список акций";
            // 
            // tb_stockName
            // 
            this.tb_stockName.Location = new System.Drawing.Point(255, 40);
            this.tb_stockName.Name = "tb_stockName";
            this.tb_stockName.Size = new System.Drawing.Size(196, 20);
            this.tb_stockName.TabIndex = 20;
            // 
            // tb_discount
            // 
            this.tb_discount.Location = new System.Drawing.Point(255, 83);
            this.tb_discount.Name = "tb_discount";
            this.tb_discount.Size = new System.Drawing.Size(100, 20);
            this.tb_discount.TabIndex = 21;
            // 
            // dtp_start
            // 
            this.dtp_start.Location = new System.Drawing.Point(529, 40);
            this.dtp_start.Name = "dtp_start";
            this.dtp_start.Size = new System.Drawing.Size(200, 20);
            this.dtp_start.TabIndex = 22;
            // 
            // dtp_end
            // 
            this.dtp_end.Location = new System.Drawing.Point(529, 83);
            this.dtp_end.Name = "dtp_end";
            this.dtp_end.Size = new System.Drawing.Size(200, 20);
            this.dtp_end.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(252, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Название акции";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(252, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Размер скидки:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(526, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Дата начала:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(526, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Дата окончания";
            // 
            // cb_typeOfDiscount
            // 
            this.cb_typeOfDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_typeOfDiscount.FormattingEnabled = true;
            this.cb_typeOfDiscount.Items.AddRange(new object[] {
            "Рублей",
            "%"});
            this.cb_typeOfDiscount.Location = new System.Drawing.Point(361, 82);
            this.cb_typeOfDiscount.Name = "cb_typeOfDiscount";
            this.cb_typeOfDiscount.Size = new System.Drawing.Size(90, 21);
            this.cb_typeOfDiscount.TabIndex = 54;
            // 
            // bt_edit
            // 
            this.bt_edit.Location = new System.Drawing.Point(380, 125);
            this.bt_edit.Name = "bt_edit";
            this.bt_edit.Size = new System.Drawing.Size(191, 21);
            this.bt_edit.TabIndex = 55;
            this.bt_edit.Text = "Редактировать";
            this.bt_edit.UseVisualStyleBackColor = true;
            this.bt_edit.Click += new System.EventHandler(this.bt_edit_Click);
            // 
            // lv_linked
            // 
            this.lv_linked.FullRowSelect = true;
            this.lv_linked.GridLines = true;
            this.lv_linked.HideSelection = false;
            this.lv_linked.Location = new System.Drawing.Point(255, 165);
            this.lv_linked.MultiSelect = false;
            this.lv_linked.Name = "lv_linked";
            this.lv_linked.Size = new System.Drawing.Size(191, 247);
            this.lv_linked.TabIndex = 56;
            this.lv_linked.UseCompatibleStateImageBehavior = false;
            this.lv_linked.View = System.Windows.Forms.View.Details;
            this.lv_linked.Click += new System.EventHandler(this.lv_linked_Click);
            // 
            // lv_notLinked
            // 
            this.lv_notLinked.FullRowSelect = true;
            this.lv_notLinked.GridLines = true;
            this.lv_notLinked.HideSelection = false;
            this.lv_notLinked.Location = new System.Drawing.Point(529, 165);
            this.lv_notLinked.MultiSelect = false;
            this.lv_notLinked.Name = "lv_notLinked";
            this.lv_notLinked.Size = new System.Drawing.Size(198, 247);
            this.lv_notLinked.TabIndex = 57;
            this.lv_notLinked.UseCompatibleStateImageBehavior = false;
            this.lv_notLinked.View = System.Windows.Forms.View.Details;
            this.lv_notLinked.Click += new System.EventHandler(this.lv_notLinked_Click);
            // 
            // bt_addOrDel
            // 
            this.bt_addOrDel.Location = new System.Drawing.Point(452, 273);
            this.bt_addOrDel.Name = "bt_addOrDel";
            this.bt_addOrDel.Size = new System.Drawing.Size(71, 23);
            this.bt_addOrDel.TabIndex = 58;
            this.bt_addOrDel.UseVisualStyleBackColor = true;
            this.bt_addOrDel.Click += new System.EventHandler(this.bt_addOrDel_Click);
            // 
            // bt_add
            // 
            this.bt_add.Location = new System.Drawing.Point(36, 422);
            this.bt_add.Name = "bt_add";
            this.bt_add.Size = new System.Drawing.Size(198, 23);
            this.bt_add.TabIndex = 59;
            this.bt_add.Text = "Добавить акцию";
            this.bt_add.UseVisualStyleBackColor = true;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // EditStocks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 457);
            this.Controls.Add(this.bt_add);
            this.Controls.Add(this.bt_addOrDel);
            this.Controls.Add(this.lv_notLinked);
            this.Controls.Add(this.lv_linked);
            this.Controls.Add(this.bt_edit);
            this.Controls.Add(this.cb_typeOfDiscount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtp_end);
            this.Controls.Add(this.dtp_start);
            this.Controls.Add(this.tb_discount);
            this.Controls.Add(this.tb_stockName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lv_stocks);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditStocks";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактирование акций";
            this.Activated += new System.EventHandler(this.EditStocks_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_stocks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox tb_stockName;
        private System.Windows.Forms.MaskedTextBox tb_discount;
        private System.Windows.Forms.DateTimePicker dtp_start;
        private System.Windows.Forms.DateTimePicker dtp_end;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cb_typeOfDiscount;
        private System.Windows.Forms.Button bt_edit;
        private System.Windows.Forms.ListView lv_linked;
        private System.Windows.Forms.ListView lv_notLinked;
        private System.Windows.Forms.Button bt_addOrDel;
        private System.Windows.Forms.Button bt_add;
    }
}