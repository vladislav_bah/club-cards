﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class SearchClient : Form
    {
        private DataBase BD;
        private MainForm mainform;
        public SearchClient(DataBase BD, MainForm mainform)
        {
            InitializeComponent();
            this.BD = BD;
            this.mainform = mainform;
            fill();
        }

        private void ClickOn(object sender, DataGridViewCellEventArgs e) //клик мышки по строке
        {
            try
            {
                //DataSet ds = BD.Select("select id from Clients where CardID = '" + dgv_Search.Rows[dgv_Search.CurrentCell.RowIndex].Cells[3].Value.ToString() + "'"); //мне не нравится поиск по ИД карты
                mainform.Fillform(Convert.ToInt32(dgv_Search.Rows[dgv_Search.CurrentCell.RowIndex].Cells[0].Value.ToString()));
                this.Close();
            }
            catch(Exception ex) {
                MessageBox.Show("Ошибка поиска клиента. Error: " + ex.Message);
            }
        }

        private void fill() //начальное заполнение формы
        {
            try
            {
                DataSet ds = BD.Select("Select ID, SecondName as 'Фамилия',FirstName as 'Имя',ThirdName as 'Отчество', " +
                "Phone as 'Номер телефона' from Clients");
                dgv_Search.DataSource = ds.Tables[0].DefaultView;




            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения списка клиентов. Error: " + ex.Message);
            }
        }

        private void tb_family_TextChanged(object sender, EventArgs e)
        {
            DataSet ds = BD.Select("Select ID, SecondName as 'Фамилия',FirstName as 'Имя',ThirdName as 'Отчество', " +
                "Phone as 'Номер телефона' from Clients where SecondName like '" + tb_family.Text + "%' and FirstName like '" + tb_name.Text + 
                "%' and ThirdName like '" + tb_secondName.Text + "%'");
            dgv_Search.DataSource = ds.Tables[0].DefaultView;
        }

        private void tb_name_TextChanged(object sender, EventArgs e)
        {
            DataSet ds = BD.Select("Select ID, SecondName as 'Фамилия',FirstName as 'Имя',ThirdName as 'Отчество', " +
                "Phone as 'Номер телефона' from Clients where SecondName like '" + tb_family.Text + "%' and FirstName like '" + tb_name.Text +
                "%' and ThirdName like '" + tb_secondName.Text + "%'");
            dgv_Search.DataSource = ds.Tables[0].DefaultView;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            DataSet ds = BD.Select("Select ID, SecondName as 'Фамилия',FirstName as 'Имя',ThirdName as 'Отчество', " +
                "Phone as 'Номер телефона' from Clients where SecondName like '" + tb_family.Text + "%' and FirstName like '" + tb_name.Text +
                "%' and ThirdName like '" + tb_secondName.Text + "%'");
            dgv_Search.DataSource = ds.Tables[0].DefaultView;
        }
    }
}
