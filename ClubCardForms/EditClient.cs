﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class EditClient : Form
    {
        private DataBase BD;
        private int id = -1;
        private MainForm mainform;
        private string imageLocation;

        public Image Image
        {
            set { pb_photo.Image = value;
            try
            {
                int ID_cl;
                if (id == -1)
                {
                    ID_cl = Convert.ToInt32(BD.Select("select max(ID)+1 from Clients").Tables[0].DefaultView[0][0]);
                }
                else
                {
                    ID_cl = id;
                }
                try
                {

                    imageLocation = "Photos\\" + Convert.ToString(ID_cl) + "_photo.jpg";
                    pb_photo.Image.Save(imageLocation);
                    pb_photo.ImageLocation = imageLocation;
                }
                catch
                {
                    Random rand = new Random();
                    imageLocation = "Photos\\" + Convert.ToString(ID_cl) + Convert.ToString(rand.Next(0, 1000)) + "_photo.jpg";
                    pb_photo.Image.Save(imageLocation);
                    pb_photo.ImageLocation = imageLocation;
                }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка открытия фото. Error: " + ex.Message);
                }
            }
        }
        public void AddChild(string name,string birthdayChild)
        {
            if (id == -1) //если пользователь еще не создан, сначала добавляем только в форму, а потом подргружаем во все в зависимые таблицы
            {
                var lvi = new ListViewItem(new[] { name, birthdayChild });
                lv_kids.Items.Add(lvi);
            }
            if (id >= 0)
            {
                var lvi = new ListViewItem(new[] { name, birthdayChild });
                lv_kids.Items.Add(lvi); //если происходит редактирование, то сразу добавляется ребенок в зависимые таблицы
                string query = "insert into Children(ParentID, DateOfBirth,name) values (" + id.ToString() + ",'" + birthdayChild + "','"+name+"')";
                try
                {
                    BD.Query(query);
                }
                catch
                {
                    MessageBox.Show("Ошибка добавления ребенка");
                }
            }
            lv_kids.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lv_kids.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }
        public EditClient(DataBase BD, int id, MainForm mainform)
        {
            this.id = id;
            this.BD = BD;
            InitializeComponent();
            fillareas();
            fillprofile();
            fillkids();
            lv_kids.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lv_kids.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            this.mainform = mainform;
            this.Text = "Редактирование профиля";
            imageLocation = pb_photo.ImageLocation;
        }
        private void fillprofile()
        {
            DataSet ds = BD.Select("Select * from Clients where ID = " + id);
            try
            {
                if (Convert.ToString(ds.Tables[0].DefaultView[0][1]) == "")
                {
                    tb_card.Text = "Карта не привязана";
                }
                else
                {
                    tb_card.Text = Convert.ToString(ds.Tables[0].DefaultView[0][1]);
                }             
                pb_photo.ImageLocation = Convert.ToString(ds.Tables[0].DefaultView[0][2]);
                tb_name.Text = Convert.ToString(ds.Tables[0].DefaultView[0][3]);
                tb_family.Text = Convert.ToString(ds.Tables[0].DefaultView[0][4]);
                tb_thirdName.Text = Convert.ToString(ds.Tables[0].DefaultView[0][5]);
                if (Convert.ToInt32(ds.Tables[0].DefaultView[0][6]) == 1)
                    rb_man.Checked = true;
                else
                    rb_woman.Checked = true;
                tb_phone.Text = Convert.ToString(ds.Tables[0].DefaultView[0][7]);
                tb_email.Text = Convert.ToString(ds.Tables[0].DefaultView[0][8]);
                dtp_birthday.Text = Convert.ToString(ds.Tables[0].DefaultView[0][9]);
                tb_sale.Text = Convert.ToString(ds.Tables[0].DefaultView[0][10]);
                cb_livePlace.SelectedIndex = Convert.ToInt32(ds.Tables[0].DefaultView[0][11]) - 1; //т.к. в БД индексы начинаются с 1
                cb_workPlace.SelectedIndex = Convert.ToInt32(ds.Tables[0].DefaultView[0][12]) - 1;
                rb_note.Text = Convert.ToString(ds.Tables[0].DefaultView[0][13]);
                cb_status.SelectedIndex = Convert.ToInt32(ds.Tables[0].DefaultView[0][14]);
                cb_typeOfDiscount.SelectedIndex = Convert.ToInt32(ds.Tables[0].DefaultView[0][15]);


            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения профиля. Перезапустите приложение. Error:" + ex.Message);
            }
        }
        private void fillkids()//деттееееей заполнить
        {
            lv_kids.Columns.Add("Имя");
            lv_kids.Columns.Add("Дата рождения");
            DataSet ds = BD.Select("select Name, DateOfBirth from Children where ParentID = '" + id.ToString() + "'");
            try
            {
                foreach (DataRow child in ds.Tables[0].Rows)
                {
                    var lvi = new ListViewItem(new[] { child.ItemArray[0].ToString(), child.ItemArray[1].ToString() });
                    lv_kids.Items.Add(lvi);
                }
            }
            catch { }
        }

        private void fillareas()
        {
            DataSet ds = BD.Select("Select AreaName from Areas");
            try
            {

                foreach (DataRow area in ds.Tables[0].Rows) //этот цикл проходится по всем районам из БД
                {
                    cb_livePlace.Items.Add(area.ItemArray[0].ToString());
                    cb_workPlace.Items.Add(area.ItemArray[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения районов. Перезапустите приложение. Error:" + ex.Message);
            }
        }
        public EditClient(DataBase BD, MainForm mainform)
        {
            this.BD = BD;
            InitializeComponent();
            lv_kids.Columns.Add("Имя");
            lv_kids.Columns.Add("Дата рождения");
            lv_kids.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lv_kids.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            fillareas();
            pb_photo.ImageLocation = "Photos\\Лого совершенство.png";
            this.mainform = mainform;
            Text = "Cоздание профиля";
            cb_typeOfDiscount.SelectedIndex = 0;
            tb_sale.Text = "0";
            cb_status.SelectedIndex = 0;
        }

        public void SetCard(string cardId)
        {
            tb_card.Text = cardId;
        }

        private void EnabledOk(object sender, EventArgs e)
        {
            if ((tb_family.Text != "" && tb_name.Text != ""&&
                tb_sale.Text != "" && tb_thirdName.Text != "" && (rb_man.Checked || rb_woman.Checked) )
                && cb_workPlace.SelectedIndex>=0 
                && cb_livePlace.SelectedIndex>=0)
                b_ok.Enabled=true;
            else
                b_ok.Enabled = false;

        }
        private void b_ok_Click(object sender, EventArgs e)
        {
            string name = tb_name.Text;
            string family = tb_family.Text;
            string thirdname = tb_thirdName.Text;
            string tel = tb_phone.Text;
            string email = tb_email.Text;
            string b_day = dtp_birthday.Text;
            string sale = tb_sale.Text;
            string card = tb_card.Text;
            string livePlace = cb_livePlace.Text;
            string workPlace = cb_workPlace.Text;
            string typeOfDiscount = cb_typeOfDiscount.SelectedIndex.ToString();
            string status = cb_status.SelectedIndex.ToString();
            string note = rb_note.Text;
            int homeArea = Convert.ToInt32(BD.Select("select ID from Areas where AreaName='"+cb_livePlace.Text+"'").Tables[0].DefaultView[0][0]);
            int jobArea = Convert.ToInt32(BD.Select("select ID from Areas where AreaName='" + cb_workPlace.Text+ "'").Tables[0].DefaultView[0][0]);
            int sex = -1;
            if (rb_man.Checked)
            {
                sex = 1;
            }
            else if (rb_woman.Checked)
            {
                sex = 0;
            }

            if (id == -1)
            {

                string query = "insert into Clients(CardID, PhotoSource,FirstName,SecondName,ThirdName,Sex,Phone,Email,DateOfBirth,PersonalDiscount, HomeAreaID,WorkAreaID,Note,Status,typeOfDiscount) values('" +
                    card+"','"+ imageLocation + "','"+ name+"','"+family+"','"+thirdname+"',"+sex+",'" +tel+"','"+email+"','"+b_day+"',"+sale+","+homeArea+","+jobArea+",'"+note+ "',"+status+","+typeOfDiscount+")";
                try
                {
                    BD.Query(query);
                    MessageBox.Show("Профиль добавлен успешно!");
                }
                catch
                {
                    MessageBox.Show("Ошибка добавления профиля");
                }

                int ClientID = Convert.ToInt32(BD.Select("select ID from Clients where CardID='" +card + "'").Tables[0].DefaultView[0][0]);

                foreach (ListViewItem kid in lv_kids.Items)
                {
                    query = "insert into Children(ParentID, DateOfBirth, name) values (" + ClientID.ToString() + ",'" + kid.SubItems[1] + "','"+ kid.SubItems[0]+"')";
                    try
                    {
                        BD.Query(query);
                    }
                    catch
                    {
                        MessageBox.Show("Ошибка добавления ребенка");
                    }
                }
                mainform.Fillform(Convert.ToInt32(BD.Select("Select max(ID) from Clients").Tables[0].DefaultView[0][0]));
            } //Сделать обновление основной формы(нужен ID от добавляемого пользователя)
            if (id >= 0)
            {

                string query = "update Clients set CardID ='" + card + "', PhotoSource ='" + imageLocation +
                    "',FirstName ='" + name + "',SecondName ='" + family + "',ThirdName='" + thirdname + "',Sex = " + sex +
                    ",Phone='" + tel + "',Email='" + email + "',DateOfBirth='" + b_day + "',PersonalDiscount=" + sale +
                    ",HomeAreaID=" + homeArea + ",WorkAreaID=" + jobArea + ",Note='" + note + "',Status=" + status + ",typeOfDiscount ="+typeOfDiscount +" where ID="+id;
                try
                {
                    BD.Query(query);
                    MessageBox.Show("Профиль изменен успешно!");
                }
                catch
                {
                    MessageBox.Show("Ошибка изменения профиля");
                }
                mainform.Fillform(id);
            } // тут все хорошо

            this.Close();

        }

        private void b_photo_Click(object sender, EventArgs e)
        {
            ofd_photo.InitialDirectory = "C:\\";
            ofd_photo.Filter = "jpg files (*.jpg)|*.jpg|All files (*.*)|*.*";
            ofd_photo.FilterIndex = 2;
            if (ofd_photo.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    int ID_cl;
                    if (id == -1)
                    {
                        ID_cl = Convert.ToInt32(BD.Select("select max(ID)+1 from Clients").Tables[0].DefaultView[0][0]);
                    }
                    else
                    {
                        ID_cl = id;
                    }
                    pb_photo.ImageLocation = ofd_photo.FileName;
                    try
                    {
                        imageLocation = "Photos\\" + Convert.ToString(ID_cl) + "_photo.jpg";
                        File.Copy(pb_photo.ImageLocation, imageLocation);
                    }
                    catch
                    {
                        Random rand = new Random();
                        imageLocation = "Photos\\" + Convert.ToString(ID_cl) + Convert.ToString(rand.Next(0, 1000)) + "_photo.jpg";
                        File.Copy(pb_photo.ImageLocation, imageLocation);
                        File.Delete("Photos\\" + Convert.ToString(ID_cl) + "_photo.jpg");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка открытия фото. Error: " + ex.Message);
                }

            }
        }

        private void b_addKid_Click(object sender, EventArgs e)
        {
            addChildForm form = new addChildForm(this);
            form.ShowDialog();
        }

        private void b_changeCard_Click(object sender, EventArgs e)
        {
            ChangeCard cc = new ChangeCard(BD, this, mainform.port);
            cc.ShowDialog();
        }

        private void b_photoFromCamera_Click(object sender, EventArgs e)
        {
            GetPhoto form = new GetPhoto(this);
            form.ShowDialog();
        }






    }
}
