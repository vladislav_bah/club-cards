﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class PaymentStory : Form
    {
        private DataBase BD;
        public PaymentStory(DataBase BD)
        {
            InitializeComponent();
            this.BD = BD;
            fill();
        }


       private void ExportToExcel()
        {
            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Финансовая выписка";

                int cellRowIndex = 1;
                int cellColumnIndex = 1;

                //Loop through each row and read value from each column. 
                for (int i = 0; i < dgv_Search.Rows.Count + 1; i++)
                {
                    for (int j = 0; j < dgv_Search.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dgv_Search.Columns[j].HeaderText;
                        }
                        else
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dgv_Search.Rows[i-1].Cells[j].Value.ToString();
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }

                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.CreatePrompt = true;
                saveDialog.OverwritePrompt = true;
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 1;
                saveDialog.FileName = "Выписка от " + DateTime.Now.ToShortDateString();
                saveDialog.DefaultExt = "xls";

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Выписка успешно сохранена");
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                workbook = null;
                excel = null;
            }

        }  
        private void fill() //начальное заполнение формы
        {
            try
            {
                DataSet ds = BD.Select("select FirstName as 'Имя',Secondname as 'Фамилия',Thirdname as 'Отчество',OfferName as 'Услуга',Cost as 'Цена',PaymentDate as 'Дата покупки',PaymentType as 'Тип платежа' from PaymentHistory,Clients_Subs,Clients where PaymentHistory.SubID=Clients_Subs.SubID and Clients.ID=Clients_Subs.ClientID");
                dgv_Search.DataSource = ds.Tables[0].DefaultView;

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (dgv_Search.Rows[i].Cells[6].Value.ToString() == "0")
                    {
                        dgv_Search.Rows[i].Cells[6].Value = "Наличный расчет";
                    }
                    else
                    {
                        dgv_Search.Rows[i].Cells[6].Value = "Терминал";
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения списка покупок. Error: " + ex.Message);
            }
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }
    }
}
