﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace ClubCardForms
{
    public partial class Com_selection : Form
    {
        MainForm form;
        public Com_selection(MainForm form)
        {
            InitializeComponent();
            this.form = form;
            fill();

        }

        private void fill()
        {
            string[] ports = SerialPort.GetPortNames();
            for (int i = 0; i < ports.Length; i++)
            {
                cb_port.Items.Add(ports[i].ToString());
            }
            cb_port.SelectedIndex = 0;

        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            if (cb_port.SelectedIndex == 0)
            {
                form.port = "COM1";
            }
            else
            {
                form.port = cb_port.SelectedItem.ToString();
            }
            this.Close();
        }

        private void Com_selection_Load(object sender, EventArgs e)
        {

        }

        private void cb_port_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
