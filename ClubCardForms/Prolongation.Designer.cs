﻿namespace ClubCardForms
{
    partial class Prolongation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Prolongation));
            this.l_stock = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_stock = new System.Windows.Forms.ComboBox();
            this.l_price = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.b_ok = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rb_nal = new System.Windows.Forms.RadioButton();
            this.rb_term = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_dateOfEnd = new System.Windows.Forms.TextBox();
            this.l_offer = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // l_stock
            // 
            this.l_stock.AutoSize = true;
            this.l_stock.Location = new System.Drawing.Point(409, 46);
            this.l_stock.Name = "l_stock";
            this.l_stock.Size = new System.Drawing.Size(50, 13);
            this.l_stock.TabIndex = 64;
            this.l_stock.Text = "Скидка: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(239, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "Продлить по акции";
            // 
            // cb_stock
            // 
            this.cb_stock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_stock.FormattingEnabled = true;
            this.cb_stock.Location = new System.Drawing.Point(242, 43);
            this.cb_stock.Name = "cb_stock";
            this.cb_stock.Size = new System.Drawing.Size(161, 21);
            this.cb_stock.TabIndex = 62;
            this.cb_stock.SelectedIndexChanged += new System.EventHandler(this.cb_stock_SelectedIndexChanged);
            // 
            // l_price
            // 
            this.l_price.AutoSize = true;
            this.l_price.Location = new System.Drawing.Point(459, 94);
            this.l_price.Name = "l_price";
            this.l_price.Size = new System.Drawing.Size(0, 13);
            this.l_price.TabIndex = 66;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(419, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 65;
            this.label2.Text = "Итого:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.b_ok);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_dateOfEnd);
            this.groupBox1.Controls.Add(this.l_offer);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.l_price);
            this.groupBox1.Controls.Add(this.cb_stock);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.l_stock);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(509, 199);
            this.groupBox1.TabIndex = 67;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Продление";
            // 
            // b_ok
            // 
            this.b_ok.Location = new System.Drawing.Point(190, 146);
            this.b_ok.Name = "b_ok";
            this.b_ok.Size = new System.Drawing.Size(121, 41);
            this.b_ok.TabIndex = 68;
            this.b_ok.Text = "Продлить";
            this.b_ok.UseVisualStyleBackColor = true;
            this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rb_nal);
            this.groupBox2.Controls.Add(this.rb_term);
            this.groupBox2.Location = new System.Drawing.Point(335, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(168, 64);
            this.groupBox2.TabIndex = 69;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Тип оплаты";
            // 
            // rb_nal
            // 
            this.rb_nal.AutoSize = true;
            this.rb_nal.Checked = true;
            this.rb_nal.Location = new System.Drawing.Point(25, 19);
            this.rb_nal.Name = "rb_nal";
            this.rb_nal.Size = new System.Drawing.Size(113, 17);
            this.rb_nal.TabIndex = 66;
            this.rb_nal.TabStop = true;
            this.rb_nal.Text = "Наличный расчет";
            this.rb_nal.UseVisualStyleBackColor = true;
            // 
            // rb_term
            // 
            this.rb_term.AutoSize = true;
            this.rb_term.Location = new System.Drawing.Point(25, 41);
            this.rb_term.Name = "rb_term";
            this.rb_term.Size = new System.Drawing.Size(76, 17);
            this.rb_term.TabIndex = 67;
            this.rb_term.Text = "Терминал";
            this.rb_term.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 13);
            this.label4.TabIndex = 70;
            this.label4.Text = "Дата окончания действия";
            // 
            // tb_dateOfEnd
            // 
            this.tb_dateOfEnd.Location = new System.Drawing.Point(7, 87);
            this.tb_dateOfEnd.Name = "tb_dateOfEnd";
            this.tb_dateOfEnd.Size = new System.Drawing.Size(181, 20);
            this.tb_dateOfEnd.TabIndex = 69;
            // 
            // l_offer
            // 
            this.l_offer.AutoSize = true;
            this.l_offer.Location = new System.Drawing.Point(6, 46);
            this.l_offer.Name = "l_offer";
            this.l_offer.Size = new System.Drawing.Size(0, 13);
            this.l_offer.TabIndex = 68;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 67;
            this.label3.Text = "Услуга";
            // 
            // Prolongation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 222);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Prolongation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Продление абонемента";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label l_stock;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_stock;
        private System.Windows.Forms.Label l_price;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_dateOfEnd;
        private System.Windows.Forms.Label l_offer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button b_ok;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rb_nal;
        private System.Windows.Forms.RadioButton rb_term;
    }
}