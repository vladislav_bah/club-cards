﻿namespace ClubCardForms
{
    partial class Treners
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_delete = new System.Windows.Forms.Button();
            this.bt_add = new System.Windows.Forms.Button();
            this.dgv_Treners = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Treners)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_delete
            // 
            this.bt_delete.Location = new System.Drawing.Point(316, 427);
            this.bt_delete.Name = "bt_delete";
            this.bt_delete.Size = new System.Drawing.Size(122, 31);
            this.bt_delete.TabIndex = 6;
            this.bt_delete.Text = "Удалить тренера";
            this.bt_delete.UseVisualStyleBackColor = true;
            this.bt_delete.Click += new System.EventHandler(this.bt_delete_Click);
            // 
            // bt_add
            // 
            this.bt_add.Location = new System.Drawing.Point(25, 428);
            this.bt_add.Name = "bt_add";
            this.bt_add.Size = new System.Drawing.Size(122, 31);
            this.bt_add.TabIndex = 5;
            this.bt_add.Text = "Добавить тренера";
            this.bt_add.UseVisualStyleBackColor = true;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // dgv_Treners
            // 
            this.dgv_Treners.AllowUserToAddRows = false;
            this.dgv_Treners.AllowUserToDeleteRows = false;
            this.dgv_Treners.AllowUserToResizeRows = false;
            this.dgv_Treners.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_Treners.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Treners.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgv_Treners.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Treners.Location = new System.Drawing.Point(12, 12);
            this.dgv_Treners.MultiSelect = false;
            this.dgv_Treners.Name = "dgv_Treners";
            this.dgv_Treners.Size = new System.Drawing.Size(451, 409);
            this.dgv_Treners.TabIndex = 4;
            this.dgv_Treners.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Offers_CellEndEdit);
            // 
            // Treners
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 471);
            this.Controls.Add(this.bt_delete);
            this.Controls.Add(this.bt_add);
            this.Controls.Add(this.dgv_Treners);
            this.Name = "Treners";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Тренера";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Treners)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bt_delete;
        private System.Windows.Forms.Button bt_add;
        private System.Windows.Forms.DataGridView dgv_Treners;

    }
}