﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class StatForm : Form
    {
        private DataBase DB;
        MainForm form;
        public StatForm(DataBase DB, MainForm form)
        {
            InitializeComponent();
            this.DB = DB;
            this.form = form;
            fill_lv_stat();
            fill_lv_history();
            fill_lv_now();
        }

        private void fill_lv_stat()
        {
            try
            {
                lv_stat.Columns.Add("Название услуги");
                lv_stat.Columns.Add("Сейчас занимается");
                DataSet tmp = DB.Select("Select distinct(name) from Offers where status = 1");
                foreach (DataRow name in tmp.Tables[0].Rows)
                {

                    var lvi = new ListViewItem(new[] { name.ItemArray[0].ToString(), "" });
                    lv_stat.Items.Add(lvi);

                }
                lv_stat.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                lv_stat.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

                for (int i = 0; i < lv_stat.Items.Count; i++)
                {
                    string name = lv_stat.Items[i].SubItems[0].Text;
                    DataSet tmp2 = DB.Select("Select count(*) from Meta, Subs, Offers " +
                        "where Meta.SubID = Subs.ID and Offers.ID = Subs.OfferID and Offers.Name = '" + name + "'");
                    lv_stat.Items[i].SubItems[1].Text = Convert.ToString(tmp2.Tables[0].DefaultView[0][0]);
                }
                string big_zall = DB.Select("Select count(*) from Meta where zal = 1").Tables[0].DefaultView[0][0].ToString();
                l_big.Text = "Большой зал: " + big_zall;
                string little_zall = DB.Select("Select count(*) from Meta where zal = 2").Tables[0].DefaultView[0][0].ToString();
                l_little.Text = "Малый зал: " + little_zall;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения статистики. Error: " + ex.Message);
            }
        }

        private void fill_lv_history()
        {
            try
            {
                lv_history.Columns.Add("ФИО");
                lv_history.Columns.Add("Услуга");
                lv_history.Columns.Add("Время начала");
                lv_history.Columns.Add("Время окончания");
                DataSet tmp = DB.Select("Select * from History order by id desc limit 100");
                foreach (DataRow value in tmp.Tables[0].Rows)
                {

                    var lvi = new ListViewItem(new[] { value.ItemArray[1].ToString(), value.ItemArray[2].ToString(), value.ItemArray[3].ToString(), value.ItemArray[4].ToString()});
                    lv_history.Items.Add(lvi);

                }
                lv_history.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                lv_history.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения статистики. Error: " + ex.Message);
            }
        }

        private void fill_lv_now()
        {
            try
            {
                lv_now.Columns.Add("ФИО");
                lv_now.Columns.Add("Услуга");
                lv_now.Columns.Add("Время начала");
                DataSet meta = DB.Select("Select * from meta");
                foreach (DataRow value in meta.Tables[0].Rows)
                {
                    DataSet info = DB.Select("Select FirstName, SecondName, ThirdName, ID from Clients, Clients_subs where SubID = " +  value.ItemArray[0].ToString() + " and ClientID = Clients.ID");
                    DataSet tmp = DB.Select("select Offers.Name, NumberOfVisits, Offers.type from Offers, Subs " +
                   "where Subs.ID = " + value.ItemArray[0].ToString() + " and Offers.ID = Subs.OfferID");
                    string type;
                    if (tmp.Tables[0].DefaultView[0][1].ToString() != "-1")
                    {
                        if (tmp.Tables[0].DefaultView[0][2].ToString() == "0")
                        {
                            type = "дневной";
                        }
                        else
                        {
                            type = "вечерний";
                        }
                        var lvi = new ListViewItem(new[] { info.Tables[0].DefaultView[0][0].ToString() + " " + info.Tables[0].DefaultView[0][1].ToString() + " " + 
                        info.Tables[0].DefaultView[0][2].ToString(), tmp.Tables[0].DefaultView[0][0].ToString() + " (" +tmp.Tables[0].DefaultView[0][1].ToString() + " посещений, " + type + ")", value.ItemArray[1].ToString() + " " + value.ItemArray[2].ToString()});
                        lv_now.Items.Add(lvi);                       
                    }
                    else
                    {
                        if (tmp.Tables[0].DefaultView[0][2].ToString() == "0")
                        {
                            type = "дневной";
                        }
                        else
                        {
                            type = "вечерний";
                        }
                        var lvi = new ListViewItem(new[] { info.Tables[0].DefaultView[0][0].ToString() + " " + info.Tables[0].DefaultView[0][1].ToString() + " " + 
                        info.Tables[0].DefaultView[0][2].ToString(), tmp.Tables[0].DefaultView[0][0].ToString() + " (месяц, " + type + ")", value.ItemArray[1].ToString() + " " + value.ItemArray[2].ToString()});
                        lv_now.Items.Add(lvi);                  
                    }
                }
                lv_now.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                lv_now.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения статистики. Error: " + ex.Message);
            }
        }
        private void bt_end_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
