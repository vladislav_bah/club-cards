﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class Treners : Form
    {
        private DataBase BD;
        public Treners(DataBase BD)
        {
            InitializeComponent();
            this.BD = BD;
            fill();
        }

        private void fill()
        {
            try
            {
                int rowsCount = dgv_Treners.Rows.Count;
                for (int i = 0; i < rowsCount; i++)
                {
                    dgv_Treners.Rows.Remove(dgv_Treners.Rows[0]);
                }
                DataSet ds = BD.Select("Select id as 'ID',SecondName as 'Фамилия', FirstName as 'Имя', Thirdname as 'Отчество',Phone as 'Телефон' from Treners");
                dgv_Treners.DataSource = ds.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения списка тренеров. Error: " + ex.Message);
            }

        }

        private void dgv_Offers_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            string query = "update Treners set SecondName ='" + dgv_Treners.Rows[dgv_Treners.CurrentRow.Index].Cells[1].Value.ToString() + "', FirstName = '" + dgv_Treners.Rows[dgv_Treners.CurrentRow.Index].Cells[2].Value.ToString() +
                    "' ,ThirdName ='" + dgv_Treners.Rows[dgv_Treners.CurrentRow.Index].Cells[3].Value.ToString() + "' where ID=" + dgv_Treners.Rows[dgv_Treners.CurrentRow.Index].Cells[0].Value.ToString();
            try
            {
                BD.Query(query);
                MessageBox.Show("Данные о тренере изменены успешно!");
                fill();
            }
            catch
            {
                MessageBox.Show("Ошибка изменения данных о тренере");
            }
        }

        private void bt_delete_Click(object sender, EventArgs e)
        {
            string query = "delete from Treners where ID=" + dgv_Treners.Rows[dgv_Treners.CurrentRow.Index].Cells[0].Value.ToString();
            DialogResult result = MessageBox.Show
               (
                "Вы уверены, что хотите удалить данного тренера? ",
                "Подтверждение",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.None,
                MessageBoxDefaultButton.Button1
               );
            if (result == DialogResult.Yes)
            {
                try
                {
                    BD.Query(query);
                    MessageBox.Show("Данные о тренере удалены успешно!");
                    fill();
                }
                catch
                {
                    MessageBox.Show("Ошибка удаления данных о тренере");
                }
            }
            
            else
            {
                return;
            }

        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            AddTrener add = new AddTrener(BD);
            add.ShowDialog();
            fill();
        }
    }


}
