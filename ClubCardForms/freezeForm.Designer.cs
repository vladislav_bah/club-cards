﻿namespace ClubCardForms
{
    partial class freezeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(freezeForm));
            this.dtp_freezeEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_freezeStartDate = new System.Windows.Forms.DateTimePicker();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.b_close = new System.Windows.Forms.Button();
            this.b_ok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dtp_freezeEndDate
            // 
            this.dtp_freezeEndDate.Location = new System.Drawing.Point(197, 25);
            this.dtp_freezeEndDate.Name = "dtp_freezeEndDate";
            this.dtp_freezeEndDate.Size = new System.Drawing.Size(161, 20);
            this.dtp_freezeEndDate.TabIndex = 61;
            // 
            // dtp_freezeStartDate
            // 
            this.dtp_freezeStartDate.Location = new System.Drawing.Point(30, 25);
            this.dtp_freezeStartDate.Name = "dtp_freezeStartDate";
            this.dtp_freezeStartDate.Size = new System.Drawing.Size(161, 20);
            this.dtp_freezeStartDate.TabIndex = 60;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(197, 9);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(148, 13);
            this.label26.TabIndex = 59;
            this.label26.Text = "Дата окончания заморозки";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(30, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(130, 13);
            this.label25.TabIndex = 58;
            this.label25.Text = "Дата начала заморозки";
            // 
            // b_close
            // 
            this.b_close.Location = new System.Drawing.Point(30, 62);
            this.b_close.Name = "b_close";
            this.b_close.Size = new System.Drawing.Size(120, 38);
            this.b_close.TabIndex = 62;
            this.b_close.Text = "Отмена";
            this.b_close.UseVisualStyleBackColor = true;
            this.b_close.Click += new System.EventHandler(this.b_close_Click);
            // 
            // b_ok
            // 
            this.b_ok.Location = new System.Drawing.Point(238, 62);
            this.b_ok.Name = "b_ok";
            this.b_ok.Size = new System.Drawing.Size(120, 38);
            this.b_ok.TabIndex = 63;
            this.b_ok.Text = "OK";
            this.b_ok.UseVisualStyleBackColor = true;
            this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
            // 
            // freezeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 112);
            this.Controls.Add(this.b_ok);
            this.Controls.Add(this.b_close);
            this.Controls.Add(this.dtp_freezeEndDate);
            this.Controls.Add(this.dtp_freezeStartDate);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "freezeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Заморозить абонемент";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtp_freezeEndDate;
        private System.Windows.Forms.DateTimePicker dtp_freezeStartDate;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button b_close;
        private System.Windows.Forms.Button b_ok;
    }
}