﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class StatForDay : Form
    {
        DataBase DB;
        public StatForDay(DataBase DB)
        {
            InitializeComponent();
            this.DB = DB;
            fill_ALL();
        }

        private void fill_ALL()
        {
            try
            {
                DataSet tmp2;
                DataSet tmp = DB.Select("Select FIO as 'ФИО',Offer as 'Услуга',startDate as 'Время начала',endDate as 'Время окончания' from History where startDate >= '" + DateTime.Now.ToShortDateString()+"'");
                dgv_stat1.DataSource = tmp.Tables[0].DefaultView;
                tmp = DB.Select("select FirstName as 'Имя',Secondname as 'Фамилия',Thirdname as 'Отчество',OfferName as 'Услуга',Cost as 'Цена',PaymentDate as 'Дата покупки',PaymentType as 'Тип платежа' from PaymentHistory,Clients_Subs,Clients where PaymentHistory.SubID=Clients_Subs.SubID and Clients.ID=Clients_Subs.ClientID and PaymentDate >='" + DateTime.Now.ToShortDateString()+"'");
                dgv_statPayment.DataSource = tmp.Tables[0].DefaultView;
                for (int i = 0; i < tmp.Tables[0].Rows.Count; i++ )
                {
                    if (dgv_statPayment.Rows[i].Cells[6].Value.ToString() == "0")
                    {
                        dgv_statPayment.Rows[i].Cells[6].Value = "Наличный расчет";
                    }
                    else
                    {
                        dgv_statPayment.Rows[i].Cells[6].Value = "Терминал";
                    }
                }
                tmp = DB.Select("Select sum(cost) from PaymentHistory where PaymentType = 0 and PaymentDate >='" + DateTime.Now.ToShortDateString()+"'");
                tmp2 = DB.Select("Select sum(cost) from PaymentHistory where PaymentType = 1 and PaymentDate >='" + DateTime.Now.ToShortDateString() + "'");
                l_ter.Text = tmp2.Tables[0].DefaultView[0][0].ToString();
                l_nal.Text = tmp.Tables[0].DefaultView[0][0].ToString();
                DataSet treners = DB.Select("select distinct SecondName, FirstName, ThirdName, treners.id from treners, fitnes_history where treners.id = trener_id and date  >= '" + DateTime.Now.ToShortDateString() + "'");
                DataSet actions = DB.Select("select distinct action from  fitnes_history where date  >= '" + DateTime.Now.ToShortDateString() + "'");

                for (int i = 0; i < treners.Tables[0].Rows.Count; i++)
                {
                    String tren;
                    tren = treners.Tables[0].DefaultView[i][0].ToString() + " " + treners.Tables[0].DefaultView[i][1].ToString()[0] + "." + treners.Tables[0].DefaultView[i][2].ToString()[0] + ".";
                    dgv_statTreners.Columns.Add(tren, tren);
                }

                try
                {
                    for (int i = 0; i < actions.Tables[0].Rows.Count; i++)
                    {
                        String action;
                        action = actions.Tables[0].DefaultView[i][0].ToString();
                        dgv_statTreners.Rows.Add();
                        dgv_statTreners.Rows[i].HeaderCell.Value = action;
                    }
                }
                catch { }

                try
                {
                    for (int i = 0; i < dgv_statTreners.Columns.Count; i++)
                        for (int j = 0; j < dgv_statTreners.Rows.Count; j++)
                        {
                            tmp = DB.Select("select count(*) from fitnes_history where action = '" + dgv_statTreners.Rows[j].HeaderCell.Value + "' and trener_id = " +
                                treners.Tables[0].DefaultView[i][3].ToString());
                            dgv_statTreners.Rows[j].Cells[i].Value = tmp.Tables[0].DefaultView[0][0].ToString();
                        }
                    dgv_statTreners.Rows.Add();
                    dgv_statTreners.Rows[dgv_statTreners.Rows.Count - 1].HeaderCell.Value = "Итого";
                    dgv_statTreners.Columns.Add("Итого", "Итого");

                    for (int i = 0; i < dgv_statTreners.Columns.Count - 1; i++)
                    {
                        int count = 0;
                        for (int j = 0; j < dgv_statTreners.Rows.Count - 1; j++)
                        {
                            count += Convert.ToInt32(dgv_statTreners.Rows[j].Cells[i].Value);
                        }
                        dgv_statTreners.Rows[dgv_statTreners.Rows.Count - 1].Cells[i].Value = count;
                    }

                    for (int i = 0; i < dgv_statTreners.Rows.Count - 1; i++)
                    {
                        int count = 0;
                        for (int j = 0; j < dgv_statTreners.Columns.Count - 1; j++)
                        {
                            count += Convert.ToInt32(dgv_statTreners.Rows[i].Cells[j].Value);
                        }
                        dgv_statTreners.Rows[i].Cells[dgv_statTreners.Columns.Count - 1].Value = count;
                    }
                }
                catch { }
                


                //tmp = DB.Select("select action, FirstName, SecondName, ThirdName from fitnes_history, treners where treners.id = fitnes_history.trener_id and date  >= '" + DateTime.Now.ToShortDateString() + "'");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка заполнения статистики. Error: " + ex.Message);
            }
        }


        private void ExportToExcel()
        {
            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            Microsoft.Office.Interop.Excel._Worksheet worksheet2 = null;
            Microsoft.Office.Interop.Excel._Worksheet worksheet3 = null;

            try
            {

                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Cтатистика посещений ";
                int cellRowIndex = 1;
                int cellColumnIndex =1;

                //Loop through each row and read value from each column. 
                for (int i = 0; i < dgv_stat1.Rows.Count + 1; i++)
                {

                    for (int j = 0; j < dgv_stat1.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dgv_stat1.Columns[j].HeaderText;
                        }
                        else
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dgv_stat1.Rows[i-1].Cells[j].Value.ToString();
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                worksheet.Columns.AutoFit();

                workbook.Sheets.Add();
                worksheet2 = workbook.Sheets[1];
                //worksheet2.Activate();
                worksheet2.Name = "Cтатистика оплат ";
                cellRowIndex = 1;
                cellColumnIndex = 1;
                //Loop through each row and read value from each column. 
                for (int i = 0; i < dgv_statPayment.Rows.Count + 1; i++)
                {
                    for (int j = 0; j < dgv_statPayment.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet2.Cells[cellRowIndex, cellColumnIndex] = dgv_statPayment.Columns[j].HeaderText;
                        }
                        else
                        {
                            worksheet2.Cells[cellRowIndex, cellColumnIndex] = dgv_statPayment.Rows[i-1].Cells[j].Value.ToString();
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                worksheet2.Cells[cellRowIndex, 1] = "Всего по терминалу";
                worksheet2.Cells[cellRowIndex, 2] = l_ter.Text;
                worksheet2.Cells[cellRowIndex + 1, 1] = "Всего по наличному рассчету";
                worksheet2.Cells[cellRowIndex + 1, 2] = l_nal.Text;
                worksheet2.Columns.AutoFit();


                workbook.Sheets.Add();
                worksheet3 = workbook.Sheets[1];
                //worksheet3.Activate();
                worksheet3.Name = "Cтатистика по тренерам";
                cellRowIndex = 1;
                cellColumnIndex = 1;
                //Loop through each row and read value from each column. 
                for (int i = 0; i < dgv_statTreners.Rows.Count +1; i++)
                {
                    if (i < dgv_statTreners.Rows.Count)
                    {
                        worksheet3.Cells[cellRowIndex + 1, cellColumnIndex] = dgv_statTreners.Rows[i].HeaderCell.Value.ToString();
                    }
                    cellColumnIndex++;
                    for (int j = 0; j < dgv_statTreners.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet3.Cells[cellRowIndex, cellColumnIndex] = dgv_statTreners.Columns[j].HeaderText;
                        }
                        else
                        {
                            try
                            {
                                worksheet3.Cells[cellRowIndex, cellColumnIndex] = dgv_statTreners.Rows[i - 1].Cells[j].Value.ToString();

                            }
                            catch
                            {
                                worksheet3.Cells[cellRowIndex, cellColumnIndex] = "";
                            }
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                worksheet3.Columns.AutoFit();

                //workbook.SaveAs("C:\\stat\\" + DateTime.Now.ToShortDateString() + ".xlsx");
                ////Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 1;
                saveDialog.FileName = DateTime.Now.ToShortDateString();
                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Выписка успешно сохранена");
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                workbook = null;
                excel = null;
            }

        }  
        private void button1_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }
    }
}
