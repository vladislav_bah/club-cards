﻿namespace ClubCardForms
{
    partial class fitnes_choose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_shedule = new System.Windows.Forms.ListView();
            this.rb_yes = new System.Windows.Forms.RadioButton();
            this.rb_no = new System.Windows.Forms.RadioButton();
            this.bt_ok = new System.Windows.Forms.Button();
            this.bt_cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lv_shedule
            // 
            this.lv_shedule.FullRowSelect = true;
            this.lv_shedule.GridLines = true;
            this.lv_shedule.Location = new System.Drawing.Point(12, 108);
            this.lv_shedule.Name = "lv_shedule";
            this.lv_shedule.Size = new System.Drawing.Size(427, 230);
            this.lv_shedule.TabIndex = 19;
            this.lv_shedule.UseCompatibleStateImageBehavior = false;
            this.lv_shedule.View = System.Windows.Forms.View.Details;
            this.lv_shedule.SelectedIndexChanged += new System.EventHandler(this.lv_shedule_SelectedIndexChanged);
            // 
            // rb_yes
            // 
            this.rb_yes.AutoSize = true;
            this.rb_yes.Checked = true;
            this.rb_yes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rb_yes.Location = new System.Drawing.Point(12, 21);
            this.rb_yes.Name = "rb_yes";
            this.rb_yes.Size = new System.Drawing.Size(215, 22);
            this.rb_yes.TabIndex = 21;
            this.rb_yes.TabStop = true;
            this.rb_yes.Text = "Нет ближайших занятий";
            this.rb_yes.UseVisualStyleBackColor = true;
            this.rb_yes.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rb_no
            // 
            this.rb_no.AutoSize = true;
            this.rb_no.Location = new System.Drawing.Point(12, 61);
            this.rb_no.Name = "rb_no";
            this.rb_no.Size = new System.Drawing.Size(150, 17);
            this.rb_no.TabIndex = 22;
            this.rb_no.Text = "Выбрать другое занятие";
            this.rb_no.UseVisualStyleBackColor = true;
            this.rb_no.CheckedChanged += new System.EventHandler(this.rb_no_CheckedChanged);
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(34, 349);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(93, 32);
            this.bt_ok.TabIndex = 23;
            this.bt_ok.Text = "Записать";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // bt_cancel
            // 
            this.bt_cancel.Location = new System.Drawing.Point(331, 349);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(95, 32);
            this.bt_cancel.TabIndex = 24;
            this.bt_cancel.Text = "Отмена";
            this.bt_cancel.UseVisualStyleBackColor = true;
            this.bt_cancel.Click += new System.EventHandler(this.bt_cancel_Click);
            // 
            // fitnes_choose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 395);
            this.Controls.Add(this.bt_cancel);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.rb_no);
            this.Controls.Add(this.rb_yes);
            this.Controls.Add(this.lv_shedule);
            this.Name = "fitnes_choose";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Запись на занятие";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_shedule;
        private System.Windows.Forms.RadioButton rb_yes;
        private System.Windows.Forms.RadioButton rb_no;
        private System.Windows.Forms.Button bt_ok;
        private System.Windows.Forms.Button bt_cancel;
    }
}