﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class addChildForm : Form
    {
        private EditClient parentform;
        public addChildForm(EditClient parentform)
        {
            InitializeComponent();
            this.parentform = parentform;
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            parentform.AddChild(tb_name.Text,dtp_birthday.Text);
            this.Close();
        }
    }
}
