﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class AddStock : Form
    {
        DataBase BD;
        public AddStock(DataBase BD)
        {
            InitializeComponent();
            this.BD = BD;
            fill_links();
        }

        private void Changed(object sender, EventArgs e)
        {
            if(tb_discount.Text != "" && tb_stockName.Text!="" &&dtp_end.Text != DateTime.Today.ToLongDateString())
            {
                bt_add.Enabled = true;
            }
        }

        private void fill_links()
        {
            lv_linked.Clear();
            lv_notLinked.Clear();
            lv_linked.Columns.Add("ID");
            lv_linked.Columns.Add("Распространяется на:");
            lv_notLinked.Columns.Add("ID");
            lv_notLinked.Columns.Add("Не распространяется на:");
            DataSet tmp2 = BD.Select("select Id,Name,NumberOfVisits,type from Offers");
            string type = "";
            foreach (DataRow stock in tmp2.Tables[0].Rows)
            {
                switch (stock.ItemArray[3].ToString())
                {
                    case "0": type = "Дневной";
                        break;

                    case "1": type = "Вечерний";
                        break;


                }
                string limit = "";
                if (stock.ItemArray[2].ToString() == "-1")
                {
                    limit = "Неограниченно";
                }
                else
                {
                    limit = stock.ItemArray[2].ToString();
                }
                var lvi = new ListViewItem(new[] { stock.ItemArray[0].ToString(), stock.ItemArray[1].ToString() + " (" + type + ") " + limit + " посещений" });
                lv_notLinked.Items.Add(lvi);

            }
            lv_linked.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lv_linked.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            lv_notLinked.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            lv_notLinked.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void bt_addOrDel_Click(object sender, EventArgs e)
        {
            if (bt_addOrDel.Text == "--->" && lv_linked.SelectedItems.Count>0 )
            {
                lv_notLinked.Items.Add(new ListViewItem(new[] {lv_linked.SelectedItems[0].SubItems[0].Text, lv_linked.SelectedItems[0].SubItems[1].Text}));
                lv_linked.SelectedItems[0].Remove();
            }

            if (bt_addOrDel.Text == "<---" && lv_notLinked.SelectedItems.Count>0 )
            {
                lv_linked.Items.Add(new ListViewItem(new[] { lv_notLinked.SelectedItems[0].SubItems[0].Text, lv_notLinked.SelectedItems[0].SubItems[1].Text }));
                lv_notLinked.SelectedItems[0].Remove();
            }
        }

        private void lv_notLinked_Click(object sender, EventArgs e)
        {
            bt_addOrDel.Text = "<---";
        }

        private void lv_linked_Click(object sender, EventArgs e)
        {
            bt_addOrDel.Text = "--->";
        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            int id = 0;
            try
            {
                BD.Query("insert into Stock(Discount,Name,StartDate,EndDate,type) values(" + tb_discount.Text + ",'" + tb_stockName.Text + "','" + dtp_start.Text + "','" + dtp_end.Text + "'," + cb_typeOfDiscount.SelectedIndex + ")");
                id = Convert.ToInt32(BD.Select("select max(id) from stock").Tables[0].DefaultView[0][0].ToString());
            }
            catch
            {
                MessageBox.Show("Ошибка добавления акции");
            }
            try
            {
                foreach (ListViewItem item in lv_linked.Items)
                {
                    BD.Query("insert into offers_stock(Offer_ID,Stock_ID) values(" + item.SubItems[0].Text + "," + id + ")");
                }
            }
            catch
            {
                MessageBox.Show("Ошибка добавления связи с услугами");
            }
            Close();
        }

    }
}
