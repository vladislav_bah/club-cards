﻿namespace ClubCardForms
{
    partial class Shedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_mon = new System.Windows.Forms.ListView();
            this.lv_tue = new System.Windows.Forms.ListView();
            this.lv_wed = new System.Windows.Forms.ListView();
            this.lv_sat = new System.Windows.Forms.ListView();
            this.lv_fri = new System.Windows.Forms.ListView();
            this.lv_thu = new System.Windows.Forms.ListView();
            this.lv_sun = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bt_delete = new System.Windows.Forms.Button();
            this.tb_exercise = new System.Windows.Forms.TextBox();
            this.bt_apply = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cb_trener = new System.Windows.Forms.ComboBox();
            this.cd_zal = new System.Windows.Forms.ComboBox();
            this.tb_time = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cb_weekDay = new System.Windows.Forms.ComboBox();
            this.tb_exercise2 = new System.Windows.Forms.TextBox();
            this.bt_add = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cb_trener2 = new System.Windows.Forms.ComboBox();
            this.cb_zal2 = new System.Windows.Forms.ComboBox();
            this.tb_time2 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lv_mon
            // 
            this.lv_mon.FullRowSelect = true;
            this.lv_mon.GridLines = true;
            this.lv_mon.Location = new System.Drawing.Point(12, 38);
            this.lv_mon.Name = "lv_mon";
            this.lv_mon.Size = new System.Drawing.Size(372, 232);
            this.lv_mon.TabIndex = 18;
            this.lv_mon.UseCompatibleStateImageBehavior = false;
            this.lv_mon.View = System.Windows.Forms.View.Details;
            this.lv_mon.SelectedIndexChanged += new System.EventHandler(this.lv_mon_SelectedIndexChanged);
            // 
            // lv_tue
            // 
            this.lv_tue.FullRowSelect = true;
            this.lv_tue.GridLines = true;
            this.lv_tue.Location = new System.Drawing.Point(416, 38);
            this.lv_tue.Name = "lv_tue";
            this.lv_tue.Size = new System.Drawing.Size(372, 232);
            this.lv_tue.TabIndex = 19;
            this.lv_tue.UseCompatibleStateImageBehavior = false;
            this.lv_tue.View = System.Windows.Forms.View.Details;
            this.lv_tue.SelectedIndexChanged += new System.EventHandler(this.lv_tue_SelectedIndexChanged);
            // 
            // lv_wed
            // 
            this.lv_wed.FullRowSelect = true;
            this.lv_wed.GridLines = true;
            this.lv_wed.Location = new System.Drawing.Point(820, 38);
            this.lv_wed.Name = "lv_wed";
            this.lv_wed.Size = new System.Drawing.Size(372, 232);
            this.lv_wed.TabIndex = 20;
            this.lv_wed.UseCompatibleStateImageBehavior = false;
            this.lv_wed.View = System.Windows.Forms.View.Details;
            this.lv_wed.SelectedIndexChanged += new System.EventHandler(this.lv_wed_SelectedIndexChanged);
            // 
            // lv_sat
            // 
            this.lv_sat.FullRowSelect = true;
            this.lv_sat.GridLines = true;
            this.lv_sat.Location = new System.Drawing.Point(820, 300);
            this.lv_sat.Name = "lv_sat";
            this.lv_sat.Size = new System.Drawing.Size(372, 232);
            this.lv_sat.TabIndex = 23;
            this.lv_sat.UseCompatibleStateImageBehavior = false;
            this.lv_sat.View = System.Windows.Forms.View.Details;
            this.lv_sat.SelectedIndexChanged += new System.EventHandler(this.lv_sat_SelectedIndexChanged);
            // 
            // lv_fri
            // 
            this.lv_fri.FullRowSelect = true;
            this.lv_fri.GridLines = true;
            this.lv_fri.Location = new System.Drawing.Point(416, 300);
            this.lv_fri.Name = "lv_fri";
            this.lv_fri.Size = new System.Drawing.Size(372, 232);
            this.lv_fri.TabIndex = 22;
            this.lv_fri.UseCompatibleStateImageBehavior = false;
            this.lv_fri.View = System.Windows.Forms.View.Details;
            this.lv_fri.SelectedIndexChanged += new System.EventHandler(this.lv_fri_SelectedIndexChanged);
            // 
            // lv_thu
            // 
            this.lv_thu.FullRowSelect = true;
            this.lv_thu.GridLines = true;
            this.lv_thu.Location = new System.Drawing.Point(12, 300);
            this.lv_thu.Name = "lv_thu";
            this.lv_thu.Size = new System.Drawing.Size(372, 232);
            this.lv_thu.TabIndex = 21;
            this.lv_thu.UseCompatibleStateImageBehavior = false;
            this.lv_thu.View = System.Windows.Forms.View.Details;
            this.lv_thu.SelectedIndexChanged += new System.EventHandler(this.lv_thu_SelectedIndexChanged);
            // 
            // lv_sun
            // 
            this.lv_sun.FullRowSelect = true;
            this.lv_sun.GridLines = true;
            this.lv_sun.Location = new System.Drawing.Point(12, 562);
            this.lv_sun.Name = "lv_sun";
            this.lv_sun.Size = new System.Drawing.Size(372, 232);
            this.lv_sun.TabIndex = 24;
            this.lv_sun.UseCompatibleStateImageBehavior = false;
            this.lv_sun.View = System.Windows.Forms.View.Details;
            this.lv_sun.SelectedIndexChanged += new System.EventHandler(this.lv_sun_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 25;
            this.label1.Text = "Понедельник";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(413, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 16);
            this.label2.TabIndex = 26;
            this.label2.Text = "Вторник";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(817, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 16);
            this.label3.TabIndex = 27;
            this.label3.Text = "Среда";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(9, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 16);
            this.label4.TabIndex = 28;
            this.label4.Text = "Четверг";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(413, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 29;
            this.label5.Text = "Пятница";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(817, 281);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 16);
            this.label6.TabIndex = 30;
            this.label6.Text = "Суббота";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(9, 543);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 16);
            this.label7.TabIndex = 31;
            this.label7.Text = "Воскресение";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bt_delete);
            this.groupBox1.Controls.Add(this.tb_exercise);
            this.groupBox1.Controls.Add(this.bt_apply);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cb_trener);
            this.groupBox1.Controls.Add(this.cd_zal);
            this.groupBox1.Controls.Add(this.tb_time);
            this.groupBox1.Location = new System.Drawing.Point(416, 562);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 114);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Редактироваие занятия";
            // 
            // bt_delete
            // 
            this.bt_delete.BackColor = System.Drawing.Color.Red;
            this.bt_delete.ForeColor = System.Drawing.Color.Yellow;
            this.bt_delete.Location = new System.Drawing.Point(518, 69);
            this.bt_delete.Name = "bt_delete";
            this.bt_delete.Size = new System.Drawing.Size(119, 36);
            this.bt_delete.TabIndex = 10;
            this.bt_delete.Text = "Удалить занятие из расписания";
            this.bt_delete.UseVisualStyleBackColor = false;
            this.bt_delete.Click += new System.EventHandler(this.bt_delete_Click);
            // 
            // tb_exercise
            // 
            this.tb_exercise.Location = new System.Drawing.Point(154, 43);
            this.tb_exercise.Name = "tb_exercise";
            this.tb_exercise.Size = new System.Drawing.Size(179, 20);
            this.tb_exercise.TabIndex = 9;
            // 
            // bt_apply
            // 
            this.bt_apply.Enabled = false;
            this.bt_apply.Location = new System.Drawing.Point(643, 69);
            this.bt_apply.Name = "bt_apply";
            this.bt_apply.Size = new System.Drawing.Size(119, 36);
            this.bt_apply.TabIndex = 8;
            this.bt_apply.Text = "Применить";
            this.bt_apply.UseVisualStyleBackColor = true;
            this.bt_apply.Click += new System.EventHandler(this.bt_apply_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(515, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Тренер";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(348, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Зал";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(151, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Занятие";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Время";
            // 
            // cb_trener
            // 
            this.cb_trener.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_trener.FormattingEnabled = true;
            this.cb_trener.Location = new System.Drawing.Point(518, 42);
            this.cb_trener.Name = "cb_trener";
            this.cb_trener.Size = new System.Drawing.Size(244, 21);
            this.cb_trener.TabIndex = 3;
            // 
            // cd_zal
            // 
            this.cd_zal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cd_zal.FormattingEnabled = true;
            this.cd_zal.Location = new System.Drawing.Point(351, 42);
            this.cd_zal.Name = "cd_zal";
            this.cd_zal.Size = new System.Drawing.Size(138, 21);
            this.cd_zal.TabIndex = 2;
            // 
            // tb_time
            // 
            this.tb_time.Location = new System.Drawing.Point(16, 43);
            this.tb_time.Name = "tb_time";
            this.tb_time.Size = new System.Drawing.Size(121, 20);
            this.tb_time.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.cb_weekDay);
            this.groupBox2.Controls.Add(this.tb_exercise2);
            this.groupBox2.Controls.Add(this.bt_add);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.cb_trener2);
            this.groupBox2.Controls.Add(this.cb_zal2);
            this.groupBox2.Controls.Add(this.tb_time2);
            this.groupBox2.Location = new System.Drawing.Point(416, 694);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(776, 121);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Добавление занятия";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 12;
            this.label16.Text = "День недели";
            // 
            // cb_weekDay
            // 
            this.cb_weekDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_weekDay.FormattingEnabled = true;
            this.cb_weekDay.Location = new System.Drawing.Point(16, 86);
            this.cb_weekDay.Name = "cb_weekDay";
            this.cb_weekDay.Size = new System.Drawing.Size(121, 21);
            this.cb_weekDay.TabIndex = 11;
            this.cb_weekDay.SelectedIndexChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_exercise2
            // 
            this.tb_exercise2.Location = new System.Drawing.Point(154, 43);
            this.tb_exercise2.Name = "tb_exercise2";
            this.tb_exercise2.Size = new System.Drawing.Size(179, 20);
            this.tb_exercise2.TabIndex = 10;
            this.tb_exercise2.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // bt_add
            // 
            this.bt_add.Enabled = false;
            this.bt_add.Location = new System.Drawing.Point(643, 69);
            this.bt_add.Name = "bt_add";
            this.bt_add.Size = new System.Drawing.Size(119, 36);
            this.bt_add.TabIndex = 8;
            this.bt_add.Text = "Добавить";
            this.bt_add.UseVisualStyleBackColor = true;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(515, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Тренер";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(348, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Зал";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(151, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Занятие";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Время";
            // 
            // cb_trener2
            // 
            this.cb_trener2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_trener2.FormattingEnabled = true;
            this.cb_trener2.Location = new System.Drawing.Point(518, 42);
            this.cb_trener2.Name = "cb_trener2";
            this.cb_trener2.Size = new System.Drawing.Size(244, 21);
            this.cb_trener2.TabIndex = 3;
            this.cb_trener2.SelectedIndexChanged += new System.EventHandler(this.EnabledOk);
            // 
            // cb_zal2
            // 
            this.cb_zal2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_zal2.FormattingEnabled = true;
            this.cb_zal2.Location = new System.Drawing.Point(351, 42);
            this.cb_zal2.Name = "cb_zal2";
            this.cb_zal2.Size = new System.Drawing.Size(138, 21);
            this.cb_zal2.TabIndex = 2;
            this.cb_zal2.SelectedIndexChanged += new System.EventHandler(this.EnabledOk);
            // 
            // tb_time2
            // 
            this.tb_time2.Location = new System.Drawing.Point(16, 43);
            this.tb_time2.Name = "tb_time2";
            this.tb_time2.Size = new System.Drawing.Size(121, 20);
            this.tb_time2.TabIndex = 0;
            this.tb_time2.TextChanged += new System.EventHandler(this.EnabledOk);
            // 
            // Shedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 823);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lv_sun);
            this.Controls.Add(this.lv_sat);
            this.Controls.Add(this.lv_fri);
            this.Controls.Add(this.lv_thu);
            this.Controls.Add(this.lv_wed);
            this.Controls.Add(this.lv_tue);
            this.Controls.Add(this.lv_mon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Shedule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Расписание";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_mon;
        private System.Windows.Forms.ListView lv_tue;
        private System.Windows.Forms.ListView lv_wed;
        private System.Windows.Forms.ListView lv_sat;
        private System.Windows.Forms.ListView lv_fri;
        private System.Windows.Forms.ListView lv_thu;
        private System.Windows.Forms.ListView lv_sun;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bt_apply;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cb_trener;
        private System.Windows.Forms.ComboBox cd_zal;
        private System.Windows.Forms.TextBox tb_time;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bt_add;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cb_trener2;
        private System.Windows.Forms.ComboBox cb_zal2;
        private System.Windows.Forms.TextBox tb_time2;
        private System.Windows.Forms.Button bt_delete;
        private System.Windows.Forms.TextBox tb_exercise;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cb_weekDay;
        private System.Windows.Forms.TextBox tb_exercise2;
    }
}