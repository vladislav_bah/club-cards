﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class EndOfExercise : Form
    {
        private int IDclient;
        private DataBase BD;
        private string SubID;

        public EndOfExercise(int IDclient, DataBase BD, string SubID)
        {
            InitializeComponent();
            this.IDclient = IDclient;
            this.SubID = SubID;
            this.BD = BD; 
            fillform();
        }

        private void fillform()
        {
            DataSet ds = BD.Select("Select PhotoSource,FirstName,SecondName,ThirdName from Clients where ID = " + IDclient);
            DataSet offerName = BD.Select("Select Name from Subs, Offers where OfferId = Offers.ID and Subs.ID = " + SubID);
            try
            {
                pb_photo.ImageLocation = Convert.ToString(ds.Tables[0].DefaultView[0][0]);
                pb_photo.Load();
            }
            catch
            {
                pb_photo.ImageLocation = "Photos/Лого совершенство.png";
            }
            lb_Name.Text = Convert.ToString(ds.Tables[0].DefaultView[0][1]);
            lb_secondName.Text = Convert.ToString(ds.Tables[0].DefaultView[0][2]);
            lb_thirdName.Text = Convert.ToString(ds.Tables[0].DefaultView[0][3]);
            lb_offer.Text = "Услуга: " + Convert.ToString(offerName.Tables[0].DefaultView[0][0]);
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            try
            {
                BD.Query("Delete from meta where SubID = " + SubID);
            }
            catch { }
            Close();
        }

        private void bt_back_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
