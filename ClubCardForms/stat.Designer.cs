﻿namespace ClubCardForms
{
    partial class StatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatForm));
            this.lv_stat = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.lv_history = new System.Windows.Forms.ListView();
            this.lv_now = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.l_little = new System.Windows.Forms.Label();
            this.l_big = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lv_stat
            // 
            this.lv_stat.GridLines = true;
            this.lv_stat.Location = new System.Drawing.Point(12, 29);
            this.lv_stat.Name = "lv_stat";
            this.lv_stat.Size = new System.Drawing.Size(280, 109);
            this.lv_stat.TabIndex = 1;
            this.lv_stat.UseCompatibleStateImageBehavior = false;
            this.lv_stat.View = System.Windows.Forms.View.Details;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Статистика по услугам";
            // 
            // lv_history
            // 
            this.lv_history.GridLines = true;
            this.lv_history.Location = new System.Drawing.Point(563, 29);
            this.lv_history.Name = "lv_history";
            this.lv_history.Size = new System.Drawing.Size(602, 454);
            this.lv_history.TabIndex = 3;
            this.lv_history.UseCompatibleStateImageBehavior = false;
            this.lv_history.View = System.Windows.Forms.View.Details;
            // 
            // lv_now
            // 
            this.lv_now.GridLines = true;
            this.lv_now.Location = new System.Drawing.Point(12, 161);
            this.lv_now.Name = "lv_now";
            this.lv_now.Size = new System.Drawing.Size(545, 322);
            this.lv_now.TabIndex = 4;
            this.lv_now.UseCompatibleStateImageBehavior = false;
            this.lv_now.View = System.Windows.Forms.View.Details;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(560, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "История посещений";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Сейчас занимаются";
            // 
            // l_little
            // 
            this.l_little.AutoSize = true;
            this.l_little.Location = new System.Drawing.Point(328, 29);
            this.l_little.Name = "l_little";
            this.l_little.Size = new System.Drawing.Size(66, 13);
            this.l_little.TabIndex = 7;
            this.l_little.Text = "Малый зал:";
            // 
            // l_big
            // 
            this.l_big.AutoSize = true;
            this.l_big.Location = new System.Drawing.Point(328, 62);
            this.l_big.Name = "l_big";
            this.l_big.Size = new System.Drawing.Size(76, 13);
            this.l_big.TabIndex = 8;
            this.l_big.Text = "Большой зал:";
            // 
            // StatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1177, 495);
            this.Controls.Add(this.l_big);
            this.Controls.Add(this.l_little);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lv_now);
            this.Controls.Add(this.lv_history);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lv_stat);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StatForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Статистика";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_stat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lv_history;
        private System.Windows.Forms.ListView lv_now;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label l_little;
        private System.Windows.Forms.Label l_big;
    }
}