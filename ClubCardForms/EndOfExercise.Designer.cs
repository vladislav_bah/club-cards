﻿namespace ClubCardForms
{
    partial class EndOfExercise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EndOfExercise));
            this.bt_ok = new System.Windows.Forms.Button();
            this.bt_back = new System.Windows.Forms.Button();
            this.pb_photo = new System.Windows.Forms.PictureBox();
            this.lb_Name = new System.Windows.Forms.Label();
            this.lb_thirdName = new System.Windows.Forms.Label();
            this.lb_secondName = new System.Windows.Forms.Label();
            this.lb_offer = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_photo)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(185, 214);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(100, 35);
            this.bt_ok.TabIndex = 0;
            this.bt_ok.Text = "Закончить занятие";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // bt_back
            // 
            this.bt_back.Location = new System.Drawing.Point(299, 214);
            this.bt_back.Name = "bt_back";
            this.bt_back.Size = new System.Drawing.Size(78, 35);
            this.bt_back.TabIndex = 1;
            this.bt_back.Text = "Отмена";
            this.bt_back.UseVisualStyleBackColor = true;
            this.bt_back.Click += new System.EventHandler(this.bt_back_Click);
            // 
            // pb_photo
            // 
            this.pb_photo.Location = new System.Drawing.Point(12, 12);
            this.pb_photo.Name = "pb_photo";
            this.pb_photo.Size = new System.Drawing.Size(167, 237);
            this.pb_photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_photo.TabIndex = 3;
            this.pb_photo.TabStop = false;
            // 
            // lb_Name
            // 
            this.lb_Name.AutoSize = true;
            this.lb_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_Name.Location = new System.Drawing.Point(202, 20);
            this.lb_Name.Name = "lb_Name";
            this.lb_Name.Size = new System.Drawing.Size(47, 15);
            this.lb_Name.TabIndex = 4;
            this.lb_Name.Text = "label2";
            // 
            // lb_thirdName
            // 
            this.lb_thirdName.AutoSize = true;
            this.lb_thirdName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_thirdName.Location = new System.Drawing.Point(202, 45);
            this.lb_thirdName.Name = "lb_thirdName";
            this.lb_thirdName.Size = new System.Drawing.Size(47, 15);
            this.lb_thirdName.TabIndex = 5;
            this.lb_thirdName.Text = "label3";
            // 
            // lb_secondName
            // 
            this.lb_secondName.AutoSize = true;
            this.lb_secondName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_secondName.Location = new System.Drawing.Point(202, 69);
            this.lb_secondName.Name = "lb_secondName";
            this.lb_secondName.Size = new System.Drawing.Size(47, 15);
            this.lb_secondName.TabIndex = 6;
            this.lb_secondName.Text = "label4";
            // 
            // lb_offer
            // 
            this.lb_offer.AutoSize = true;
            this.lb_offer.Location = new System.Drawing.Point(202, 162);
            this.lb_offer.Name = "lb_offer";
            this.lb_offer.Size = new System.Drawing.Size(35, 13);
            this.lb_offer.TabIndex = 7;
            this.lb_offer.Text = "label4";
            // 
            // EndOfExercise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 261);
            this.Controls.Add(this.lb_offer);
            this.Controls.Add(this.lb_secondName);
            this.Controls.Add(this.lb_thirdName);
            this.Controls.Add(this.lb_Name);
            this.Controls.Add(this.pb_photo);
            this.Controls.Add(this.bt_back);
            this.Controls.Add(this.bt_ok);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EndOfExercise";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Завершение занятия";
            ((System.ComponentModel.ISupportInitialize)(this.pb_photo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_ok;
        private System.Windows.Forms.Button bt_back;
        private System.Windows.Forms.PictureBox pb_photo;
        private System.Windows.Forms.Label lb_Name;
        private System.Windows.Forms.Label lb_thirdName;
        private System.Windows.Forms.Label lb_secondName;
        private System.Windows.Forms.Label lb_offer;
    }
}