﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubCardForms
{
    public partial class AddTrener : Form
    {
        private DataBase BD;
        public AddTrener(DataBase BD)
        {
            InitializeComponent();
            this.BD = BD;
        }

        private void EnabledOk(object sender, EventArgs e)
        {
            if (tb_firstName.Text!="" && tb_secondName.Text!="" && tb_thirdName.Text!="" )
                bt_add.Enabled = true;
            else
                bt_add.Enabled = false;
        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            string query = "insert into Treners(FirstName,SecondName,ThirdName,Phone) values('" +
                   tb_firstName.Text + "','" + tb_secondName.Text + "','" + tb_thirdName.Text + "','" + tb_phone.Text + "')";
            try
            {
                BD.Query(query);
                MessageBox.Show("Информация о тренере добавлена успешно!");
            }
            catch
            {
                MessageBox.Show("Ошибка добавления информации о тренере");
            }
            this.Close();
        }


    }
}
